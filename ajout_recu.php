<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & FAIRE_FACTURE))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$facture = intval($_POST['facture']);
	$somme = intval($_POST['somme']);
	$date = htmlspecialchars($_POST['date'], ENT_QUOTES);
	$payement = htmlspecialchars($_POST['payement'], ENT_QUOTES);
	if(strlen($somme) > 0 && strlen($date) > 0 && strlen($payement) > 0 && $somme > 0)
		mysql_query('INSERT INTO '.$bdd_prefixe.'recu (id, facture, somme, date, payement)
			VALUES ("", "'.$adherent.'", "'.$somme.'", "'.$date.'", "'.$payement.'")');
	header('location: factures.php');
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajout d'un reçu</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Ajout d'un reçu</h2>

	<form action="ajout_recu.php" method="post">
	Facture : <select name="facture">
		<?php $requete = mysql_query('SELECT *, f.id AS id, a.nom AS nom FROM '.$bdd_prefixe.'factures f
INNER JOIN '.$bdd_prefixe.'adherents a ON a.id = f.adherent');
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<option value="'.$donnees['id'].'">#'.$donnees['id'].' '.$donnees['prenom'].' '.$donnees['nom'].' '.formater_date($donnees['date']).'</option>';
			}
		} ?>
		</select><br />
	<label name="somme">Montant : <input name="somme" /></label> (Attention : il faut un point et non une virgule)<br />
	<label name="date">Date : <input name="date" type="date" value="<?php echo date("Y-m-d"); ?>" /></label><br />
	<label name="payement">Moyen de paiement : <input name="payement" /></label><br />
	<input type="hidden" name="envoi" value="1" />
	<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>