<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['calendrier'] || !((int)$_SESSION['permission'] & TOUCHE_CALENDRIER))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$titre = htmlspecialchars($_POST['titre'], ENT_QUOTES);
	$description = htmlspecialchars($_POST['description'], ENT_QUOTES);
	$date = htmlspecialchars($_POST['date'], ENT_QUOTES);
	$lieu = htmlspecialchars($_POST['lieu'], ENT_QUOTES);
	$lat = floatval($_POST['lat']);
	$lon = floatval($_POST['lon']);
	if(strlen($titre) > 0 && strlen($date) > 0 && strlen($description) > 0)
		mysql_query('INSERT INTO '.$bdd_prefixe.'calendrier (titre, description, date, lieu, latitude, longitude) VALUE ("'.$titre.'", "'.$description.'", "'.$date.'", "'.$lieu.'", "'.$lat.'", "'.$lon.'")') or die(mysql_error());
	header('location: calendrier.php');
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajouter un événement</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<script type="text/javascript">
		function getXMLHttpRequest()
		{
			var xhr = null;
			
			if (window.XMLHttpRequest || window.ActiveXObject) {
				if (window.ActiveXObject) {
					try {
						xhr = new ActiveXObject("Msxml2.XMLHTTP");
					} catch(e) {
						xhr = new ActiveXObject("Microsoft.XMLHTTP");
					}
				} else {
					xhr = new XMLHttpRequest(); 
				}
			} else {
				alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
				return null;
			}
			
			return xhr;
		}

		function recup_lieu()
		{
			var xhr = getXMLHttpRequest();

			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
					var json = eval('(' + xhr.responseText + ')');
					document.getElementById("statut").innerHTML = "";
					afficher_lieu(json);
				} else if (xhr.readyState < 4) {
					document.getElementById("statut").innerHTML = "Recherche…";
				}
			};

			xhr.open("GET", "http://nominatim.openstreetmap.org/search/"+document.getElementById("lieu").value + "?format=json&countrycode=fr&accept-language=fr", true);
			xhr.send(null);
		}

		function afficher_lieu(infos)
		{
			if(infos.length != 0)
			{
				document.getElementById("statut").innerHTML = "OK";
				document.getElementById('lieu').value = infos[0].display_name;
				document.getElementById('lat').value = infos[0].lat;
				document.getElementById('lon').value = infos[0].lon;
			} else {
				document.getElementById("statut").innerHTML = "Aucun résultat";
			}
		}
		</script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Ajouter un événement</h2>

	<form action="ajout_evenement.php" method="post">
	<p>
		<label name="titre">Titre : <input name="titre" /></label><br />
		Description : <textarea name="description"></textarea><br />
		<label name="date">Date : <input name="date" /> (Format AAAA-MM-JJ)</label><br />
		<label name="lieu">Lieu : <input name="lieu" id="lieu" onChange="recup_lieu();" /></label> <span id="statut">(De préférence : nom ou adresse, ville)</span>
		<input type="hidden" id="lat" name="lat" /> <input type="hidden" id="lon" name="lon" /><br />
		<label name="type">Type : <select type="type"><option value="0">Sortie culturelle</option><option value="1">Sortie loisir</option></select></label><br />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>