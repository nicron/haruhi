<?php
require_once("configuration.php");
//Connexion à la boite
$flux_imap = imap_open('{'.$serveur_yuki.':993/ssl/novalidate-cert}', $courriel_yuki, $mdp_yuki);
if($flux_imap === false)
{
  echo 'Erreur à la connexion';
  exit();
}

//On regarde si on a des mails
$statut = imap_check($flux_imap);

if($statut->Nmsgs > 0) //Des nouveaux messages
{
    $liste_mail = imap_fetch_overview($flux_imap,'1:'.$statut->Nmsgs);
    foreach($liste_mail as $courriel)
    {
	$infos_message = imap_fetchstructure($flux_imap, $courriel->uid, FT_UID);
	//On va récupérer la ml concerné par ça :
	$pour = $courriel->to;
	//On regarde si on a pas « … <xxx@yyy.fr> »
	preg_match("#^(.*) <(.*)>$#Ui", $pour, $pour_resultat);
	if(count($pour_resultat) > 0)
		$pour = $pour_resultat[2];
	
	$requete = mysql_query('SELECT id FROM '.$bdd_prefixe.'ml WHERE adresse = "'.$pour.'"');
	if($requete === false)
	      $id_ml = 0;
	else
	{
	      $donnees = mysql_fetch_array($requete);
	      $id_ml = $donnees['id'];
	}
	
	$titre_courriel = '';
	foreach(imap_mime_header_decode($courriel->subject) as $e)
	{
		$titre_courriel .= htmlspecialchars_decode($e->text);
	}
	
	$titre_courriel = utf8_encode($titre_courriel);
	//Sat, 29 Dec 2012 16:58:43 +0100
	$date = date_create_from_format("D, d M Y H:i:s O", substr($courriel->date, 0, 31)); //On retire les éventuels (GMT) à la fin
	if(isset($courriel->references))
		$id_citation_serveur = $courriel->references;
	else
		$id_citation_serveur = NULL;
		
	if(isset($courriel->in_reply_to))
		$id_reponse_serveur = $courriel->in_reply_to;
	else
		$id_reponse_serveur = NULL;
	
	//Encodage
	$charset = 'UTF-8';
	foreach($infos_message->parameters as $i => $p)
	{
		if($p->attribute == "charset")
			$charset = $p->value;
	}
	foreach(extraire_parties(1, $infos_message) as $r)
	{
		//print_r($r);
		$section = $r[0];
		if($r[1] == "message")
		{
			$contenu = imap_fetchbody($flux_imap, $courriel->uid, $section, FT_UID);
			//On applique le bon décodage
			switch ($infos_message->encoding)
			{
				case ENC7BIT:
				case ENCQUOTEDPRINTABLE:
					$contenu = imap_qprint($contenu);
					break;
				case ENC8BIT:
				case ENCOTHER:
					$contenu = $contenu;
					break;
				case ENCBINARY:
					$contenu = imap_binary($contenu);
					break;
				case ENCBASE64:
					$contenu = imap_base64($contenu);
					break;
			}
			$titre_courriel = '';
			foreach(imap_mime_header_decode($courriel->subject) as $e)
			{
				$titre_courriel .= htmlspecialchars_decode($e->text);
			}
			
			$contenu = htmlspecialchars($contenu, (ENT_COMPAT | ENT_HTML401 | ENT_IGNORE), $charset);
			
			mysql_query('INSERT INTO '.$bdd_prefixe.'archives_ml (titre, date, id_ml, expediteur, message,
				id_message_serveur, id_reponse_serveur, id_citation_serveur, type_contenu)
				      VALUES ("'.$titre_courriel.'", "'.$date->format("Y-m-d H:i:s").'", '.$id_ml.',
				      "'.htmlspecialchars($courriel->from, (ENT_COMPAT | ENT_HTML401), $charset).'",
				      "'.$contenu.'", "'.$courriel->message_id.'",
				      "'.$id_reponse_serveur.'", "'.$id_citation_serveur.'", "'.$r[2].'")') or die(mysql_error());
				$id_message = mysql_insert_id();
		} else if($r[1] == "fichier")
		{
			$fichier = imap_fetchbody($flux_imap, $courriel->uid, $section, FT_UID);
			switch ($r[4])
			{
				case ENC7BIT:
				case ENCQUOTEDPRINTABLE:
					$fichier = imap_qprint($fichier);
					break;
				case ENC8BIT:
				case ENCOTHER:
					$fichier = $fichier;
					break;
				case ENCBINARY:
					$fichier = imap_binary($fichier);
					break;
				case ENCBASE64:
					$fichier = imap_base64($fichier);
					break;
			}
			$nom_fichier_tmp = '';
			foreach(imap_mime_header_decode($r[2]) as $e)
			{
				$nom_fichier_tmp .= htmlspecialchars_decode($e->text);
			}
			$nom_echape_fichier = str_replace(array('\'', '"', ' '), "_", $nom_fichier_tmp);
			//On regarde si le fichier existe déjà
			if(!isset($_GET['enr']) || $_GET['enr'] != 'no')
			{
				$id_fichier = 0;
				$requete = mysql_query('SELECT id, version FROM '.$bdd_prefixe.'fichiers WHERE nom = "'.$nom_echape_fichier.'" ORDER BY version DESC') or die(mysql_error());
				if(!($requete === false))
				{
					if($donnees = mysql_fetch_array($requete))
					{
						$version = $donnees['version'] + 1;
						$id_fichier = $donnees['id'];
					} else
						$version = 1;
				} else
					$version = 1;
				$coupe = explode('.', $nom_echape_fichier);
				$coupe[count($coupe) - 2] .= '_v'.$version;
				$nom_fichier = implode('.', $coupe);
				$pt_fichier = fopen('fichiers/'.$nom_fichier, 'w+');
				fwrite($pt_fichier, $fichier);
				fclose($pt_fichier);
				mysql_query('INSERT INTO '.$bdd_prefixe.'fichiers (nom, taille, auteur, version, visibilite)
					VALUES ("'.$nom_echape_fichier.'", "'.$r[3].'", "-1", "'.$version.'", 3)') or die(mysql_error());
				$id_fichier_ajout = mysql_insert_id();
				if($id_fichier != 0)
					mysql_query('UPDATE '.$bdd_prefixe.'fichiers SET nouvelle_version = '.mysql_insert_id().' WHERE id = '.$id_fichier) or die(mysql_error());
				
				//Et on ajoute le lien dans la table
				mysql_query('INSERT INTO '.$bdd_prefixe.'lien_archives (id_message, id_fichier) VALUES ('.$id_message.', '.$id_fichier_ajout.')') or die(mysql_error());
			}
		}
	}
	
	//Et on oubli pas de marquer le mail comme à supprimer
	imap_delete($flux_imap, $courriel->uid, FT_UID);
    }
    imap_expunge($flux_imap); //On vide les toilettes (efface vraiment les messages)
}

function extraire_parties($section, $message)
{
	if($message->type == 1) //Mixte
	{
		if($message->subtype == "ALTERNATIVE") //Même contenu, suffit de prendre le HTML de préférence
		{
			$types = array();
			foreach($message->parts as $i => $p)
			{
				$types[$section + $i] = $p->subtype;
			}
			
			if(in_array("HTML", $types))
				return array(array(array_search("HTML", $types), "message", "HTML", "alt"));
			else
				return array(array(array_search("PLAIN", $types), "message", "PLAIN", "alt"));
		} elseif($message->subtype == "MIXED") //Plusieurs choses
		{
			$retour = array();
			foreach($message->parts as $i => $p)
			{
				$r = extraire_parties($section + $i, $p);
				$retour[] = $r[0];
			}
			
			//On refait une passe pour savoir s'il y a une alternative ou pas 
			$alt = false;
			foreach($retour as $r)
			{
				if($r[3] == "alt")
					$alt = true;
			}
			
			if($alt) //Y'a du alternative, on retire le reste
			{
				foreach($retour as $i => $r)
				{
					if($r[1] == "message" && $r[3] != "alt")
						unset($retour[$i]);
				}
			} else //On garde que le PLAIN avec le plus d'info
			{
				$max = 0;
				$i_max = -1;
				foreach($retour as $i => $r)
				{
					if($r[1] == "message")
					{
						if($max == 0 || $r[4] > $max)
						{
							if($i_max != -1) //Un nouveau max, on retire le précédent
								unset($retour[$i_max]);
							$max = $r[4];
							$i_max = $i;
						} else //Pas un max, poubelle
							unset($retour[$i]);
					}
				}
			}
			return $retour;
		}
	} else if($message->type == 0) //PLAIN, texte seul ou HTML
	{
		return array(array($section, "message", $message->subtype, "seul", $message->bytes));
	} else if($message->type == 3 || $message->type == 4 || $message->type == 5 || $message->type == 6)
	{
		return array(array($section, "fichier", $message->parameters[0]->value, $message->bytes, $message->encoding));
	}
}

//Déco
imap_close($flux_imap);
?>