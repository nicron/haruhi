<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$pseudo = htmlspecialchars($_POST['pseudo']);
	$adresse = htmlspecialchars($_POST['adresse'], ENT_QUOTES);
	$telephone = htmlspecialchars($_POST['telephone'], ENT_QUOTES);
	$courriel = htmlspecialchars($_POST['courriel'], ENT_QUOTES);
	//On récupère l'ancien courriel
	$requete0 = mysql_query('SELECT titre, courriel FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
	$donnees0 = mysql_fetch_array($requete0);
	//On s'occupe d'ajouter le membre dans les bonnes ML
	if($donnees0['courriel'] != $courriel)
	{
		$requete = $pdo->query('SELECT m.adresse FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'ml m ON m.id = l.id_ml WHERE l.id_entite = '.$donnees0['titre'].' AND l.is_titre = 1');
		if(!($requete === false))
		{
			$liens = $requete->fetchAll();
			foreach($liens as $donnees)
			{
				$adresse = explode('@', $donnees['adresse']);
				$adresse = $adresse[0];
				try {
					$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

					//login
					$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

					if($donnees0['courriel'] != NULL)
						$soap->mailingListSubscriberDel($session, $domaine_ovh, $adresse, $donnees0['courriel']);
					if($courriel != NULL)
						$soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $courriel);

					//logout
					$soap->logout($session);
				} catch(SoapFault $fault) {
					echo $fault;
				}
			}
		}

		$requete = $pdo->query('SELECT m.adresse FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'ml m ON m.id = l.id_ml WHERE l.id_entite = '.$_SESSION['id_adherent'].' AND l.is_titre = 0');
		if(!($requete === false))
		{
			$liens = $requete->fetchAll();
			foreach($liens as $donnees)
			{
				$adresse = explode('@', $donnees['adresse']);
				$adresse = $adresse[0];
				try {
					$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

					//login
					$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

					if($donnees0['courriel'] != NULL)
						$soap->mailingListSubscriberDel($session, $domaine_ovh, $adresse, $donnees0['courriel']);
					if($courriel != NULL)
						$soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $courriel);

					//logout
					$soap->logout($session);
				} catch(SoapFault $fault) {
					echo $fault;
				}
			}
		}
	}
	if(strlen($prenom) > 0 && strlen($nom) > 0)
		$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET prenom = "'.$prenom.'", nom = "'.$nom.'", pseudo = "'.$pseudo.'", adresse = "'.$adresse.'", telephone = "'.$telephone.'", courriel = "'.$courriel.'" WHERE id = '.$_SESSION['id_adherent']);
	header('location: mon_compte.php');
	exit();
}

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
$user = $requete->fetch();
$mode = MODE_MODIF;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification de ses données personnelles</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Modifier ses informations</h2>

		<form action="modif_info_perso.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="prenom">Prénom</label></div>
					<div class="cellule"><input name="prenom" <?php echo 'value="'.$user['prenom'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom</label></div>
					<div class="cellule"><input id="champ_nom" name="nom" <?php echo 'value="'.$user['nom'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="pseudo">Pseudo</label></div>
					<div class="cellule"><input id="champ_pseudo" name="pseudo" <?php echo 'value="'.$user['pseudo'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="adresse">Adresse</label></div>
					<div class="cellule"><textarea name="adresse"><?php echo $user['adresse']; ?></textarea></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="telephone">Téléphone</label></div>
					<div class="cellule"><input name="telephone" <?php echo 'value="'.$user['telephone'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="courriel">Courriel</label></div>
					<div class="cellule"><input name="courriel" <?php echo 'value="'.$user['courriel'].'" '; ?>/></div>
				</div>
			</div>

			<p>
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
