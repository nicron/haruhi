<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & AVOIR_COURRIEL))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	if($_POST['mdp'] == $_POST['mdp_re'] && strlen($_POST['mdp']) >= 6 && strlen($_POST['mdp']) <= 12)
	{
		$mdp = htmlspecialchars($_POST['mdp'], ENT_QUOTES);
		$requete = mysql_query('SELECT a.nom, a.prenom, m.mail FROM '.$bdd_prefixe.'membres m LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id_membre = m.id WHERE m.id = '.$_SESSION['id']);
		$donnees = mysql_fetch_array($requete);

		try {
		$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

		$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

		if($_SESSION['type_courriel'] == 2) //Un alias existe déjà, faut le zigouiller avant toute chose
		{
			//redirectedEmailDel
			$soap->redirectedEmailDel($session, $domaine_ovh, $donnees['prenom'].'.'.$donnees['nom'], $donnees['mail'], "");
		}

		//popAdd
		$soap->popAdd($session, $domaine_ovh, $donnees['prenom'].'.'.$donnees['nom'], $mdp, "Membre bureau", $nic_ovh, "1000");

		//logout
		$soap->logout($session);

		$_SESSION['type_courriel'] = 1;
		mysql_query('UPDATE '.$bdd_prefixe.'membres SET mail = "'.$donnees['prenom'].'.'.$donnees['nom'].'@'.$domaine_ovh.'", type = 1 WHERE id = '.$_SESSION['id']);
		header('location: mon_compte.php');

		} catch(SoapFault $fault) {
		echo $fault;
		}
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Création d'un courriel</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Création d'une boite mail</h2>

	<form action="ajout_courriel.php" method="post">
	<p>
		<label name="mdp">Mot de passe : <input type="password" name="mdp" /> (Entre 6 et 12 caractères)</label><br />
		<label name="mdp_re">Mot de passe (encore) : <input type="password" name="mdp_re" /></label><br />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>