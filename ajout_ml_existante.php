<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['ovh'] || !((int)$_SESSION['permission'] & GERER_ML))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$adresse = htmlspecialchars($_POST['adresse'], ENT_QUOTES);
	
	if(isset($_POST['newsletter']) && $_POST['newsletter'] == 1)
	    $newsletter = 1;
	else
	    $newsletter = 0;
		
	if(isset($_POST['cotisation']) && $_POST['cotisation'] == 1)
	    $cotisation = 1;
	else
	    $cotisation = 0;
	
	try {
		$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

		//login
		$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

		//on créer la ML
		$liste_adresse = $soap->mailingListSubscriberList($session, $domaine_ovh, $adresse);
		mysql_query('INSERT INTO '.$bdd_prefixe.'ml (nom, adresse, newsletter, cotisation) VALUES ("'.$nom.'", "'.$adresse.'@'.$domaine_ovh.'", '.$newsletter.', '.$cotisation.')');

		$liste_ajout = [];
		$liste_ok = [];
		$liste_supp = [];
		$id_ml = mysql_insert_id();

		//On ajoute les gens qu'il faut à cette dernière
		if(isset($_POST['titre']) && count($_POST['titre']) > 0)
		{
			foreach($_POST['titre'] as $titre)
			{
				$titre = intval($titre);
				if($cotisation == 1)
					$where = ' AND date_fin_cotisation >= NOW()';
				else
					$where = '';
				$requete = mysql_query('SELECT courriel FROM '.$bdd_prefixe.'adherents WHERE titre = '.$titre.$where);
				while($donnees = mysql_fetch_array($requete))
				{
					if($donnees['courriel'] != NULL)
					{
						if(in_array($donnees['courriel'], $liste_adresse))
							$liste_ok[] = $donnees['courriel'];
						else
							$liste_ajout[] = $donnees['courriel'];
					}
				}

				mysql_query('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.$id_ml.', '.$titre.', 1);');
			}
		}

		if(isset($_POST['adherent']) && count($_POST['adherent']) > 0)
		{
			foreach($_POST['adherent'] as $adherent)
			{
				$adherent = intval($adherent);
				$requete = mysql_query('SELECT courriel FROM '.$bdd_prefixe.'adherents WHERE id = '.$adherent);
				$donnees = mysql_fetch_array($requete);
				if($donnees['courriel'] != NULL)
				{
					if(in_array($donnees['courriel'], $liste_adresse))
						$liste_ok[] = $donnees['courriel'];
					else
						$liste_ajout[] = $donnees['courriel'];
				}

				mysql_query('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.$id_ml.', '.$adherent.', 0);');
			}
		}
		
		foreach($liste_adresse as $mail)
		{
			if(!in_array($mail, $liste_ok) && !in_array($mail, $liste_ajout))
				$liste_supp[] = $mail;
		}

		foreach($liste_ajout as $mail)
			$soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $mail);
		
		foreach($liste_supp as $mail)
			$soap->mailingListSubscriberDel($session, $domaine_ovh, $adresse, $mail);

		//logout
		$soap->logout($session);
		header('location: liste_ml.php');
	} catch(SoapFault $fault) {
		echo $fault;
	}

}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajouter une mailing list</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Ajouter une Mailing List</h2>
	<form action="ajout_ml_existante.php" method="post">
	<p>Nom : <input name="nom" /><br />
	Adresse : <input name="adresse" />@<?php echo $domaine_ovh; ?><br />
	Uniquement à jour de leur cotisation ? <input type="checkbox" name="cotisation" value="1" /><br />
	Liste d'information ? <input type="checkbox" name="newsletter" value="1" /><br />
	Membres de la ML :<br />
	Titres :<br />
	<select multiple name="titre[]">
	<?php $requete = mysql_query('SELECT id, nom FROM '.$bdd_prefixe.'titre');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
			echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
	?>
	</select><br />
	Adherents :<br />
	<select multiple name="adherent[]">
	<?php $requete = mysql_query('SELECT id, nom, prenom FROM '.$bdd_prefixe.'adherents');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
			echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].' '.$donnees['prenom'].'</option>';
	}
	?>
	</select><br />
	<input type="hidden" name="envoi" value="1" />
	<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>