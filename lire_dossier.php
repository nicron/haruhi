<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & LIRE_DOSSIER))
	header('location: index.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: dossiers.php');

$id = intval($_GET['id']);

$requete = mysql_query('SELECT *, d.id AS id, e.prenom AS prenom_enfant, e.nom AS nom_enfant, a.prenom AS prenom_intervenant, a.nom AS nom_intervenant
					FROM '.$bdd_prefixe.'dossiers d
					LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = d.intervenant
					LEFT JOIN '.$bdd_prefixe.'enfants e ON e.id = d.enfant WHERE d.id = '.$id);
$donnees = mysql_fetch_array($requete);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Afficher un dossier</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="edition_facture.css" type="text/css" media="print" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<nav id="panneau_page">
	<h2><a href="dossiers.php">Dossier</a></h2>
	</nav>

	<div id="cadre_stockage">
	<p>Intervenant : <?php echo $donnees['prenom_intervenant'].' '.$donnees['nom_intervenant']; ?><br />
	Enfant : <?php echo $donnees['prenom_enfant'].' '.$donnees['nom_enfant']; ?><br />
	Date : <?php echo formater_date($donnees['date']); ?><br />
	<?php echo nl2br($donnees['message']); ?>
	</p>
	</div>
	<?php include('bas_page.php'); ?>
	</body>
</html>