<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['ovh'] || !((int)$_SESSION['permission'] & GERER_ML))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	$requete = mysql_query('SELECT adresse FROM '.$bdd_prefixe.'ml WHERE id = '.$id);
	$donnees = mysql_fetch_array($requete);
	$explode = explode('@', $donnees['adresse']);
	$adresse = $explode[0];
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	
	if(isset($_POST['newsletter']) && $_POST['newsletter'] == 1)
	    $newsletter = 1;
	else
	    $newsletter = 0;
	
	if(isset($_POST['cotisation']) && $_POST['cotisation'] == 1)
	    $cotisation = 1;
	else
	    $cotisation = 0;
	
	mysql_query('UPDATE '.$bdd_prefixe.'ml SET nom = "'.$nom.'", newsletter = "'.$newsletter.'", cotisation = "'.$cotisation.'" WHERE id = '.$id);
	
	if(isset($_POST['modif_config']) && $_POST['modif_config'] == 1)
	{
		$proprio = htmlspecialchars($_POST['proprio'], ENT_QUOTES);
		$reponse = htmlspecialchars($_POST['reponse'], ENT_QUOTES);
		if(isset($_POST['moderer_msg']) && $_POST['moderer_msg'] == 1)
			$moderer_msg = true;
		else
			$moderer_msg = false;
		if(isset($_POST['membres_msg']) && $_POST['membres_msg'] == 1)
			$membres_msg = true;
		else
			$membres_msg = false;
		if(isset($_POST['moderer_inscription']) && $_POST['moderer_inscription'] == 1)
			$moderer_inscription = true;
		else
			$moderer_inscription = false;
	
		try {
			$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

			//login
			$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

			$soap->mailingListModify($session, $domaine_ovh, $adresse, $proprio, "fr", $reponse, $moderer_msg, $membres_msg, $moderer_inscription);
			
			//logout
			$soap->logout($session);
		} catch(SoapFault $fault) {
			echo $fault;
		}
	}
	
	//On supprime les gens qu'on doit supprimer
	if(isset($_POST['titre_supp']) && count($_POST['titre_supp']) > 0)
	{
		foreach($_POST['titre_supp'] as $id_lien)
		{
			mysql_query('DELETE FROM '.$bdd_prefixe.'ml_lien WHERE id = '.$id_lien);
		}
	}

	if(isset($_POST['adherent_supp']) && count($_POST['adherent_supp']) > 0)
	{
		foreach($_POST['adherent_supp'] as $id_lien)
		{
			mysql_query('DELETE FROM '.$bdd_prefixe.'ml_lien WHERE id = '.$id_lien);
		}
	}
	//On ajoute les gens qu'il faut à cette dernière
	if(isset($_POST['titre']) && count($_POST['titre']) > 0)
	{
		foreach($_POST['titre'] as $titre)
		{
			mysql_query('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.$id_ml.', '.$titre.', 1);');
		}
	}

	if(isset($_POST['adherent']) && count($_POST['adherent']) > 0)
	{
		foreach($_POST['adherent'] as $adherent)
		{
			mysql_query('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.$id_ml.', '.$adherent.', 0);');
		}
	}
	
	header('location: synchro_ml.php?id='.$id_ml);

} elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: liste_ml.php');

$id = intval($_GET['id']);

$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'ml WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modifier une mailing list</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Modifier une Mailing List</h2>
	<form action="modif_ml.php" method="post">
	<p>Nom : <input name="nom" value="<?php echo $donnees['nom']; ?>" /><br />
	Adresse : <?php echo $donnees['adresse']; ?><br />
	Liste d'information ? <input type="checkbox" name="newsletter" value="1" <?php if($donnees['newsletter'] == 1) echo 'checked'; ?> /><br />
	Uniquement à jour de leur cotisation ? <input type="checkbox" name="cotisation" value="1" <?php if($donnees['cotisation'] == 1) echo 'checked'; ?> /><br />
	Modifier la configuration de la mailing list : <input type="checkbox" name="modif_config" value="1" /><br />
	Courriel du propriétaire : <input name="proprio" /><br />
	Réponse à : <select name="reponse"><option value="mailinglist">À la Mailing List</option><option value="lastuser">À l'auteur</option></select><br />
	Modérer les messages ? <input type="checkbox" name="moderer_msg" value="1" /><br />
	Seuls les membres peuvent envoyer des messages <input type="checkbox" name="membres_msg" value="1" /><br />
	Inscriptions contrôlées ? <input type="checkbox" name="moderer_inscription" value="1" /><br />
	Membres de la ML :<br />
	Titres :<br />
	<?php
	$requete2 = mysql_query('SELECT l.id, l.id_entite, t.nom FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'titre t ON t.id = l.id_entite WHERE l.is_titre = 1 AND l.id_ml = '.$id);
	$titre = array();
	while($donnees2 = mysql_fetch_array($requete2))
	{
		echo $donnees2['nom'].' <input type="checkbox" name="titre_supp[]" value="'.$donnees2['id'].'" /> Supprimer<br />';
		$titre[] = $donnees2['id_entite'];
	}
	?>
	<select multiple name="titre[]">
	<?php $requete = mysql_query('SELECT id, nom FROM '.$bdd_prefixe.'titre');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
		{
			if(in_array($donnees['id'], $titre) === false)
				echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
		}
	}
	?>
	</select><br />
	Adherents :<br />
	<?php
	$requete3 = mysql_query('SELECT l.id, l.id_entite, a.nom, a.prenom FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = l.id_entite WHERE l.is_titre = 0 AND l.id_ml = '.$id);
	$adherent = array();
	while($donnees3 = mysql_fetch_array($requete3))
	{
		echo $donnees3['nom'].' '.$donnees3['prenom'].' <input type="checkbox" name="adherent_supp[]" value="'.$donnees3['id'].'" /> Supprimer<br />';
		$adherent[] = $donnees3['id_entite'];
	}
	?>
	<select multiple name="adherent[]">
	<?php $requete = mysql_query('SELECT id, nom, prenom FROM '.$bdd_prefixe.'adherents');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
		{
			if(in_array($donnees['id'], $adherent) === false)
				echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].' '.$donnees['prenom'].'</option>';
		}
	}
	?>
	</select><br />
	<input type="hidden" name="envoi" value="1" />
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>