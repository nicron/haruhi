<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$classe = htmlspecialchars($_POST['classe']);
	$id = intval($_POST['enfant']);
	$parent = intval($_POST['parent']);
	$intervenant = intval($_POST['intervenant']);
	$naissance = htmlspecialchars($_POST['naissance'], ENT_QUOTES);
	mysql_query('UPDATE '.$bdd_prefixe.'enfants SET prenom = "'.$prenom.'", nom = "'.$nom.'", classe = "'.$classe.'", naissance = "'.$naissance.'", id_intervenant = "'.$intervenant.'" WHERE id = '.$id);
	//header('location: enfant.php?id='.$parent);
	header('location: voir_adherent.php?id='.$parent);
} elseif(isset($_GET['supp']) && intval($_GET['supp']) > 0)
{
	$enfant = intval($_GET['supp']);
	$requete = mysql_query('SELECT id_parent FROM '.$bdd_prefixe.'enfants WHERE id = '.$enfant);
	$donnees = mysql_fetch_array($requete);
	$parent = $donnees['id_parent'];
	mysql_query('DELETE FROM '.$bdd_prefixe.'enfants WHERE id = '.$enfant);
	//header('location: enfant.php?id='.$parent);
	header('location: voir_adherent.php?id='.$parent);
} elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: adherents.php');

$id = intval($_GET['id']);

$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'enfants WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification d'un enfant</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Modification un enfant</h2>

	<form action="modif_enfant.php" method="post">
	<p>
		<label name="nom">Nom : <input name="nom" value="<?php echo $donnees['nom']; ?>" /></label><br />
		<label name="prenom">Prénom : <input name="prenom" value="<?php echo $donnees['prenom']; ?>" /></label><br />
		<label name="classe">Classe : <input name="classe" value="<?php echo $donnees['classe']; ?>" /></label><br />
		<label name="naissance">Date de naissance : <input name="naissance" value="<?php echo $donnees['naissance']; ?>" /> (Format : AAAA-MM-JJ)</label><br />
		Intervenant : <select name="intervenant">
		<option value="0">Aucun</option>
		<?php $requete2 = mysql_query('SELECT * FROM '.$bdd_prefixe.'adherents');
		if(!($requete2 === false))
		{
			while($donnees2 = mysql_fetch_array($requete2))
			{
				echo '<option value="'.$donnees2['id'].'"';
				if($donnees2['id'] == $donnees['id_intervenant'])
					echo ' selected ';
				echo '>'.$donnees2['prenom'].' '.$donnees2['nom'].'</option>';
			}
		} ?>
		</select><br />
		<input type="hidden" name="enfant" value="<?php echo $id; ?>" />
		<input type="hidden" name="parent" value="<?php echo $donnees['id_parent']; ?>" />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>
