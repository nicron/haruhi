<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: stockage.php');

if(!$fonctionnalites_statut['stockage'])
	header('location: index.php');

require('configuration.php');
$id = intval($_GET['id']);

$requete = mysql_query('SELECT nom, taille, version, visibilite, pseudo, auteur FROM '.$bdd_prefixe.'fichiers f LEFT JOIN '.$bdd_prefixe.'membres m ON m.id = f.auteur WHERE f.id = '.$id);
$donnees = mysql_fetch_array($requete);

$type = type_fichier($donnees['nom']);
$coupe = explode('.', $donnees['nom']);
$coupe[count($coupe) - 2] .= '_v'.$donnees['version'];
$nom_fichier = implode('.', $coupe);

//On va humaniser la lecture de la taille
function taille_humaine($taille)
{
	if($taille >= 1073741824)
		return number_format(($taille / 1073741824), 2, ',', ' ').' Gio';
	elseif($taille >= 1048576)
		return number_format(($taille / 1048576), 2, ',', ' ').' Mio';
	elseif($taille >= 1024)
		return number_format(($taille / 1024), 2, ',', ' ').' Kio';
	else
		return number_format(($taille), 2, ',', ' ').' o';
}

function type_fichier($fichier)
{
	$explode = explode('.', $fichier);
	$ext = strtolower($explode[count($explode) - 1]);
	switch($ext)
	{
		case 'png':
		case 'jpg':
		case 'gif':
		case 'bmp':
		case 'jpeg':
			return 'Image';
			break;
		case 'svg':
			return 'Image vectorielle';
			break;
		case 'doc':
			return 'Document Texte';
			break;
		case 'odt':
		case 'ods':
			return 'Open Document';
			break;
		case 'pdf':
			return 'PDF';
			break;
		default:
			return 'Autre';
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Stockage</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<nav id="panneau_page">
			<h2><a href="stockage.php">Stockage</a></h2>
			<p>
				<?php echo $donnees['nom'].'<br />';
				echo 'Taille : '.taille_humaine($donnees['taille']).'<br />';
				echo 'Envoyé par : ';
				if($donnees['auteur'] == '-1')
					echo 'Yuki';
				else
					echo $donnees['pseudo'];
				echo '<br />';
				echo 'Version '.$donnees['version'].'<br />';
				echo '<a href="afficher_fichier.php?id='.$id.'">Télécharger</a>';
				?>
			</p>
			<?php if($type == 'PDF') { ?>
			<div>
				<a onclick="allerPage(1)"><img src="images/stockage_debut.png" alt="Début" title="Début" /></a>
				<a id="prev" onclick="goPrevious()"><img src="images/stockage_precedent.png" alt="Précédent" title="Précédent" /></a>
				<a id="next" onclick="goNext()"><img src="images/stockage_suivant.png" alt="Suivant" title="Suivant" /></a>
				<a onclick="allerPage(-1)"><img src="images/stockage_fin.png" alt="Fin" title="Fin" /></a><br />
				<a onclick="zoom1();"><img src="images/stockage_zoom1.png" alt="Zoom 1" title="Zoom 1"/></a>
				<a onclick="zoomAvant()"><img src="images/stockage_zoomavant.png" alt="Zoom Avant" title="Zoom Avant" /></a>
				<a onclick="zoomArriere()"><img src="images/stockage_zoomarriere.png" alt="Zoom Arrière" title="Zoom Arrière" /></a>
				<br />
				<span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
			</div>
		</nav>

		<div id="cadre_stockage">
			<?php 
	switch($type)
	{
		case 'Image':
			echo '<img src="fichiers/'.$nom_fichier.'" alt="Image" />';
			break;
		case 'PDF':
			?>
			<div>
				<canvas id="the-canvas" style="border:1px solid black"></canvas>
			</div>

			<!-- Use latest PDF.js build from Github -->
			<script type="text/javascript" src="pdf.js"></script>

			<script type="text/javascript">
				var url = '<?php echo 'fichiers/'.$nom_fichier; ?>';
				PDFJS.disableWorker = true;

				var pdfDoc = null,
						pageNum = 1,
						scale = 0.7,
						canvas = document.getElementById('the-canvas'),
						ctx = canvas.getContext('2d');

				//
				// Get page info from document, resize canvas accordingly, and render page
				//
				function renderPage(num) {
					// Using promise to fetch the page
					pdfDoc.getPage(num).then(function(page) {
						var viewport = page.getViewport(scale);
						canvas.height = viewport.height;
						canvas.width = viewport.width;

						// Render PDF page into canvas context
						var renderContext = {
							canvasContext: ctx,
							viewport: viewport
						};
						page.render(renderContext);
					});

					// Update page counters
					document.getElementById('page_num').textContent = pageNum;
					document.getElementById('page_count').textContent = pdfDoc.numPages;
				}

				//
				// Go to previous page
				//
				function goPrevious() {
					if (pageNum <= 1)
						return;
					pageNum--;
					renderPage(pageNum);
				}

				//
				// Go to next page
				//
				function goNext() {
					if (pageNum >= pdfDoc.numPages)
						return;
					pageNum++;
					renderPage(pageNum);
				}

				function allerPage(num)
				{
					if(num > 0)
						pageNum = num;
					else if(num < 0)
						pageNum = pdfDoc.numPages + num + 1;
					else
						return;
					renderPage(pageNum);
				}

				function zoomAvant()
				{
					if(scale < 4)
						scale += 0.1;
					else
						return;
					renderPage(pageNum);
				}

				function zoom1()
				{
					scale = 1;
					renderPage(pageNum);
				}

				function zoomArriere()
				{
					if(scale > 0.1)
						scale -= 0.1;
					else
						return;
					renderPage(pageNum);
				}

				//
				// Asynchronously download PDF as an ArrayBuffer
				//
				PDFJS.getDocument(url).then(function getPdfHelloWorld(_pdfDoc) {
					pdfDoc = _pdfDoc;
					renderPage(pageNum);
				});
			</script>  
			<?php break;
			/* case 'Image vectorielle':
		echo '<svg src="fichiers/'.$nom_fichier.'" />';
		break;*/
		default:
			echo 'Aucun aperçu disponible';
			break;
	}
			?>
		</div>
		<?php include('bas_page.php'); ?>
	</body>
</html>