<?php

	$host = "localhost";
	$username = "root";
	$password = "";
	$bdd_name = "haruhi";
	$bdd_prefixe = "haruhi_";

	//OVH
	$nic_ovh = "";
	$mdp_ovh = "";
	$domaine_ovh = "";

	//Yuki
	$serveur_yuki = "";
	$courriel_yuki = "";
	$mdp_yuki = "";

	//Mikuru
	$adresse_mikuru = "";

	//Questions tutorats :
	$questions_tutorat = array();

	//Fonctionnalités
	$fonctionnalites_statut = array(
		"ovh" => true,
		"yuki" => false,
		"mikuru" => false,
		"intervenants" => false,
		"factures" => true,
		"stockage" => false,
		"calendrier" => false,
		"avatar" => false,
		"cartemembre" => false
	);

	//Infos diverses
	$infos_siteasso = "";
	$infos_nomasso = "";
	$infos_adresseasso = "";
?>
