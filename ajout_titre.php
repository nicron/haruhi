<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);

	if(substr($_POST['couleur'], 0, 1) == '#')
	    $couleur = '#'.substr($_POST['couleur'], 1);
	else
	    $couleur = htmlspecialchars($_POST['couleur']);

	if(strlen($nom) > 0)
		mysql_query('INSERT INTO '.$bdd_prefixe.'titre (nom, couleur) VALUE ("'.$nom.'", "'.$couleur.'")');
	header('location: titres.php');
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajouter un titre</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
		<style type="text/css">
		.test_couleur
		{
		    border: 1px solid black;
		    width: 12px;
		    height: 12px;
		    display: inline-block;
		    cursor: pointer;
		}
		
		#case_couleur
		{
		    border: 1px dashed black;
		    width: 12px;
		    height: 12px;
		    display: inline-block;
		}
		</style>

		<script type="text/javascript">
		    function maj_couleur()
		    {
			document.getElementById('case_couleur').style.backgroundColor = document.getElementById('couleur').value;
		    }
		    
		    function choisir_couleur(nom)
		    {
			document.getElementById('couleur').value = nom;
			maj_couleur();
		    }
		</script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Ajouter un titre</h2>

	<form action="ajout_titre.php" method="post">
	<p>
		<label name="nom">Nom : <input name="nom" /></label><br />
		<label name="couleur">Couleur : <input name="couleur" type="color" id="couleur" onChange="maj_couleur();" /></label> <span id="case_couleur"></span>
	</p>
		<div class="test_couleur" onClick="choisir_couleur('#46B8F1');" style="background-color:#46B8F1"></div>
		<div class="test_couleur" onClick="choisir_couleur('#ffedb0');" style="background-color:#ffedb0"></div>
		<div class="test_couleur" onClick="choisir_couleur('#f0f0f0');" style="background-color:#f0f0f0"></div>
		<div class="test_couleur" onClick="choisir_couleur('#F0BDE5');" style="background-color:#F0BDE5"></div>
		<div class="test_couleur" onClick="choisir_couleur('#d6d6d6');" style="background-color:#d6d6d6"></div>
		<div class="test_couleur" onClick="choisir_couleur('#FFB5B5');" style="background-color:#FFB5B5"></div>
		<div class="test_couleur" onClick="choisir_couleur('#FFE991');" style="background-color:#FFE991"></div>
		<div class="test_couleur" onClick="choisir_couleur('#CFFECC');" style="background-color:#CFFECC"></div>
	<p>
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
		
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>