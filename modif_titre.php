<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	if(substr($_POST['couleur'], 0, 1) == '#')
	    $couleur = '#'.substr($_POST['couleur'], 1);
	else
	    $couleur = htmlspecialchars($_POST['couleur']);

	if(strlen($nom) > 0)
		mysql_query('UPDATE '.$bdd_prefixe.'titre SET nom = "'.$nom.'", couleur = "'.$couleur.'" WHERE id = '.$id);
	header('location: titres.php');
} elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: titres.php');

$id = intval($_GET['id']);
$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'titre WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification de titre</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
		<style type="text/css">
		.test_couleur
		{
		    border: 1px solid black;
		    width: 12px;
		    height: 12px;
		    display: inline-block;
		    cursor: pointer;
		}
		
		#case_couleur
		{
		    border: 1px dashed black;
		    width: 12px;
		    height: 12px;
		    display: inline-block;
		}
		</style>

		<script type="text/javascript">
		    function maj_couleur()
		    {
			document.getElementById('case_couleur').style.backgroundColor = document.getElementById('couleur').value;
		    }
		    
		    function choisir_couleur(nom)
		    {
			document.getElementById('couleur').value = nom;
			maj_couleur();
		    }
		</script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Modifier un titre</h2>

	<form action="modif_titre.php" method="post">
	<p>
		<label name="nom">Nom : <input name="nom" value="<?php echo $donnees['nom']; ?>" /></label><br />
		<label name="couleur">Couleur : <input name="couleur" type="color" id="couleur" value="<?php echo $donnees['couleur']; ?>" onChange="maj_couleur();" /></label>
		<span id="case_couleur"></span>
	</p>
		<div class="test_couleur" onClick="choisir_couleur('#46B8F1');" style="background-color:#46B8F1"></div>
		<div class="test_couleur" onClick="choisir_couleur('#ffedb0');" style="background-color:#ffedb0"></div>
		<div class="test_couleur" onClick="choisir_couleur('#f0f0f0');" style="background-color:#f0f0f0"></div>
		<div class="test_couleur" onClick="choisir_couleur('#F0BDE5');" style="background-color:#F0BDE5"></div>
		<div class="test_couleur" onClick="choisir_couleur('#d6d6d6');" style="background-color:#d6d6d6"></div>
		<div class="test_couleur" onClick="choisir_couleur('#FFB5B5');" style="background-color:#FFB5B5"></div>
		<div class="test_couleur" onClick="choisir_couleur('#FFE991');" style="background-color:#FFE991"></div>
		<div class="test_couleur" onClick="choisir_couleur('#CFFECC');" style="background-color:#CFFECC"></div>
	<p>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>