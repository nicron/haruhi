<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & VOIR_FACTURE))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Reçus</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2>Reçus</h2>
	    <?php if((int)$_SESSION['permission'] & FAIRE_FACTURE) { ?>
	    <p><a href="ajout_recu.php">Ajouter un reçu</a></p>
	    <?php }
		/*if((int)$_SESSION['permission'] & GERER_TYPE_FACTURE) { ?>
	    <p><a href="gerer_type_factures.php">Gérer les types de factures</a></p>
	    <?php } */ ?>
	</nav>

	<div id="cadre_stockage">
	<table>
	<tr><th>N°</th><th>Adhérent</th><th>Montant</th><th>Date</th><th>Moyen de paiement</th><!--<th>Type</th>--><th>Actions</th></tr>
	<?php $requete = mysql_query('SELECT *, r.id AS id, a.nom AS nom
FROM '.$bdd_prefixe.'recu r LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = f.adherent');
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				if((int)$_SESSION['permission'] & FAIRE_FACTURE)
				{
					echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['prenom'].' '.$donnees['nom'].'</td><td>'.number_format($donnees['somme'], 2, ',', ' ').' €</td><td>'.formater_date($donnees['date']).'</td>';
					echo '<td>'.$donnees['payement'].'</td>';
					echo '<td><a href="afficher_recu.php?id='.$donnees['id'].'"><img src="images/stockage_voir.png" title="Afficher" alt="Afficher" /></a>';
					/*echo ' <a href="factures.php?visibilite='.$donnees['id'].'">';
					if($donnees['visibilite'] == 0)
						echo '<img src="images/facture_afficher.png" title="Rendre visible" alt="Rendre visible" />';
					else
						echo '<img src="images/facture_masquer.png" title="Masquer" alt="Masquer" />';
					echo '</a>';*/
					echo '</td></tr>';
				} elseif($donnees['visibilite'] == 1)
				{
					echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['prenom'].' '.$donnees['nom'].'</td><td>'.number_format($donnees['somme'], 2, ',', ' ').' €</td><td>'.formater_date($donnees['date']).'</td>';
					echo '<td>'.$donnees['payement'].'</td>';
					//echo '<td>'.$donnees['nom_type_facture'].'</td>';
					echo '<td><a href="afficher_recu.php?id='.$donnees['id'].'"><img src="images/stockage_voir.png" title="Afficher" alt="Afficher" /></a>';
					echo '</td></tr>';
				}
			}
		}
	?>
	</table>
	</div>
	<?php include('bas_page.php'); ?>
	</body>
</html>