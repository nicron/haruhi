<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Mes factures</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Mes factures</h2>

	<table>
	<tr><th>N°</th><th>Montant</th><th>Date</th><th>Moyen de paiement</th><th>Actions</th></tr>
	<?php
		$requete = $pdo->query('SELECT *, f.id AS id FROM '.$bdd_prefixe.'factures f
			INNER JOIN '.$bdd_prefixe.'factures_type t ON t.id = f.type
			WHERE adherent = '.$_SESSION['id_adherent'].' AND visibilite = 1');
		if(!($requete === false))
		{
			$factures = $requete->fetchAll();
			foreach($factures as $facture)
			{
				echo '<tr><td>'.$facture['id'].'</td><td>'.number_format($facture['somme'], 2, ',', ' ').' €</td><td>'.formater_date($facture['date']).'</td><td>'.$facture['payement'].'</td>';
				echo '<td>';
							if($facture['externe'] == 0) //Facture interne
								echo '<a href="afficher_facture.php?id='.$facture['id'].'">';
							else //Facture externe
								echo '<a target="_blank" href="'.$facture['lien'].'">';
							echo 'Afficher</a></td></tr>';
			}
		}
	?>
	</table>

	<?php include('bas_page.php'); ?>
	</body>
</html>
