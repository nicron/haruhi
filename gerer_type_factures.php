<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['factures'] || !((int)$_SESSION['permission'] & GERER_TYPE_FACTURE))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Gérer les types de factures</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Types de facture</h2>

	<table>
	<tr><th>Id</th><th>Nom</th><th>Externe</th><th>Actions</th></tr>
	<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'factures_type');
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<tr>';
				echo '<td>'.$donnees['id'].'</td>';
				echo '<td>'.$donnees['nom'].'</td>';
				echo '<td>'.($donnees['externe'] ? 'Oui' : 'Non').'</td>';
				echo '<td><a href="form_facture_type.php?id='.$donnees['id'].'">Modifier</a></td>';
				echo '</tr>';
			}
		}
	?>
	</table>

	<p><a href="form_facture_type.php">Ajouter un type</a></p>
	<?php include('bas_page.php'); ?>
	</body>
</html>
