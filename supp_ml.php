<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['ovh'] || !((int)$_SESSION['permission'] & GERER_ML))
	header('location: index.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: liste_ml.php');

$id = intval($_GET['id']);
$requete = mysql_query('SELECT adresse FROM '.$bdd_prefixe.'ml WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);
$explode = explode('@', $donnees['adresse']);
$adresse = $explode[0];
try {
	$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

	//login
	$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

	$soap->mailingListDel($session, $domaine_ovh, $adresse);

	mysql_query('DELETE FROM '.$bdd_prefixe.'ml WHERE id = '.$id);
	mysql_query('DELETE FROM '.$bdd_prefixe.'ml_lien WHERE id_ml = '.$id);

	//logout
	$soap->logout($session);

	header('location: liste_ml.php');
} catch(SoapFault $fault) {
	echo $fault;
}
?>
