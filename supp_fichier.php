<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['stockage'] || !((int)$_SESSION['permission'] & ENVOYER_FICHIER))
	header('location: index.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: stockage.php');

function supprimer_dossier($adresse_dossier)
{
    $ens_fichier = scandir($adresse_dossier);
    foreach($ens_fichier as $i=>$f)
    {
	if($i >= 2) //On enlève . et ..
	{
	    if(is_dir($adresse_dossier.$f)) //C'est un dossier → Récursif
		supprimer_dossier($adresse_dossier.$f.'/');
	    else //fichier
		unlink($adresse_dossier.$f);
	}
    }
    rmdir($adresse_dossier);
}

$id = intval($_GET['id']);

$requete = mysql_query('SELECT nom, version, auteur FROM '.$bdd_prefixe.'fichiers WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);

if($donnees['auteur'] != $_SESSION['id'])
	header('location: stockage.php');

$coupe = explode('.', $donnees['nom']);
$coupe[count($coupe) - 2] .= '_v'.$donnees['version'];
$nom_fichier = implode('.', $coupe);

if(strtolower($coupe[count($coupe) - 1]) == 'odt') //On supprime les aperçus
    supprimer_dossier('fichiers/'.$nom_fichier.'_apercu/');

unlink('fichiers/'.$nom_fichier);

mysql_query('DELETE FROM '.$bdd_prefixe.'fichiers WHERE id = '.$id);
header('location: stockage.php');
?>