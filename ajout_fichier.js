var zone_drop;
var jeton = 0; //permet de savoir si on peut recharger la page ou pas
var liste_fichiers = new Array();

function init_dial()
{
  zone_drop = document.getElementById('zone_envoi');
zone_drop.addEventListener('dragleave', sortie_drap, false);
zone_drop.addEventListener('dragenter', entree_drap, false);
zone_drop.addEventListener('dragover', encours_drap, false);
zone_drop.addEventListener('drop', depot, false);
}

function ouvrir_dial_fichier(id)
{
	document.getElementById('dial_ajout_fichier').style.display = 'block';
}

function fermer_dial_fichier()
{
	document.getElementById('dial_ajout_fichier').style.display = 'none';
}

function entree_drap(event)
{
    event.preventDefault();
    event.stopPropagation();
    zone_drop.className = 'drop_encours';
}

function sortie_drap(event)
{
    event.preventDefault();
    event.stopPropagation();
    zone_drop.className = 'drop_pas';
}

function encours_drap(event)
{
    event.preventDefault();
    event.stopPropagation();
    zone_drop.className = 'drop_encours';
    /*event.dataTransfer.effectAllowed= "copy";
    event.dataTransfer.dropEffect = "copy";*/
}

function depot(event)
{
    event.preventDefault();
    event.stopPropagation();
    zone_drop.className = 'drop_pas';
    var fichiers = event.dataTransfer.files;

  if (fichiers.length == 0)
  {
    // Pas de fichiers : bizarre
    return;
  } else
  {
    //envoyer_fichiers(fichiers);
    for(var i = 0, f; f = fichiers[i]; i++)
      liste_fichiers.push(f);
    maj_liste_fichiers();
  }
}

function choix_fichier_dial(event)
{
  //alert(document.getElementById('champ_dial_fichier').value);
  fichiers = event.target.files;
  for(var i = 0, f; f = fichiers[i]; i++)
    liste_fichiers.push(f);
  maj_liste_fichiers();
}

function envoyer_fichiers()
{
    liste = liste_fichiers;
    jeton += liste.length;
    for (var i = 0 ; i < liste.length ; i++)
    {
      var fichier = liste[i];
      //alert("name="+fichier.name+"/size="+fichier.size);
      envoi(fichier);
    }
    liste_fichiers = new Array();
}

function maj_liste_fichiers()
{
  var div = '';
  for(var i = 0; i < liste_fichiers.length; i++)
  {
    var f = liste_fichiers[i];
      div += '<tr><td><strong>'+ escape(f.name)+ '</strong></td><td>'+ taille_humain(f.size)+ '</td><td><a onClick="supprimer_fichier_liste('+i+')" style="cursor: pointer;">Supp</a></td></tr>';
  }
  document.getElementById('table_en_cours_envoi').innerHTML = div;
}

function taille_humain(taille) {
	if(taille >= 1073741824)
		return (Math.floor((taille / 1073741824) * 100) / 100) + ' Gio';
	else if(taille >= 1048576)
		return (Math.floor((taille / 1048576) * 100) / 100) + ' Mio';
	else if(taille >= 1024)
		return (Math.floor((taille / 1024) * 100) / 100) + ' Kio';
	else
		return taille+ ' o';
}

function supprimer_fichier_liste(num)
{
  var nvelle_liste = new Array();
  for(var i = 0; i < liste_fichiers.length; i++)
  {
    if(i != num)
      nvelle_liste.push(liste_fichiers[i]);
  }
  liste_fichiers = nvelle_liste;
  maj_liste_fichiers();
}

function envoi(fichier) {
        var xhr = new XMLHttpRequest();
        
        //xhr.upload.addEventListener("progress",  onUploadProgress, false);
        xhr.open("POST", "ajouter_fichier.php", true);

        // on s'abonne à tout changement de statut pour détecter
        // une erreur, ou la fin de l'upload
        xhr.onreadystatechange = function(){
	  if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
	      jeton -= 1;
	      if(jeton == 0)
		document.location.reload();
	  } 
	}; 
	
	var formData = new FormData();
 
	// Ajout de clés/valeurs
	formData.append("envoi", 1);
	formData.append("fichier", fichier); // Le fichier brut
	formData.append('id', 0);
	formData.append('visibilite', document.getElementById('cs_visibilite').value);

        xhr.send(formData);
}
/*
function onUploadProgress(event) {
         if (event.lengthComputable) {
                 yourProgressBar.style.width = (event.loaded / event.total) * 100 + "%";
         }
}*/