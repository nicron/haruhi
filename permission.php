<?php
define ('PAGE_INTERVENANT', 0x01);
define ('LIRE_DOSSIER', 0x02);
define ('LISTE_ADHERENT', 0x04);
define ('TOUCHE_ADHERENT', 0x08);
define ('VOIR_FACTURE', 0x10);
define ('FAIRE_FACTURE', 0x20);
define ('ENVOYER_FICHIER', 0x40);
define ('AVOIR_COURRIEL', 0x80);
define ('MODIFIER_PERMISSION', 0x100);
define ('TOUCHE_CALENDRIER', 0x200);
define ('GERER_ML', 0x400);
define ('GERER_TYPE_FACTURE', 0x800);
define ('CONSULTER_ML', 0x1000);
define ('CARTE_ADHERENT', 0x2000);
?>
