<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');

require('configuration.php');

if(!((int)$_SESSION['permission'] & AVOIR_COURRIEL))
	header('location: index.php');
	
if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
	if($_POST['mdp'] == $_POST['mdp_re'] && strlen($_POST['mdp']) > 0)
	{
		$mdp = htmlspecialchars($_POST['mdp']);
	} else {
		header('location: mon_compte.php');
		exit();
	}
	
	if(strlen($pseudo) > 0 && !in_array(strtolower($pseudo), array('haruhi', 'kyon', 'mikuru', 'yuki', 'itsuki')))
	{
		//On récupère le courriel du membre
		$requete = mysql_query('SELECT mail FROM '.$bdd_prefixe.'membres WHERE id = '.$_SESSION['id']);
		$donnees = mysql_fetch_array($requete);
		mysql_query('INSERT INTO users (username,digesta1)
		      VALUES("'.$pseudo.'", "'.md5($pseudo.':Haruhi:'.$mdp).'");') or die(mysql_error());
		mysql_query('INSERT INTO principals (uri,email,displayname)
		      VALUES ("principals/'.$pseudo.'", "'.$donnees['mail'].'","Compte de '.$_SESSION['pseudo'].'");') or die(mysql_error());
		$id_principal = mysql_insert_id();
		mysql_query('INSERT INTO principals (uri,email,displayname)
		      VALUES ("principals/'.$pseudo.'/calendar-proxy-read", null, null);') or die(mysql_error());
		$id_lecture = mysql_insert_id();
		mysql_query('INSERT INTO principals (uri,email,displayname)
		      VALUES ("principals/'.$pseudo.'/calendar-proxy-write", null, null);') or die(mysql_error());
		mysql_query('INSERT INTO calendars (principaluri, displayname, uri, description, components, ctag, transparent) VALUES
		      ("principals/'.$pseudo.'","Calendrier perso de '.$pseudo.'","'.$pseudo.'-perso","","VEVENT,VTODO",1, 0);') or die(mysql_error());
		 //     echo 'init ok';
		//Faut ajouter les droits
		//Calendrier commun
		$requete1 = mysql_query('SELECT id from principals where uri= "principals/mikuru/calendar-proxy-write"');
		$donnees1 = mysql_fetch_array($requete1);
		/*$requete2 = mysql_query('SELECT id from principals where uri= "principals/mikuru/calendar-proxy-read"');
		$donnees2 = mysql_fetch_array($requete2);*/
		mysql_query('INSERT INTO groupmembers (principal_id, member_id) VALUES ('.$donnees1['id'].', '.$id_principal.')') or die(mysql_error());
		//mysql_query('INSERT INTO groupmembers (principal_id, member_id) VALUES ('.$donnees2['id'].', '.$donnees0['id'].')') or die(mysql_error());
		//echo 'droits calendrier co ok';
		//Calendrier des autres
		$requete = mysql_query('SELECT id from principals WHERE uri != "principals/'.$pseudo.'/calendar-proxy-read" AND uri LIKE "%calendar-proxy-read"');
		while($donnees = mysql_fetch_array($requete))
		{
			mysql_query('INSERT INTO groupmembers (principal_id, member_id) VALUES ('.$donnees['id'].', '.$id_principal.')') or die(mysql_error());
		}
		//echo 'calendrier autres ok';
		//Les autres sur mon calendrier
		$requete3 = mysql_query('SELECT id from principals where uri LIKE "principals/%" AND uri NOT LIKE "%calendar-proxy-read"
		AND uri NOT LIKE "%calendar-proxy-write" AND id != '.$id_principal);
		while($donnees3 = mysql_fetch_array($requete3))
		{
			mysql_query('INSERT INTO groupmembers (principal_id, member_id) VALUES ('.$id_lecture.', '.$donnees3['id'].')') or die(mysql_error());
		}
		//echo 'autres calendrier ok';
		//On le met dans haruhi
		mysql_query('UPDATE '.$bdd_prefixe.'membres SET mikuru = "'.$pseudo.'" WHERE id = '.$_SESSION['id']);
		header('location: mon_compte.php');
		exit();
	} else
		header('location: creer_identifiants_mikuru.php');
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Créer Identifiants Mikuru</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Créer ses Identifiants pour Mikuru</h2>

	<form action="creer_identifiants_mikuru.php" method="post">
	<p>
		<label name="pseudo">Pseudo : <input name="pseudo" /></label><br />
		<label name="mdp">Mot de passe : <input type="password" name="mdp" /> (Entre 6 et 12 caractères)</label><br />
		<label name="mdp_re">Mot de passe (encore) : <input type="password" name="mdp_re" /></label><br />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>