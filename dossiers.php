<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['intervenants'] || !((int)$_SESSION['permission'] & LIRE_DOSSIER))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Dossiers</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	<h2>Dossier</h2>
	<a href="ajouter_dossier.php">Ajouter un dossier</a>
	</nav>

	<div id="cadre_stockage">
	<table>
	<tr><th>N° de dossier</th><th>Enfant</th><th>Intervenant</th><th>Date</th><th>Actions</th></tr>
	<?php $requete = mysql_query('SELECT *, d.id AS id, e.prenom AS prenom_enfant, e.nom AS nom_enfant, a.prenom AS prenom_intervenant, a.nom AS nom_intervenant
					FROM '.$bdd_prefixe.'dossiers d
					LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = d.intervenant
					LEFT JOIN '.$bdd_prefixe.'enfants e ON e.id = d.enfant');
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<tr><td>'.$donnees['id'].'</td>';
				echo '<td>'.$donnees['prenom_enfant'].' '.$donnees['nom_enfant'].'</td>';
				echo '<td>'.$donnees['prenom_intervenant'].' '.$donnees['nom_intervenant'].'</td>';
				echo '<td>'.formater_date($donnees['date']).'</td>';
				echo '<td><a href="lire_dossier.php?id='.$donnees['id'].'">Consulter</a></td>';
				echo '</tr>';
			}
		}
	?>
	</table>
	</div>
	<?php include('bas_page.php'); ?>
	</body>
</html>