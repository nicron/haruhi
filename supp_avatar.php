<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['avatar'])
{
	header('location: index.php');
	exit();
}

$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
$user = $requete->fetch();
if(!is_null($user['avatar']))
	unlink('images/avatar/'.$user['avatar']);

$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET avatar = NULL WHERE id = '.$_SESSION['id_adherent']);
header('location: modif_avatar.php');
exit();
?>
