<?php
/**
 * Filename : cardgen.php (class)
 * 
 * Générateur de cartes d'adhérents (Extension pour HARUHI)
 * @author Nicolas FAURE <contact@nicronics.com>
 * @created on 2016-03-31
 */

/* Inclus la bibliothèque TCPDF pour la génération des fichiers PDF */
define ('K_TCPDF_EXTERNAL_CONFIG', true); // désactive la configuration par défaut
/* Document unit of measure [pt=point, mm=millimeter, cm=centimeter, in=inch]. */
define ('PDF_UNIT', 'mm');
/* */
define ('PDF_PAGE_FORMAT', 'A4');
define ('PDF_PAGE_ORIENTATION', 'P');
define ('PDF_MARGIN_HEADER', 0);
define ('PDF_MARGIN_FOOTER', 0);

/* Dimensions A4 portrait */
define ('A4_h', 297);
define ('A4_w', 210);
/* Dimensions des cartes (unité = PDF_UNIT)*/
define ('CARDWIDTH', 85);
define ('CARDHEIGHT', 54);
define ('CARDPADDING', 4);
define ('PDF_MARGIN_TOP', (A4_h%CARDHEIGHT)/2);
define ('PDF_MARGIN_BOTTOM', PDF_MARGIN_TOP);
define ('PDF_MARGIN_LEFT', (A4_w%CARDWIDTH)/2);
define ('PDF_MARGIN_RIGHT', PDF_MARGIN_LEFT);
/* Description du document */
define ('PDF_CREATOR', 'CardGen');
define ('PDF_AUTHOR', 'Nicolas FAURE <contact@nicronics.com>');
define ('PDF_HEADER_TITLE', 'Cartes Adhérents');
define ('PDF_HEADER_STRING', "Cartes adhérents de l'association");

/* Chemin de stockage des pdf générés */
define ('CACHE_DIR', '/cache/cardgen/');

require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');



class CardGen {
	public $config; // configuration des cartes
	public $lengths; // longueurs maximales des champs texte
	
	protected $xoffset;
	protected $yoffset;
	public $cellwidth;
	
	function __construct() {
		global $infos_adresseasso,$infos_nomasso;
		// Récupère les paramètres des cartes dans le tableau $this->conf
		$this->config = $this->getConfig();
		if($this->config === false) {
			// Valeurs de configuration par défaut
			$this->config = array(
				'year' => date('Y'),
				'title' => mb_strtoupper("carte d'adhésion", 'UTF-8'),
				'coor' => $infos_adresseasso,
				'txt' => "Atteste que le titulaire de cette carte s'est acquitté de sa cotisation et accède de plein droit aux activités de l'association sur l'année civile mentionnée.\n\n"
			);
		}
		// Défini les longueurs maximales des champs du formulaire
		$this->lengths = array('year'=>5, 'title'=>23, 'coor'=>127, 'txt'=>255);

		// Initialisation des offsets
		$this->xoffset = 0;
		$this->yoffset = 0;
	}
	
	function getConfig() {
		global $pdo, $bdd_prefixe;
		/* Récupération de la configuration */
		$res = $pdo->query('SELECT * FROM '.$bdd_prefixe.'cardgen_config');
		if(! $config = $res->fetch(PDO::FETCH_ASSOC))
			return false;
		return $config;
	}
	
	function updateConfig() {
		global $pdo, $bdd_prefixe;
		/* Mise à jour de la base de données */
		$values = array(
			$this->config['year'],
			$this->config['title'],
			$this->config['coor'],
			$this->config['txt']
			);
		$req = $pdo->prepare('UPDATE '.$bdd_prefixe.'cardgen_config SET `year` = ?, `title` = ?, `coor` = ?, `txt` = ?');
		$req->execute($values);
		$res = $pdo->query('SELECT COUNT(*) FROM '.$bdd_prefixe.'cardgen_config');
		if($res->fetchColumn() < 1) {
			$req = $pdo->prepare('INSERT INTO '.$bdd_prefixe.'cardgen_config SET `year` = ?, `title` = ?, `coor` = ?, `txt` = ?');
			$req->execute($values);
		}
	}
	
	public function fontBuilder($fontfile='newfont.ttf') { /* POUR MEMO */
		/* Construit la police d'écriture */
		/*
		 * A utiliser uniquement pour construire les fichiers de la police
		 * que l'on a placée dans le répertoire fonts/
		 *
		 */
		$font = new TCPDF_FONTS();
		$fontname = $font->addTTFfont(K_PATH_FONTS.$fontfile, 'TrueType', '', 32);
		echo $fontname."<br>";
	}
	
	public function isMD5($s='') {
		return strlen($s)==32 && ctype_xdigit($s);
	}
	
	public function isValidID($s='') {
		return is_numeric($s) && $s==intval($s);
	}
	
	public function isValidEmail($s='') {
		return filter_var($s, FILTER_VALIDATE_EMAIL);
	}

	public function isValidURL($s='') {
		return filter_var($s, FILTER_VALIDATE_URL);
	}

	public function oneCard($pdf, $xoffset=0, $yoffset=0, $num=1, $firstname='Prénom', $lastname='Nom', $pseudo='', $date='0000-00-00', $avatar='') {
		/* Génère le contenu d'une carte */

		// dimensions (en mm)
		$photowidth = 25; // largeur de la photo
		$borderwidth = 1; // bord de la photo
		$lh = 5 ; // hauteur de la légende (pseudo)
		$cellwidth = CARDWIDTH - CARDPADDING - $photowidth - $borderwidth; // défini la largeur des cellules
		$left = $xoffset + CARDPADDING + $photowidth + $borderwidth; // Positionne les cellules à droite de la photo

		$basedir = realpath(dirname(__FILE__).'/../');
		// Fond de carte
		$fond = $basedir.CACHE_DIR.'fond_carte.png';
		if(file_exists($fond))
			$pdf->Image($fond, $xoffset, $yoffset, CARDWIDTH, CARDHEIGHT, '', '', '', false, 300, '', false, false, array('LTRB' => array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10', 'color' => array(0, 0, 0))) );
		// affiche la photo de l'adhérent
		if(empty($avatar) || !file_exists($basedir.'/images/avatar/'.$avatar))
			$avatar = 'default.png';
		$pdf->Image($basedir.'/images/avatar/'.$avatar, ($xoffset + CARDPADDING + $borderwidth/2), ($yoffset + CARDPADDING + $borderwidth/2), $photowidth, '', '', '', 'B', false, 300, '', false, false, array('LTRB' => array('width' => $borderwidth, 'dash' => 0, 'color' => array(255,255,255))), true, false, false);
		// et son pseudo
		if(empty($pseudo)) $pseudo = $firstname;
		$pdf->SetFont('dejavusanscondensed', 'I', 8);
		$pdf->setColor('fill', 255);
		$pdf->SetX(($xoffset + CARDPADDING), true);
		$pdf->Cell(($photowidth + $borderwidth), $lh, $pseudo, 0, 1, 'C', true, '', 1, false, 'T', 'M');
		// affiche le logo de l'association
		/* Pour contourner le problème de gestion de la transparence par la bibliothèque TCPDF
		   le logo est inséré dans l'image de fond */
		/*
		* Sans ce bug, la ligne suivante aurait été utilisée *
		$pdf->Image($basedir.'/images/logo-alpha.png', ($left + 5), ($yoffset + 3), 45, '', '', '', 'N', false, 300, '', false, false, 0);
		*/
		
		// affiche les coordonnées de l'association
		$pdf->SetFont('dejavusansextralight', '', 5);
		$pdf->MultiCell($cellwidth, 0, $this->config['coor'], 0, 'C', false, 1, $left, ($yoffset + 15.5), true, 0, false, true, 0, 'T', false);

		$pdf->setCellPaddings(CARDPADDING, 0, CARDPADDING, 0);
		// affiche le titre
		$pdf->SetFont('times', 'IB', 12);
		$pdf->MultiCell($cellwidth, 0, $this->config['title'].' '.$this->config['year'], 0, 'C', false, 1, $left, ($yoffset + 22), true, 0, false, true, 0, 'T', false);
		// affiche le numéro d'adhérent
		$pdf->SetFont('courier', '', 11);
		$pdf->SetTextColor(200,0,0);
		$pdf->MultiCell($cellwidth, 0, sprintf("N° %06s", $num), 0, 'C', false, 1, $left, ($yoffset + 33), true, 0, false, true, 0, 'T', false);
		$pdf->SetTextColor(0);
		// prénom nom
		$pdf->SetFont('helvetica', '', 10);
		$pdf->MultiCell($cellwidth, 0, ucfirst(strtolower($firstname)).' '.mb_strtoupper($lastname, 'UTF-8'), 0, 'C', false, 1, $left, ($yoffset + 38), true, 0, false, true, 0, 'T', false);
		// affiche le texte
		$pdf->SetFont('times', 'I', 7);
		$pdf->MultiCell(CARDWIDTH, 0, $this->config['txt'], 0, 'J', false, 1, $xoffset, ($yoffset + CARDHEIGHT - CARDPADDING - 5), true, 0, false, true, 0, 'T', false);
	}
	
	private function initPDF() {
		global $infos_nomasso;
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false, false);

		$pdf->SetCreator('CardGen');
		$pdf->SetAuthor('Nicolas FAURE <contact@nicronics.com>');
		$pdf->SetTitle('Cartes Adhérents');
		$pdf->SetSubject('');
		$keywords = (empty($infos_nomasso)?'':$infos_nomasso.', ').'association, adhérents, cartes, cartes adhérents, membres';
		$pdf->SetKeywords($keywords);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(false, 0);
		
		return $pdf;
	}
	
	function getPDF() { /* Retourne dans un fichier PDF l'intégralité des cartes d'adhérent actives */
		global $pdo, $bdd_prefixe;

		$pdf = $this->initPDF();

		$res = $pdo->query('SELECT id_membre, prenom, nom, pseudo, date_fin_cotisation, avatar FROM `'.$bdd_prefixe.'adherents` WHERE date_fin_cotisation >= \''.date('Y-m-d').'\'');

		/* Peuplement des pages */
		$r=0;
		$c=0;
		while($row = $res->fetch()) {
			if($r==0 && $c==0)
				$pdf->AddPage();
			$this->oneCard($pdf, (PDF_MARGIN_LEFT + $c * CARDWIDTH), (PDF_MARGIN_TOP + $r * CARDHEIGHT), $row['id_membre'], $row['prenom'], $row['nom'], (empty($row['pseudo'])?$row['prenom']:$row['pseudo']), $row['date_fin_cotisation'], $row['avatar']); // retourne le contenu d'une carte adhérent
			if($c<2)
				$c++;
			else {
				$c=0;
				if($r<floor(A4_h/CARDHEIGHT))
					$r++;
				else
					$r=0;
			}
		}

		$pdf->Output('cartes-adherents.pdf', 'I');
	}

	function getCard($id, $save=false) { /* Retourne la carte de l'adhérent identifié par $id */
		if(!CardGen::isValidID($id))
			return false; // pas la peine de continuer

		global $pdo, $bdd_prefixe, $infos_nomasso, $infos_siteasso, $infos_adresseasso;
		$basedir = realpath(dirname(__FILE__).'/../');
		
		if(! $res = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id.' AND date_fin_cotisation >= \''.date('Y-m-d').'\'') )
			return false;
		if(! $row = $res->fetch(PDO::FETCH_ASSOC))
			return false;

		$msg =<<<EOD
{$infos_nomasso} vous remercie pour le soutien apporté par votre adhésion :)

Voici, ci-dessous, votre carte de membre officiel.

EOD;

		$pdf = $this->initPDF();
		/* Génération de la page */
		$pdf->AddPage();
		// affiche le logo de l'association
		$pdf->Image($basedir.'/images/logo_asso_facture.png', PDF_MARGIN_LEFT, PDF_MARGIN_TOP, 80, '', '', '', 'N', false, 300, 'C', false, false, 0);
		// affiche le titre
		$pdf->SetFont('times', 'I', 20);
		$pdf->MultiCell((A4_w - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT), 20, 'Carte d\'Adhérent', 0, 'C', false, 1, '', 40);
		// affiche le message
		$pdf->SetFont('helvetica', 'I', 14);
		$pdf->MultiCell((A4_w - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT), 40, $msg, 0, 'J', false, 1, '', 80);
		// affiche le pied de page
		$pdf->SetFont('helvetica', 'IB', 10);
		$pdf->MultiCell((A4_w - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT), 5, $infos_nomasso, 0, 'C', false, 1, '', 278, true, 0, false, true, 0, 'T');
		$pdf->SetFont('helvetica', 'I', 8);
		$pdf->MultiCell((A4_w - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT), 8, $infos_adresseasso.(CardGen::isValidURL($infos_siteasso)?"\n".$infos_siteasso:""), 0, 'C', false, 1, '', '', true, 0, false, true, 0, 'T');
		// insert la carte adhérent
		$this->oneCard($pdf, ((A4_w - CARDWIDTH) / 2), (PDF_MARGIN_TOP + 2*CARDHEIGHT), $row['id_membre'], $row['prenom'], $row['nom'], (empty($row['pseudo'])?$row['prenom']:$row['pseudo']), $row['date_fin_cotisation'], $row['avatar']);

		$cardname = 'CG_'.md5('CardGen_'.sprintf("%09d",$id)).'.pdf';
		if($save === false) {
			// retourne le contenu d'une carte adhérent
			if(empty($row['cartemembre'])) {
				$pdf->Output($basedir.CACHE_DIR.$cardname, 'FI');
				$req = $pdo->prepare('UPDATE '.$bdd_prefixe.'adherents SET cartemembre = :cardname WHERE id = :id');
				$req->execute(array(':cardname' => $cardname, ':id' => $id));
			}
			else
				$pdf->Output($cardname, 'I');
		}
		else {
			// enregistre le fichier carte adhérent
			$pdf->Output($basedir.CACHE_DIR.$cardname, 'F');
			$req = $pdo->prepare('UPDATE '.$bdd_prefixe.'adherents SET cartemembre = :cardname WHERE id = :id');
			$req->execute(array(':cardname' => $cardname, ':id' => $id));
		}
	}
	
	public function getURL($id) {
		if(!CardGen::isValidID($id))
			return false; // pas la peine de continuer

		global $pdo, $bdd_prefixe;

		if($res = $pdo->query('SELECT COUNT(*) FROM '.$bdd_prefixe.'adherents WHERE id = '.$id.' AND date_fin_cotisation >= \''.date('Y-m-d').'\'') ) {
			if($res->fetchColumn() < 1)
				return false;
		}
		else
			return false;
		// Récupère le nom de fichier pdf de l'adhérent
		$res = $pdo->query('SELECT cartemembre FROM '.$bdd_prefixe.'adherents WHERE id = '.$id.' AND date_fin_cotisation >= \''.date('Y-m-d').'\'');
		if(! $row = $res->fetch(PDO::FETCH_ASSOC))
			return false;
		if(empty($row['cartemembre']))
			return false;
			
		return CACHE_DIR.$row['cartemembre'];
	}
	
	function manage_downloaded_background($filename) {
		/* Copie et traite l'image de fond téléchargée */
		// retourne un texte d'erreur si besoin
		$dest = realpath(dirname(__FILE__).'/..').CACHE_DIR.'fond_carte';
		$check = getimagesize($filename);
		if($check !== false) {
			switch($check['mime']) {
				case 'image/jpeg' :
					$img0 = imagecreatefromjpeg($filename);
					break;
				case 'image/png' :
					$img0 = imagecreatefrompng($filename);
					break;
				default :
					unlink($filename);
					return 'Erreur : Type de fichier incorrect! Seuls les fichiers PNG et JPEG sont acceptés.<br />';
			}
			// Retaille l'image au format carte
			$new_width = $check[0];
			$new_height = $check[1];
			$ratio_carte = CARDWIDTH / CARDHEIGHT;
			$ratio_image = $new_width / $new_height;
			$correc = 1 + ($ratio_image - $ratio_carte) / $ratio_carte;
			if($correc < 1)
				$new_height = round($new_height*$correc);
			else
				$new_width = round($new_width/$correc);
			$img = imagecreatetruecolor($new_width, $new_height);
			imagecopy($img, $img0, 0, 0, round(($check[0]-$new_width)/2), round(($check[1]-$new_height)/2), $new_width, $new_height);
			imagedestroy($img0);
			// Crée le fichier image de fond
			@unlink($dest.'.png'); // supprime d'abord l'ancien fichier
			imagepng($img,$dest.'.png',5);
			// Crée la miniature
			$mimg = imagecreatetruecolor((CARDWIDTH*4), (CARDHEIGHT*4)); // 4 points par millimètre
			imagecopyresampled($mimg, $img, 0, 0, 0, 0, (CARDWIDTH*4), (CARDHEIGHT*4), $new_width, $new_height);
			@unlink($dest.'_mini.png'); // supprime d'abord l'ancien fichier
			imagepng($mimg,$dest.'_mini.png',7);
			imagedestroy($img);
			imagedestroy($mimg);
		}
		else {
			unlink($filename);
			return 'Erreur : Type de fichier incorrect! Seuls les fichiers PNG et JPEG sont acceptés.<br />';
		}
		
		return '';
	}
} // fin de classe CardGen
?>
