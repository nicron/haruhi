<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');

require('configuration.php');

if(!$fonctionnalites_statut['stockage'])
	header('location: index.php');

//On va humaniser la lecture de la taille
function taille_humaine($taille)
{
	if($taille >= 1073741824)
		return number_format(($taille / 1073741824), 2, ',', ' ').' Gio';
	elseif($taille >= 1048576)
		return number_format(($taille / 1048576), 2, ',', ' ').' Mio';
	elseif($taille >= 1024)
		return number_format(($taille / 1024), 2, ',', ' ').' Kio';
	else
		return number_format(($taille), 2, ',', ' ').' o';
}

function type_fichier_en_image($fichier)
{
    $type = type_fichier($fichier);
    switch($type)
    {
	case 'Image':
	    return '<img src="images/stockage_image.png" alt="Image" />';
	    break;
	case 'Image vectorielle':
	    return '<img src="images/stockage_svg.png" alt="Image vectorielle" />';
	    break;
	case 'Document Texte':
	case 'Open Document Texte':
	    return '<img src="images/stockage_doc_texte.png" alt="Document Texte" />';
	    break;
	case 'PDF':
	    return '<img src="images/stockage_pdf.png" alt="PDF" />';
	    break;
	default:
	    return '<img src="images/stockage_autre.png" alt="Autre" />';
    }
}

function type_fichier($fichier)
{
    $explode = explode('.', $fichier);
    $ext = strtolower($explode[count($explode) - 1]);
    switch($ext)
    {
	case 'png':
	case 'jpg':
	case 'gif':
	case 'bmp':
	case 'jpeg':
	    return 'Image';
	    break;
	case 'svg':
	    return 'Image vectorielle';
	    break;
	case 'doc':
	    return 'Document Texte';
	    break;
	case 'odt':
	    return 'Open Document Texte';
	    break;
	case 'pdf':
	    return 'PDF';
	    break;
	default:
	    return 'Autre';
    }
}

$couleur_point = array(0 => 'noir', 1 => 'rouge', 2 => 'jaune', 3 => 'vert');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Stockage</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		
		<script type="text/javascript" src="ajout_fichier.js"></script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2>Stockage</h2>
	    <?php if((int)$_SESSION['permission'] & ENVOYER_FICHIER) { ?>
<!-- 	    <p><a href="ajouter_fichier.php">Ajouter un fichier</a></p> -->
	    <p><a onClick="ouvrir_dial_fichier(0);" href="#">Ajouter un fichier</a></p>
	    <?php } ?>
	    <p class="petit">Visibilité :<br />
	    <?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'titre ORDER BY visibilite DESC');
	    $visibilite = array();
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				$visibilite[$donnees['visibilite']][] = $donnees['nom'];
			}

			foreach($visibilite as $k => $nom)
			{
				echo '<img src="images/visibilite_'.$couleur_point[$k].'.png" alt="'.$donnees['visibilite'].'"/> '.implode(', ', $nom).'<br />';
			}
		} ?>
	    </p>
	</nav>
	
	<div id="dial_ajout_fichier">
	<span id="dial_close" onClick="fermer_dial_fichier();">X</span>
	<div id="zone_envoi" class="drop_pas">Déposez vos fichiers ici !</div>
	Ou selectionnez : <input type="file" style="display: none;" id="champ_dial_fichier" multiple  onChange="choix_fichier_dial(event);" /><button onClick="document.getElementById('champ_dial_fichier').click();">Choisir</button><br />
	Visibilité : <select id="cs_visibilite">
		<?php
		foreach($visibilite as $k => $nom)
		{
			echo '<option value="'.$k.'">'.$k.' − '.implode(', ', $nom).'</option>';
		}
		?>
		</select><br />
	<a style="cursor: pointer;" onClick="envoyer_fichiers();">Envoyer</a>
	</div>
	<script type="text/javascript">init_dial();</script>
	
	<div id="dial_en_cours_envoi">
	<table id="table_en_cours_envoi"></table>
	</div>

	<div id="cadre_stockage">
	<table>
	<tr><th></th><th>N°</th><th>Nom</th><th>Taille</th><th>Auteur</th><th>Version</th><th>Actions</th></tr>
	<?php $requete = mysql_query('SELECT *, f.id AS id FROM '.$bdd_prefixe.'fichiers f LEFT JOIN '.$bdd_prefixe.'membres m ON m.id = f.auteur WHERE f.visibilite <= '.$_SESSION['visibilite'].' ORDER BY f.nom ASC');
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<tr';
				if($donnees['nouvelle_version'] != 0)
					echo ' class="fichier_vieux" ';
				echo '>';
				echo '<td><img src="images/visibilite_'.$couleur_point[$donnees['visibilite']].'.png" alt="'.$donnees['visibilite'].'"/></td><td>'.$donnees['id'].'</td>';
				echo '<td>'.type_fichier_en_image($donnees['nom']).' '.$donnees['nom'].'</td><td>'.taille_humaine($donnees['taille']).'</td>';
				if($donnees['auteur'] == '-1')
					echo '<td>Yuki</td>';
				else
					echo '<td>'.$donnees['pseudo'].'</td>';
				echo '<td>'.$donnees['version'].'</td>';
				echo '<td><a href="voir_fichier.php?id='.$donnees['id'].'"><img src="images/stockage_voir.png" title="Voir" alt="Voir" /></a> ';
				echo '<a href="afficher_fichier.php?id='.$donnees['id'].'"><img src="images/stockage_telecharger.png" title="Télécharger" alt="Télécharger" /></a>';
				if($donnees['nouvelle_version'] == 0 && (int)$_SESSION['permission'] & ENVOYER_FICHIER)
					echo ' <a href="ajouter_fichier.php?id='.$donnees['id'].'"><img src="images/stockage_nouvelle_version.png" title="Envoyer une nouvelle version" alt="Envoyer une nouvelle version" /></a>';
				if($donnees['auteur'] == $_SESSION['id'] && (int)$_SESSION['permission'] & ENVOYER_FICHIER)
					echo ' <a href="supp_fichier.php?id='.$donnees['id'].'"><img src="images/adherent_supprimer.png" title="Supprimer" alt="Supprimer" /></a>';
				echo '</td></tr>';
			}
		}
	?>
	</table>
	</div>
	<?php include('bas_page.php'); ?>
	</body>
</html>
