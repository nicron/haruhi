<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & AVOIR_COURRIEL))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	if($_POST['mdp'] == $_POST['mdp_re'] && strlen($_POST['mdp']) >= 6 && strlen($_POST['mdp']) <= 12)
	{
		$mdp = htmlspecialchars($_POST['mdp'], ENT_QUOTES);
		$requete = mysql_query('SELECT mail FROM '.$bdd_prefixe.'membres WHERE id = '.$_SESSION['id']);
		$donnees = mysql_fetch_array($requete);
		$coupe = explode('@', $donnees['mail']);
		$login_mail = $coupe[0];
		try {
		$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

		//login
		$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

		//popModifyPassword
		$soap->popModifyPassword($session, $domaine_ovh, $login_mail, $mdp, false);

		//logout
		$soap->logout($session);

		header('location: mon_compte.php');
		} catch(SoapFault $fault) {
		echo $fault;
		}
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification Mot de passe Courriel</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Modifier le mot de passe de son courriel</h2>

	<form action="modif_mdp_courriel.php" method="post">
	<p>
		<label name="mdp">Mot de passe : <input type="password" name="mdp" /> (Entre 6 et 12 caractères)</label><br />
		<label name="mdp_re">Mot de passe (encore) : <input type="password" name="mdp_re" /></label><br />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>