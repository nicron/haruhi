<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & LISTE_ADHERENT))
	header('location: index.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: adherents.php');

$id = intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Enfants</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Enfants</h2>
	<p>Enfants de cet adhérent :</p>
	<ul>
	<?php $requete = mysql_query('SELECT *, e.id AS id, e.prenom AS prenom, e.nom AS nom, a.prenom AS prenom_intervenant, a.nom AS nom_intervenant FROM '.$bdd_prefixe.'enfants e LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = e.id_intervenant WHERE id_parent = '.$id);
		  $i = 0;
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<ol>'.$donnees['prenom'].' '.$donnees['nom'].' '.$donnees['classe'];
				if($donnees['prenom_intervenant'] != NULL)
					echo ' (Intervenant : '.$donnees['prenom_intervenant'].' '.$donnees['nom_intervenant'].')';
				if((int)$_SESSION['permission'] & TOUCHE_ADHERENT)
					echo ' − <a href="modif_enfant.php?id='.$donnees['id'].'">Modifier</a> <a href="modif_enfant.php?supp='.$donnees['id'].'">Supprimer</a>';
				echo '</ol>';
				$i++;
			}
		} 
		if($i == 0)
		    echo '<ol>Pas d\'enfant</ol>';?>
	</ul>
	<?php if((int)$_SESSION['permission'] & TOUCHE_ADHERENT) { ?>
	<p><a href="ajout_enfant.php?id=<?php echo $id; ?>">Ajouter un enfant</a></p>
	<?php } ?>

	<p>Enfants de cet intervenant :</p>
	<ul>
	<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'enfants WHERE id_intervenant = '.$id);
		  $i = 0;
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<ol>'.$donnees['prenom'].' '.$donnees['nom'].'</ol>';
				$i++;
			}
		} 
		if($i == 0)
		    echo '<ol>Pas d\'enfant</ol>';?>
	</ul>
	<?php include('bas_page.php'); ?>
	</body>
</html>