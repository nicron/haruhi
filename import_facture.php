<?php
session_start();
ini_set('mbstring.substitute_character', "none");
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['factures'] || !((int)$_SESSION['permission'] & FAIRE_FACTURE))
	header('location: index.php');

function nettoyer($label)
{
	if($label[0] == "\"")
		return substr($label[0], 1, -1);
	else
		return $label;
}

function nettoyerAccent($texte)
{
	return strtr($texte, 'àâéèêîôùû', 'aaeeeiouu');
}

function obtenirValeur($cellules, $case, $titre, $concat = false)
{
	global $typesFacture;
	if(count($titre[$case]) > 0)
	{
		if($concat)
		{
			$c = array();
			foreach($titre[$case] as $i)
				$c[] = $cellules[$i];
			$contenu = implode(' ', $c);
		}
		else
			$contenu = $cellules[$titre[$case][0]];

		$contenu = mb_convert_encoding($contenu, 'UTF-8', 'UTF-16');
		if($case == 'type')
		{
			if(isset($typesFacture[$contenu]))
				return $typesFacture[$contenu];
			else
				return null;
		}

		if(in_array($case, array('date', 'date_inscription', 'date_fin_cotisation')))
		{
			$coupe = explode(' ', $contenu);
			$date_coupe = explode('/', $coupe[0]);

			return $date_coupe[2].'-'.$date_coupe[1].'-'.$date_coupe[0];
		}
		return $contenu;
	}
	else if(isset($_POST['default_'.$case]))
	{
		$default = $_POST['default_'.$case];
		if($default == '__aujourdhui')
			return date('Y-m-d');
		elseif($default == '__date_facturation')
			return obtenirValeur($cellules, "date", $titre);
		elseif($default == '__auto')
		{
			//Là, c'est en fonction de la case qu'on a
			if($case == 'date_inscription')
				return obtenirValeur($cellules, "date", $titre);
			else if($case == 'date_fin_cotisation')
			{
				$date = obtenirValeur($cellules, "date", $titre);
				//On doit aussi récupérer le type de facture
				$typeFacture = obtenirValeur($cellules, 'type', $titre);
				return analyserTypeFacture($typeFacture, $date)['fin'];
			}
			else if($case == 'date')
				return date('Y-m-d');
		}
		return $default;
	}
	else
		return null;
}

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$mode = MODE_AJOUT;
	$fichier = fopen($_FILES['fichier']['tmp_name'], 'r');
	$separateur = ';';
	$i = 0;
	$titres =
		[
		"nom" => [],
		"prenom" => [],
		"pseudo" => [],
		"adresse" => [],
		"telephone" => [],
		"courriel" => [],
		"date_inscription" => [],
		"date_fin_cotisation" => [],
		"commentaire" => [],
		"somme" => [],
		"date" => [],
		"payement" => [],
		"lien" => [],
		"type" => [],
		"titre" => []
	];

	//On stocke le type de facture
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type');
	if(!($requete === false))
	{
		$types = $requete->fetchAll();
		$externe = [];
		$interne = [];
		$typesFacture = array();
		foreach($types as $type)
		{
			if($type['externe'] == 1)
				$externe[] = $type['id'];
			else
				$interne[] = $type['id'];
			$typesFacture[$type['nom']] = $type;
		}
	}

	//On prépare nos requêtes
	$requeteFactureExterne = $pdo->prepare('SELECT id FROM '.$bdd_prefixe.'factures WHERE lien = ?');
	$requeteAdherent = $pdo->prepare('SELECT * FROM '.$bdd_prefixe.'adherents WHERE nom = ? AND prenom = ?');

	while(!feof($fichier))
	{
		$cellules = fgetcsv($fichier, 0, $separateur);
		if($i == 0) //Entête
		{
			foreach($cellules as $j => $l)
			{
				$label = nettoyer($l);
				if(isset($_POST['titre_'.$j]) && $_POST['titre_'.$j] != '_null')
					$titres[$_POST['titre_'.$j]][] = $j;
			}
		} elseif(count($cellules) > 1) //Les données
		{
			//Chaque ligne correspond à une facture, on fait donc les données
			//Table facture :  id 	adherent 	somme 	date 	payement 	lien 	type	visibilite
			$facture = array(
				'adherent' => 0,
				'somme' => obtenirValeur($cellules, 'somme', $titres),
				'date' => obtenirValeur($cellules, 'date', $titres),
				'payement' => obtenirValeur($cellules, 'payement', $titres),
				'lien' => obtenirValeur($cellules, 'lien', $titres),
				'type' => obtenirValeur($cellules, 'type', $titres),
				'visibilite' => 1 //Fixé à toujours visible
			);

			if($facture['type'] == null) //Type non trouvé => erreur
			{
				echo 'Aucun type correspondant'."<br />\n";
				continue;
			} else { //On met l'id
				$facture['type'] = $facture['type']['id'];
			}

			//On regarde le type de facture
			if(in_array($facture['type'], $interne)) //Facture interne
			{
				//On vide le lien
				$facture['lien'] = null;
			} else //Externe
			{
				//On regarde si on a déjà une facture du genre
				$requeteFactureExterne->execute(array($facture['lien']));
				$factureBDD = $requeteFactureExterne->fetch();
				if(!($factureBDD === false)) //On a déjà quelque chose
					continue; //On passe à l'élément suivant
			}

			//On continue, va falloir trouver l'adhérent
			//Table adhérent : id 	prenom 	nom 	pseudo 	adresse 	telephone 	courriel 	id_membre 	titre 	date_inscription 	date_fin_cotisation 	commentaire
			$adherent = array(
				'nom' => obtenirValeur($cellules, 'nom', $titres),
				'prenom' => obtenirValeur($cellules, 'prenom', $titres)
			);
			$requeteAdherent->execute(array($adherent['nom'], $adherent['prenom']));
			$adherentBDD = $requeteAdherent->fetch();
			if(!($adherentBDD === false)) ///L'adhérent existe déjà :) //TODO
			{
				$facture['adherent'] = $adherentBDD['id'];
				$adherent['adresse'] = $adherentBDD['adresse'];
				//TODO : Calcul de la fin de cotisation
				$finCotis = new DateTime($adherentBDD['date_fin_cotisation']);
				$finCotis->add(new DateInterval('P1Y'));
				$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET date_fin_cotisation = "'.$finCotis->format('Y-m-d').'" WHERE id = '.$facture['adherent']);
			} else { //On va le crée
				$adherent['pseudo'] = obtenirValeur($cellules, 'pseudo', $titres);
				$adherent['telephone'] = obtenirValeur($cellules, 'telephone', $titres);
				$adherent['adresse'] = obtenirValeur($cellules, 'adresse', $titres, true);
				$adherent['courriel'] = obtenirValeur($cellules, 'courriel', $titres);
				$adherent['titre'] = obtenirValeur($cellules, 'titre', $titres);
				$adherent['date_inscription'] = obtenirValeur($cellules, 'date_inscription', $titres);
				$adherent['date_fin_cotisation'] = obtenirValeur($cellules, 'date_fin_cotisation', $titres);
				$adherent['commentaire'] = obtenirValeur($cellules, 'commentaire', $titres, true);
				$pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (id, prenom, nom, pseudo, adresse, telephone, courriel, titre, id_membre, date_inscription, date_fin_cotisation, commentaire) VALUES ("", "'.$adherent['prenom'].'", "'.$adherent['nom'].'", "'.$adherent['pseudo'].'", "'.$adherent['adresse'].'", "'.$adherent['telephone'].'", "'.$adherent['courriel'].'", "'.$adherent['titre'].'", "0", "'.$adherent['date_inscription'].'", "'.$adherent['date_fin_cotisation'].'", "'.htmlspecialchars($adherent['commentaire']).'")');
				$facture['adherent'] = $pdo->lastInsertId();
			}

			//Et donc, maintenant, y a plus qu'à ajouter la facture
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'factures (id, adherent, somme, date, payement, type, visibilite, lien)
			VALUES ("", "'.$facture['adherent'].'", "'.$facture['somme'].'", "'.$facture['date'].'", "'.$facture['payement'].'", '.$facture['type'].', '.$facture['visibilite'].', '.(is_null($facture['lien']) ? 'NULL' : '"'.$facture['lien'].'"').')');
			$id = $pdo->lastInsertId();
			if(in_array($facture['type'], $interne)) //facture interne, on va copier les valeurs dans factures détails
			{
				//On a besoin des infos de l'adherent
				$pdo->exec('INSERT INTO `'.$bdd_prefixe.'factures_details` (`id`, `asso`, `nom_adherent`, `adresse_adherent`, `signature`)
			VALUES ('.$id.', "'.$infos_nomasso.'<br />'.$infos_adresseasso.'", "'.$adherent['prenom'].' '.$adherent['nom'].'", "'.$adherent['adresse'].'", "'.signature($facture['date']).'")');
			}
		}
		$i++;
	}
	fclose($fichier);
	header('location: factures.php');
	exit();
}

$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Import d'une facture</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Import d'une facture</h2>

		<form action="import_facture.php" method="post" enctype="multipart/form-data">
			<div class="formulaire" id="formulaire">
				<div class="ligne">
					<div class="cellule intitule" style="vertical-align: middle;"><label name="fichier">Fichier :</label></div>
					<div class="cellule">
						<input type="file" name="fichier" id="fichier_import" required />
						<span id="fichier_infos"></span>
					</div>
				</div>
			</div>
			<p>
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>
		<script type="application/javascript">
			var correspondanceHelloAsso = {
				'Type': 'type',
				'Date': 'date',
				'Montant': 'somme',
				'Reçu': 'lien',
				'Prénom': 'prenom',
				'Nom': 'nom',
				'Adresse': 'adresse',
				'Code Postal': 'adresse',
				'Ville': 'adresse',
				'Pays': 'adresse',
				'Email': 'courriel',
				'Commentaire': 'commentaire',
				'Pseudo sur le forum': 'pseudo'
			};

			//Table adhérent : id 	prenom 	nom 	pseudo 	adresse 	telephone 	courriel 	id_membre 	titre 	date_inscription 	date_fin_cotisation 	commentaire
			//Table facture :  id 	adherent 	somme 	date 	payement 	lien 	type	visibilite
			var options = //key = name, value = label
					{
						_null: {label: '', type: 'null'},
						nom: {label: 'Nom', type: 'string'},
						prenom: {label: 'Prénom', type: 'string'},
						pseudo: {label: 'Pseudo', type: 'string'},
						adresse: {label: 'Adresse', type: 'string'},
						telephone: {label: 'Téléphone', type: 'string'},
						courriel: {label: 'Courriel', type: 'email'},
						date_inscription: {label: 'Date d\'inscription', type: 'date'},
						date_fin_cotisation: {label: 'Date de fin de cotisation', type: 'date'},
						commentaire: {label: 'Commentaire', type: 'string'},
						somme: {label: 'Montant', type: 'number'},
						date: {label: 'Date de facturation', type: 'date'},
						payement: {label: 'Moyen de payement', type: 'string'},
						lien: {label: 'Lien du reçu', type: 'string'},
						titre: {label: 'Titre', type: 'titre'},
						type: {label: 'Type de facture', type: 'type_facture'}
					};

			var typesFacture = <?php
					$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type');
					if(!($requete === false))
					{
						$types = $requete->fetchAll();
						$toJson = [];
						foreach($types as $type)
						{
							$toJson[$type['id']] = $type['nom'];
						}
						echo json_encode($toJson);
					} else
						echo '{}';
					?>;

			var titresMembre = <?php
					$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'titre');
					if(!($requete === false))
					{
						$types = $requete->fetchAll();
						$toJson = [];
						foreach($types as $type)
						{
							$toJson[$type['id']] = $type['nom'];
						}
						echo json_encode($toJson);
					} else
						echo '{}';
					?>;

			function afficherFormulaireChoixDefaut()
			{
				var formulaire = document.getElementById('formulaire');

				var divLigne = document.createElement('div');
				divLigne.className = 'ligne';

				var divLabel = document.createElement('div');
				divLabel.className = 'cellule intitule';
				divLabel.innerHTML = 'Valeurs par défauts : ';
				divLigne.appendChild(divLabel);

				formulaire.appendChild(divLigne);

				for(var key in options)
				{
					var infos = options[key];
					if(infos.type != 'null')
					{
						divLigne = document.createElement('div');
						divLigne.className = 'ligne';

						divLabel = document.createElement('div');
						divLabel.className = 'cellule intitule';
						divLabel.innerHTML = infos.label+' : ';
						divLigne.appendChild(divLabel);

						var divSelect = document.createElement('div');
						divSelect.className = 'cellule';
						var elementForm;
						if(infos.type == 'string')
							elementForm = document.createElement('input');
						else if(infos.type == 'email')
						{
							elementForm = document.createElement('input');
							elementForm.setAttribute('type', 'email');
						}
						else if(infos.type == 'number')
						{
							elementForm = document.createElement('input');
							elementForm.setAttribute('type', 'number');
						}
						else if(infos.type == 'date')
						{
							elementForm = ajouterOption({'__auto': 'Automatique', '__aujourdhui': 'Aujourd\'hui', '__date_facturation': 'Date de facturation'});
						}
						else if(infos.type == 'type_facture')
						{
							elementForm = ajouterOption(typesFacture);
						}
						else if(infos.type == 'titre')
						{
							elementForm = ajouterOption(titresMembre);
						}
						elementForm.setAttribute('name', 'default_'+key);
						divSelect.appendChild(elementForm);
						divLigne.appendChild(divSelect);

						formulaire.appendChild(divLigne);
					}
				}
			}

			function ajouterOption(options)
			{
				var select = document.createElement('select');
				for(var key in options)
				{
					var option = options[key];
					var tagOption = document.createElement('option');
					tagOption.value = key;
					tagOption.innerHTML = option;
					select.appendChild(tagOption);
				}
				return select;
			}

			function faireSelect(i, nomColonne)
			{
				var select = document.createElement('select');
				select.setAttribute('name', 'titre_'+i);
				for(var key in options)
				{
					var infos = options[key];
					var label = infos.label;
					var option = document.createElement('option');
					option.value = key;
					option.innerHTML = label;
					if(
						(correspondanceHelloAsso[nomColonne] != undefined && correspondanceHelloAsso[nomColonne] == key) ||
						(nomColonne.toLowerCase() == label.toLowerCase())
					)
						option.setAttribute('selected', true);
					select.appendChild(option);
				}
				return select;
			}

			function chargementFichier(evt)
			{
				var fichier = evt.target.files[0]; //On récupère le fichier
				var fileInput = document.getElementById("fichier_import");
				fileInput.style.display = 'none';
				//fichier.name : Nom du fichier
				//fichier.type : Type du fichier
				//fichier.size : Taille du fichier
				//fichier.lastModifiedDate : Date de dernière modification
				var fichierInfo = document.getElementById('fichier_infos');
				fichierInfo.innerHTML = fichier.name+' ('+fichier.size.toLocaleString()+'o)';

				//On récupère le contenu du fichier
				var reader = new FileReader();
				reader.onload = function(c)
				{
					var contenu = c.target.result;
					var lignes = contenu.split("\n");
					var entete = lignes[0].trim();
					var colonnes = entete.split(";");
					var infos = document.getElementById('formulaire');

					var divLigne = document.createElement('div');
					divLigne.className = 'ligne';

					var divLabel = document.createElement('div');
					divLabel.className = 'cellule intitule';
					divLabel.innerHTML = 'Lignes : ';
					divLigne.appendChild(divLabel);

					var divSelect = document.createElement('div');
					divSelect.className = 'cellule';
					divSelect.innerHTML = (lignes.length - 1);
					divLigne.appendChild(divSelect);

					infos.appendChild(divLigne);
					for(var i in colonnes)
					{
						var label = colonnes[i];
						var divLigne = document.createElement('div');
						divLigne.className = 'ligne';

						var divLabel = document.createElement('div');
						divLabel.className = 'cellule intitule';
						divLabel.innerHTML = label;
						divLigne.appendChild(divLabel);

						var divSelect = document.createElement('div');
						divSelect.className = 'cellule';
						divSelect.appendChild(faireSelect(i, label));
						divLigne.appendChild(divSelect);

						infos.appendChild(divLigne);
					}

					afficherFormulaireChoixDefaut();
				};
				reader.readAsText(fichier);
			}

			var fileInput = document.getElementById("fichier_import");
			fileInput.addEventListener('change', chargementFichier, false);
		</script>
		<?php include('bas_page.php'); ?>
	</body>
</html>
