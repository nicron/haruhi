var i_enfants = 1;
function maj_date_fin_cotisation()
{
    var date = document.getElementById('debut_cotis').value;
    if(date == "0000-00-00")
        document.getElementById('fin_cotis').value = "0000-00-00";
    else
    {
        var coupe = date.split('-');
        nouvelle_date = (Number(coupe[0])/* + 1*/) + '-' + coupe[1] + '-' + coupe[2];
        document.getElementById('fin_cotis').value = nouvelle_date;
    }
    afficher_verif_format_date('fin_cotis', 'erreur_fin_cotis');
}

function verif_format_date(id)
{
    var date = document.getElementById(id).value;
    var coupe = date.split('-');
    var mois = Number(coupe[1]);
    if(mois == 4 || mois == 6 || mois == 9 || mois == 11) //Nb jours
        mois = 30;
    else if(mois == 2)
    {
        var an = Number(coupe[0]);
        if((an % 4 == 0) && (an % 100 != 0 || an % 400 ==0))
            mois = 29;
        else
            mois = 28;
    } else
        mois = 31;

    if(coupe[0].length != 4 || Number(coupe[1]) < 0 || Number(coupe[1]) > 12 || Number(coupe[2]) < 0 || Number(coupe[2]) > mois)
        return false;
    else
        return true;
}

function afficher_verif_format_date(id_date, id_affiche)
{
    if(verif_format_date(id_date))
    {
        document.getElementById(id_affiche).innerHTML = '';
        document.getElementById(id_date).style.backgroundColor = '#BAFFC7';
    }
    else
    {
        document.getElementById(id_affiche).innerHTML = 'Format incorrect, doit être (AAAA-MM-JJ)';
        document.getElementById(id_date).style.backgroundColor = '#FFB4AC';
    }
}

function ajouter_ligne()
{
    var ligne = document.createElement('div');
    ligne.className="ligne";
    ligne.id = 'ligne_enfant_'+i_enfants;
    ligne.innerHTML = '<div class="cellule"><input name="nom_enfants[]" placeholder="Nom" value="'+document.getElementById('champ_nom').value+'" /></div> \
<div class="cellule"><input name="prenom_enfants[]" placeholder="Prénom" /></div> \
<div class="cellule"><input name="date_enfants[]" placeholder="Date de naissance" /></div> \
<div class="cellule"><input type="button" onClick="ajouter_ligne();" value="+" /> <input type="button" onClick="supp_ligne('+i_enfants+');" value="-" /></div>';
    document.getElementById('tab_enfants').appendChild(ligne);
    i_enfants = i_enfants + 1;
}

function supp_ligne(id)
{
    var ligne = document.getElementById('ligne_enfant_'+id);
    document.getElementById('tab_enfants').removeChild(ligne);
}
