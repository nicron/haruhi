<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['calendrier'])
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Calendrier</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2>Calendrier</h2>
	    <?php if((int)$_SESSION['permission'] & TOUCHE_CALENDRIER) { ?>
	    <p><a href="ajout_evenement.php">Ajouter un événement</a></p>
	    <?php } ?>
	    
	    <p>
	    <?php if(isset($_GET['annee']) && intval($_GET['annee']) > 0)
		$an = intval($_GET['annee']);
	    else
		$an = date('Y');
	    echo '← <a href="calendrier.php?annee='.($an - 1).'">Année précédente</a><br /><a href="calendrier.php?annee='.($an + 1).'">Année suivante</a> →';
	    ?></p>
	</nav>

<script type="text/javascript">
function ouvrir_mois(num)
{
	var ul_event = document.getElementsByTagName('ul');
	var j = 0;
	for(var i = 0; i < ul_event.length; i++)
	{
	    if(ul_event[i].getAttribute('class') == 'event')
	    {
		if(j == num)
		    ul_event[i].style.width = 'auto';
		else
		    ul_event[i].style.width = '0px';
		j++;
	    }
	}
}

function generer_calendrier(annee)
{
    if(annee != 0 && annee != (new Date()).getFullYear())
	var today = new Date(annee, 8, 1);
    else
	var today = new Date();
    var ul = document.getElementById('liste_calendrier');
    var nom_mois = new Array('Septembre', 'Octobre', 'Novembre', 'Décembre', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août');
    var nom_jour = new Array('Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa');
    var array_event = new Array();
    var array_bidon;
    //Et là, c'est le moment du remplissage en php ^^'
    <?php
    if(isset($_GET['annee']) && intval($_GET['annee']) > 0)
	$date = intval($_GET['annee']) + 1;
    else
	$date = 'YEAR(CURRENT_DATE() + INTERVAL 4 MONTH)';
    $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'calendrier WHERE YEAR(date + INTERVAL 4 MONTH) = '.$date.' ORDER BY date ASC'); //WHERE date >= CURRENT_DATE()
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				$coupe = explode('-', $donnees['date']);
				echo 'array_bidon = new Array('.((intval($coupe[1]) + 3) % 12).', '.intval($coupe[2]).', '.$donnees['id'].', "'.$donnees['titre'].'");'."\n";
				echo 'array_event.push(array_bidon);'."\n";
			}
		}
    ?>
    var j_array_event = 0;
    if(today.getMonth() >= 8) //Septembre
	var an_debut = today.getFullYear();
    else
	var an_debut = today.getFullYear() - 1;

    for(var i = 0; i < 12; i++)
    {
	if(i >= 4)
	    var an = an_debut + 1;
	else
	    var an = an_debut;
	var li_mois = document.createElement('li'); //On va créer la liste du mois
	var ul_li_mois = document.createElement('ul');
	var li_event = document.createElement('li'); //Et pour les events du mois
	var ul_li_event = document.createElement('ul');
	li_mois.innerHTML = '<span class="nom_mois">' + nom_mois[i] + ' ' + an + '</span>'; //On met le mois
	ul_li_mois.setAttribute('class', 'mois');
	li_event.setAttribute('class', 'event');
	ul_li_event.setAttribute('class', 'event');
	li_mois.setAttribute('onclick', 'ouvrir_mois(' + i + ')');
	//On va trouver le nombre de jour
	if(i == 0 || i == 2 || i == 7 || i == 9)
	    var nb_jour = 30;
	else if(i == 5) //Février
	{
	    if(((an % 4 == 0) && (an % 100 != 0)) || (an % 400 == 0)) //bisextile
		var nb_jour = 29;
	    else
		var nb_jour = 28;
	} else
	    var nb_jour = 31;

	var premier = new Date(); //On va trouver le premier
	premier.setDate(1);
	premier.setMonth((i + 8) % 12);
	premier.setYear(an);
	i_jour = premier.getDay();
	for(var j = 1; j <= nb_jour; j++)
	{
	    var nouveau_li = document.createElement('li');
	    var nouveau_li_event = document.createElement('li');
	    nouveau_li.innerHTML = nom_jour[i_jour] + ' ' + j;
	    if(i_jour == 0 && (i + 8) % 12 == today.getMonth() && j == today.getDate() && today.getFullYear() == (new Date()).getFullYear())
		nouveau_li.setAttribute('class', 'today dim');
	    else if((i + 8) % 12 == today.getMonth() && j == today.getDate() && today.getFullYear() == (new Date()).getFullYear())
		nouveau_li.setAttribute('class', 'today');
	    else if(i_jour == 0) //Dimanche
		nouveau_li.setAttribute('class', 'dim');
	    //On regarde pour l'evenement
	    if(j_array_event < array_event.length && array_event[j_array_event][0] == i && array_event[j_array_event][1] == j) //Event
	    {
		if((i + 8) % 12 == today.getMonth() && j == today.getDate() && today.getFullYear() == (new Date()).getFullYear()) //aujourd'hui
		    nouveau_li.setAttribute('class', 'today evenement');
		else
		    nouveau_li.setAttribute('class', 'evenement');
		nouveau_li_event.innerHTML = '<a href="lire_evenement.php?id='+array_event[j_array_event][2]+'">'+array_event[j_array_event][3]+'</a>';
		j_array_event++;
	    }
	    i_jour = (i_jour + 1) % 7;
	    ul_li_mois.appendChild(nouveau_li);
	    ul_li_event.appendChild(nouveau_li_event);
	}
	li_mois.appendChild(ul_li_mois);
	ul.appendChild(li_mois);
	li_event.appendChild(ul_li_event);
	ul.appendChild(li_event);
    }
}
</script>
	<div id="cadre_stockage">
	<ul id="liste_calendrier">
	</ul>
<script type="text/javascript">
generer_calendrier(<?php if(isset($_GET['annee']) && intval($_GET['annee']) > 0) echo intval($_GET['annee']); else echo '0'; ?>);
<?php if(!isset($_GET['annee']) || intval($_GET['annee']) == 0) { ?>
today = new Date();
ouvrir_mois((today.getMonth() + 4) % 12);
<?php } else { ?>
ouvrir_mois(0);
<?php } ?>
</script>
	</div>

	<?php include('bas_page.php'); ?>
	</body>
</html>