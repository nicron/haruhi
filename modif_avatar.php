<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['avatar'])
{
	header('location: index.php');
	exit();
}

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$fichier = $_FILES['avatar'];
	if(!in_array($fichier['type'], array('image/png', 'image/jpeg', 'image/gif')))
	{
		echo 'Le fichier envoyé n\'est pas une image :(';
	} else {
		//On récupère l'extension
		$explode_nom = explode('.', $fichier['name']);
		//On génère son nouveau nom
		$nom_fichier = genererCodeAleatoire(16).'.'.$explode_nom[count($explode_nom) - 1];
		move_uploaded_file($fichier['tmp_name'], 'images/avatar/'.$nom_fichier);
		//On récupère l'adresse de l'ancien pour le supprimer
		$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
		$user = $requete->fetch();
		if(!is_null($user['avatar']))
			unlink('images/avatar/'.$user['avatar']);

		$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET avatar = "'.$nom_fichier.'" WHERE id = '.$_SESSION['id_adherent']);
		header('location: mon_compte.php');
		exit();
	}
}

$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
$user = $requete->fetch();
$mode = MODE_MODIF;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification de son avatar</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Modifier son avatar</h2>

		<p>
			Avatar actuel :<br />
			<?php
			if(is_null($user['avatar']))
				echo 'Aucun avatar';
			else
			{
				echo '<img src="images/avatar/'.$user['avatar'].'" alt="Avatar" /><br />';
				echo '<a href="supp_avatar.php">Supprimer</a>';
			}
			?>
		</p>

		<form action="modif_avatar.php" method="post" enctype="multipart/form-data">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="avatar">Nouvel avatar : </label></div>
					<div class="cellule"><input name="avatar" type="file" /></div>
				</div>
			</div>

			<p>
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
