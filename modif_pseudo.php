<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
	if(strlen($pseudo) > 0 && !in_array(strtolower($pseudo), array('haruhi', 'kyon', 'mikuru', 'yuki', 'itsuki')))
	{
		$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'membres WHERE pseudo = "'.$pseudo.'"');
		if(!($requete === false))
		{
			if(!(mysql_fetch_array($requete)))
			{
				mysql_query('UPDATE '.$bdd_prefixe.'membres SET pseudo = "'.$pseudo.'" WHERE id = '.$_SESSION['id']);
				//On met à jour mikuru
				//Désactivé pour le moment (logins différents)
				/*mysql_query('UPDATE user SET username = "'.$pseudo.'" WHERE username = "'.$_SESSION['pseudo'].'"');
				mysql_query('UPDATE principals SET uri = "principals/'.$pseudo.'" WHERE uri = "principals/'.$_SESSION['pseudo'].'"');
 				mysql_query('UPDATE principals SET uri = "principals/'.$pseudo.'/calendar-proxy-read" WHERE uri = "principals/'.$_SESSION['pseudo'].'/calendar-proxy-read"');
				mysql_query('UPDATE principals SET uri = "principals/'.$pseudo.'/calendar-proxy-write" WHERE uri = "principals/'.$_SESSION['pseudo'].'/calendar-proxy-read"');
				mysql_query('UPDATE calendars SET principaluri = "principals/'.$pseudo.'" WHERE principaluri = "principals/'.$_SESSION['pseudo'].'"');*/
				//Et la session
				$_SESSION['pseudo'] = $pseudo;
				header('location: mon_compte.php');
			} else
				header('location: modif_pseudo.php');
		} else
			header('location: modif_pseudo.php');
	} else
			header('location: modif_pseudo.php');
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification de l'identifiant de connexion</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Modifier de l'identifiant de connexion</h2>

	<form action="modif_pseudo.php" method="post">
	<p>
		<label name="pseudo">Nouvel identifiant : <input name="pseudo" /></label><br />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>
