<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['ovh'] || !((int)$_SESSION['permission'] & GERER_ML))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Liste ML</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Mailing list</h2>

	<table>
	<tr><th>Id</th><th>Nom</th><th>Adresse</th><th>Action</th></tr>
	<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'ml');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
			echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['nom'].'</td><td>'.$donnees['adresse'].'</td><td><a href="synchro_ml.php?id='.$donnees['id'].'">Synchroniser</a> <a href="modif_ml.php?id='.$donnees['id'].'">Modifier</a> <a href="supp_ml.php?id='.$donnees['id'].'">Supprimer</a></td></tr>';
	} ?>
	</table>

	<p>
		<a href="ajout_ml.php">Ajouter une ML</a><br />
		<a href="ajout_ml_existante.php">Ajouter une ML existante</a>
	</p>
	<?php include('bas_page.php'); ?>
	</body>
</html>