<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: adherents.php');

$id = intval($_GET['id']);
$requete = $pdo->query('SELECT nom, prenom, pseudo, courriel FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
$user = $requete->fetch();

if(is_null($user['courriel']) || strlen($user['courriel']) == 0)
{
 echo 'Impossible de créer un compte à cet adhérent : Aucun courriel n\'a été renseigné dans son profil';
 exit();
}
//Génération du mot de passe
$mdp = genererCodeAleatoire(8);

if(is_null($user['pseudo']))
 $pseudo = $user['prenom'].'.'.$user['nom'];
else
 $pseudo = $user['pseudo'];
$mdp_hash = hash('sha512', $mdp);
$courriel = mail($user['courriel'], "Inscription sur Haruhi", wordwrap("Bonjour,\n
\n
L'association ".$infos_nomasso." utilise l'outil /Haruhi/ pour la gestion et l'information de ses adhérents.\n
\n
Cet outil est accessible a l'adresse suivante : $infos_siteasso\n
Votre login est : $pseudo\n
Votre mot de passe : $mdp\n
\n
Pour toute information complémentaire, n'hésitez pas à contacter le responsable technique de l'association\n
\n
Bonne journée ! :)", 70));
if($courriel)
{
    $pdo->exec('INSERT INTO '.$bdd_prefixe.'membres (id, pseudo, mdp, mail, type) VALUES ("", "'.$pseudo.'", "'.$mdp_hash.'", "'.$donnees['courriel'].'", "0")');
    $last_id = $pdo->lastInsertId();
    $pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET id_membre = '.$last_id.' WHERE id = '.$id);
    header('location: adherents.php');
} else
    echo "Erreur";
?>
