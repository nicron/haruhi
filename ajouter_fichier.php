<?php
session_start();
error_reporting(E_ALL);
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');


if(!$fonctionnalites_statut['stockage'] || !((int)$_SESSION['permission'] & ENVOYER_FICHIER))
	header('location: index.php');

function supprimer_dossier($adresse_dossier)
{
    $ens_fichier = scandir($adresse_dossier);
    foreach($ens_fichier as $i=>$f)
    {
	if($i >= 2) //On enlève . et ..
	{
	    if(is_dir($adresse_dossier.$f)) //C'est un dossier → Récursif
		supprimer_dossier($adresse_dossier.$f.'/');
	    else //fichier
		unlink($adresse_dossier.$f);
	}
    }
    rmdir($adresse_dossier);
}

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	if($_FILES['fichier']['size'] > 0)
	{
	print_r($_POST);
		$id_fichier = intval($_POST['id']);
		$visibilite = intval($_POST['visibilite']);
		$nom_echape_fichier = str_replace(array('\'', '"', ' '), "_", $_FILES['fichier']['name']);
		if($id_fichier != 0)
		{
			$requete = mysql_query('SELECT version FROM '.$bdd_prefixe.'fichiers WHERE id = '.$id_fichier) or die(mysql_error());
			$donnees = mysql_fetch_array($requete);
			$version = $donnees['version'] + 1;
		} else {
			$requete = mysql_query('SELECT id, version FROM '.$bdd_prefixe.'fichiers WHERE nom = "'.$nom_echape_fichier.'" ORDER BY version DESC') or die(mysql_error());
			if(!($requete === false))
			{
				if($donnees = mysql_fetch_array($requete))
				{
					$version = $donnees['version'] + 1;
					$id_fichier = $donnees['id'];
				} else
					$version = 1;
			} else
				$version = 1;
		}
		$coupe = explode('.', $nom_echape_fichier);
		$coupe[count($coupe) - 2] .= '_v'.$version;
		$nom_fichier = implode('.', $coupe);
		/*if(strtolower($coupe[count($coupe) - 1]) == 'odt') //C'est le gros lot !
		{
		    require_once('creer_apercu_odt.php');
		    $zip = new ZipArchive;
		    if ($zip->open($_FILES['fichier']['tmp_name']) === TRUE) {
			mkdir('fichiers/'.$nom_fichier.'_apercu/');
			mkdir('fichiers/'.$nom_fichier.'_apercu/tmp/');
			$zip->extractTo('fichiers/'.$nom_fichier.'_apercu/tmp/');
			$zip->close();
			faire_apercu_odt('fichiers/'.$nom_fichier.'_apercu');
			supprimer_dossier('fichiers/'.$nom_fichier.'_apercu/tmp/');
		    }
		}*/
		move_uploaded_file($_FILES['fichier']['tmp_name'], 'fichiers/'.$nom_fichier);
		mysql_query('INSERT INTO '.$bdd_prefixe.'fichiers (nom, taille, auteur, version, visibilite)
				VALUES ("'.$nom_echape_fichier.'", "'.$_FILES['fichier']['size'].'", "'.$_SESSION['id'].'", "'.$version.'", "'.$visibilite.'")') or die(mysql_error());
		if($id_fichier != 0)
			mysql_query('UPDATE '.$bdd_prefixe.'fichiers SET nouvelle_version = '.mysql_insert_id().' WHERE id = '.$id_fichier) or die(mysql_error());
	}
	//header('location: stockage.php');
	exit();
}

if(isset($_GET['id']) && intval($_GET['id']) > 0)
	$id = intval($_GET['id']);
else
	$id = 0;

?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajout d'un fichier</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Ajout d'un fichier</h2>

	<form action="ajouter_fichier.php" method="post" enctype="multipart/form-data">
	<label name="fichier">Fichier : <input name="fichier" type="file" /></label><br />
	Visibilité : <select name="visibilite">
		<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'titre ORDER BY visibilite DESC');
		if(!($requete === false))
		{
			$visibilite = array();
			while($donnees = mysql_fetch_array($requete))
			{
				$visibilite[$donnees['visibilite']][] = $donnees['nom'];
			}

			foreach($visibilite as $k => $nom)
			{
				echo '<option value="'.$k.'">'.$k.' − '.implode(', ', $nom).'</option>';
			}
		} ?>
		</select><br />
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<input type="hidden" name="envoi" value="1" />
	<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>