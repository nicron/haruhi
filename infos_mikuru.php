<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');

require('configuration.php');

if(!((int)$_SESSION['permission'] & AVOIR_COURRIEL))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Infos Mikuru</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Infos pour Mikuru</h2>
	
	<?php
	$requete = mysql_query('SELECT mikuru FROM '.$bdd_prefixe.'membres WHERE id = '.$_SESSION['id']);
		$donnees = mysql_fetch_array($requete); ?>
	
	<p>Pseudo pour l'identifiation : <?php echo $donnees['mikuru']; ?></p>
	
	<p>Calendrier commun au bureau :<br />
	Accès en écriture et en lecture par tout le bureau<br />
	Lien à entrer dans vos logiciels gérant les calendriers :<br />
	<?php echo $adresse_mikuru; ?>/calendarserver.php/calendars/mikuru/mikuru</p>
	
	<p>Calendrier personnel :<br />
	Accès en lecture par tout le bureau<br />
	Lien : <?php echo $adresse_mikuru; ?>/calendarserver.php/calendars/<?php echo $donnees['mikuru']; ?>/<?php echo $donnees['mikuru']; ?>-perso</p>
	
	<p>Calendrier des autres membres du bureau :<br />
	Accès en lecture seulement<br />
	Liens :
	<?php $requete = mysql_query('SELECT username FROM users WHERE username != "mikuru" AND username != "'.$donnees['mikuru'].'"');
	while($donnees = mysql_fetch_array($requete))
	  echo '<br />'.$adresse_mikuru.'/calendarserver.php/calendars/'.$donnees['username'].'/'.$donnees['username'].'-perso';
	?></p>
	
	<p>iOS : Utiliser le lien suivant pour avoir tous les calendriers d'un coup :<br />
	<?php echo $adresse_mikuru; ?>/calendarserver.php/</p>
	
	<?php include('bas_page.php'); ?>
	</body>
</html>