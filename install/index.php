<!DOCTYPE html>
<html>
	<head>
		<title>Installateur d'Haruhi</title>
	</head>
	<body>
		<form action="install.php" method="post">
			<h1>Installateur d'Haruhi</h1>
			<h2>Administrateur :</h2>
			<p>
				<label for="admin_pseudo">Pseudo : </label><input name="admin_pseudo" required /><br />
				<label for="admin_mdp">Mot de passe : </label><input type="password" name="admin_mdp" required /><br />
				<label for="admin_mail">Courriel : </label><input name="admin_mail" type="email" required />
			</p>
			<h2>Base de données :</h2>
			<p>
				<label for="bdd_host">Serveur : </label><input name="bdd_host" value="localhost" required /><br />
				<label for="bdd_login">Login : </label><input name="bdd_login" required /><br />
				<label for="bdd_mdp">Mot de passe : </label><input type="password" name="bdd_mdp" /><br />
				<label for="bdd_name">Nom de la BDD : </label><input name="bdd_name" required /><br />
				<label for="bdd_prefixe">Préfixe des tables : </label><input name="bdd_prefixe" value="haruhi_" />
			</p>
			<h2>API Ovh :</h2>
			<p>
				Activer : 
					<input type="radio" name="ovh_state" id="ovh_state_true" value="true" /> <label for="ovh_state_true">Oui</label>
					<input type="radio" name="ovh_state" id="ovh_state_false" value="false" /> <label for="ovh_state_false">Non</label>
				<br />
				<label for="ovh_nic">Nic : </label><input name="ovh_nic" /><br />
				<label for="ovh_mdp">Mot de passe : </label><input type="password" name="ovh_mdp"  /><br />
				<label for="ovh_ndd">Nom de domaine : </label><input name="ovh_ndd" />
			</p>
			<h2>Yuki :</h2>
			<p>
				Activer : 
					<input type="radio" name="yuki_state" id="yuki_state_true" value="true" /> <label for="yuki_state_true">Oui</label>
					<input type="radio" name="yuki_state" id="yuki_state_false" value="false" /> <label for="yuki_state_false">Non</label>
				<br />
				<label for="yuki_courriel">Courriel : </label><input name="yuki_courriel" /><br />
				<label for="yuki_mdp">Mot de passe : </label><input type="password" name="yuki_mdp"  /><br />
				<label for="yuki_server">Serveur : </label><input name="yuki_server" />
			</p>
			<h2>Fonctionnalités :</h2>
			<p>
				Mikuru : 
					<input type="radio" name="mikuru_state" id="mikuru_state_true" value="true" /> <label for="mikuru_state_true">Oui</label>
					<input type="radio" name="mikuru_state" id="mikuru_state_false" value="false" /> <label for="mikuru_state_false">Non</label>
				<br />
				Intervenants : 
					<input type="radio" name="intervenants_state" id="intervenants_state_true" value="true" /> <label for="intervenants_state_true">Oui</label>
					<input type="radio" name="intervenants_state" id="intervenants_state_false" value="false" /> <label for="intervenants_state_false">Non</label>
				<br />
				Factures : 
					<input type="radio" name="factures_state" id="factures_state_true" value="true" /> <label for="factures_state_true">Oui</label>
					<input type="radio" name="factures_state" id="factures_state_false" value="false" /> <label for="factures_state_false">Non</label>
				<br />
				Stockage : 
					<input type="radio" name="stockage_state" id="stockage_state_true" value="true" /> <label for="stockage_state_true">Oui</label>
					<input type="radio" name="stockage_state" id="stockage_state_false" value="false" /> <label for="stockage_state_false">Non</label>
				<br />
				Calendrier : 
					<input type="radio" name="calendrier_state" id="calendrier_state_true" value="true" /> <label for="calendrier_state_true">Oui</label>
					<input type="radio" name="calendrier_state" id="calendrier_state_false" value="false" /> <label for="calendrier_state_false">Non</label>
				<br />
				Avatar :
					<input type="radio" name="avatar_state" id="avatar_state_true" value="true" /> <label for="avatar_state_true">Oui</label>
					<input type="radio" name="avatar_state" id="avatar_state_false" value="false" /> <label for="avatar_state_false">Non</label>
				<br />
				Carte de membre :
					<input type="radio" name="cartemembre_state" id="cartemembre_state_true" value="true" /> <label for="cartemembre_state_true">Oui</label>
					<input type="radio" name="cartemembre_state" id="cartemembre_state_false" value="false" /> <label for="cartemembre_state_false">Non</label>
			</p>
			<h2>Autres infos :</h2>
			<p>
				<label for="infos_nomasso">Nom de l'association : </label><input name="infos_nomasso" value="" required /><br />
				<label for="infos_siteasso">Site web de l'association : </label><input name="infos_siteasso" value="http://" required /><br />
				<label for="infos_adresseasso">Adresse de l'association : </label><textarea name="infos_adresseasso" required></textarea>
			</p>
			<input type="submit" value="Installer" />
		</form>
	</body>
</html>
