CREATE TABLE IF NOT EXISTS `haruhi_adherents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `pseudo` varchar(128) NULL,
  `adresse` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `courriel` varchar(512) NOT NULL,
  `avatar` varchar(24) DEFAULT NULL,
  `cartemembre` varchar(64) DEFAULT NULL,
  `id_membre` int(11) NOT NULL,
  `titre` int(11) NOT NULL,
  `date_inscription` date NOT NULL,
  `date_fin_cotisation` date NOT NULL,
  `commentaire` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_archives_ml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(256) NOT NULL,
  `date` datetime NOT NULL,
  `id_ml` int(11) NOT NULL,
  `expediteur` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `id_message_serveur` varchar(128) NOT NULL,
  `id_reponse_serveur` varchar(128) NOT NULL,
  `id_citation_serveur` varchar(128) NOT NULL,
  `type_contenu` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_calendrier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `lieu` varchar(512) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `type` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_dossiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `intervenant` int(11) NOT NULL,
  `enfant` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_enfants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `naissance` date NOT NULL,
  `id_intervenant` int(11) NOT NULL,
  `classe` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_factures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adherent` int(11) NOT NULL,
  `somme` float NOT NULL,
  `date` date NOT NULL,
  `payement` varchar(128) NOT NULL,
  `lien` varchar(1024) DEFAULT NULL,
  `type` int(2) NOT NULL COMMENT '0: Cotisation, 1: Don',
  `visibilite` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_factures_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(16) NOT NULL,
  `externe` tinyint(1) NOT NULL DEFAULT '0',
  `phrase` text,
  `cotisation` tinyint(1) NOT NULL,
  `duree` int(11) DEFAULT NULL,
  `unite` tinyint(1) DEFAULT NULL,
  `debut_periode` date DEFAULT NULL,
  `fin_periode` date DEFAULT NULL,
  `termine_periode` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `haruhi_factures_details` (
  `id` int(11) NOT NULL,
  `asso` text,
  `nom_adherent` varchar(256) DEFAULT NULL,
  `adresse_adherent` text,
  `signature` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_fichiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(1024) NOT NULL,
  `taille` int(12) NOT NULL,
  `auteur` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `nouvelle_version` int(11) NOT NULL,
  `visibilite` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_lien_archives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_message` int(11) NOT NULL,
  `id_fichier` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(128) NOT NULL,
  `mdp` varchar(512) NOT NULL,
  `mail` varchar(512) NOT NULL,
  `type` int(2) NOT NULL COMMENT '0: rien, 1: boite mail, 2: alias',
  `mikuru` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_ml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(256) NOT NULL,
  `adresse` varchar(256) NOT NULL,
  `newsletter` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_ml_lien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ml` int(11) NOT NULL,
  `id_entite` int(11) NOT NULL,
  `is_titre` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_titre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(128) NOT NULL,
  `permission` int(11) NOT NULL,
  `visibilite` int(11) NOT NULL,
  `couleur` varchar(16) NOT NULL,
  `ordre` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_cardgen_config` (
  `year` varchar(4),
  `title` varchar(20) NOT NULL,
  `coor` text,
  `txt` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
