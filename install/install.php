<?php

function def($key)
{
	return (isset($_POST[$key]) ? $_POST[$key] : 'false');
}

$contenu = '<?php 

	$host = "'.$_POST['bdd_host'].'";
	$username = "'.$_POST['bdd_login'].'";
	$password = "'.$_POST['bdd_mdp'].'";
	$bdd_name = "'.$_POST['bdd_name'].'";
	$bdd_prefixe = "'.$_POST['bdd_prefixe'].'";

	//OVH
	$nic_ovh = "'.$_POST['ovh_nic'].'";
	$mdp_ovh = "'.$_POST['ovh_mdp'].'";
	$domaine_ovh = "'.$_POST['ovh_ndd'].'";

	//Yuki
	$serveur_yuki = "'.$_POST['yuki_server'].'";
	$courriel_yuki = "'.$_POST['yuki_courriel'].'";
	$mdp_yuki = "'.$_POST['yuki_mdp'].'";

	//Mikuru

	//Questions tutorats :
	$questions_tutorat = array();
	
	//Fonctionnalités
	$fonctionnalites_statut = array(
		"ovh" => '.def('ovh_state').',
		"yuki" => '.def('yuki_state').',
		"mikuru" => '.def('mikuru_state').',
		"intervenants" => '.def('intervenants_state').',
		"factures" => '.def('factures_state').',
		"stockage" => '.def('stockage_state').',
		"calendrier" => '.def('calendrier_state').',
		"avatar" => '.def('avatar_state').',
		"cartemembre" => '.def('cartemembre_state').'
	);
	
	//Infos diverses
	$infos_nomasso = "'.$_POST['infos_nomasso'].'";
	$infos_siteasso = "'.$_POST['infos_siteasso'].'";
	$infos_adresseasso = "'.$_POST['infos_adresseasso'].'";
?>';
$fichier = fopen('../infos_connexion.php', 'w');
fwrite($fichier, $contenu);
fclose($fichier);

//Il faut encore remplir la base de données
$requete_sql = str_replace('haruhi_', $_POST['bdd_prefixe'], file_get_contents('install.sql'));

try{
	$bdd_pdo_dns = 'mysql:host='.$_POST['bdd_host'].';dbname='.$_POST['bdd_name'];
	$pdo = new PDO($bdd_pdo_dns, $_POST['bdd_login'],$_POST['bdd_mdp']);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //Renvoi des exceptions lors d'erreurs
} catch ( Exception $e ) {
	echo "Connexion à MySQL impossible : ", $e->getMessage();
	die();
}

foreach(explode(';', $requete_sql) as $sql)
{
	if(!is_null($sql) && strlen(trim($sql)) > 1)
		$pdo->exec($sql);
}

//On crée les premières données
//Titre
$pdo->exec("INSERT INTO `".$_POST['bdd_prefixe']."titre` (`id`, `nom`, `permission`, `visibilite`, `couleur`, `ordre`) VALUES
(1, 'Administrateur', 16383, 3, '#46b8f1', 1)");

//Membre
$pdo->exec("INSERT INTO `".$_POST['bdd_prefixe']."membres` (`id`, `pseudo`, `mdp`, `mail`, `type`, `mikuru`) VALUES
(1, '".$_POST['admin_pseudo']."', '".hash('sha512', $_POST['admin_mdp'])."', '".$_POST['admin_mail']."', 0, '')");

//Adhérent
$pdo->exec("INSERT INTO `".$_POST['bdd_prefixe']."adherents` (`id`, `prenom`, `nom`, `courriel`, `id_membre`, `titre`, `date_inscription`, `date_fin_cotisation`) VALUES
(1, '".$_POST['admin_pseudo']."', 'Admin', '".$_POST['admin_mail']."', 1, 1, CURRENT_DATE(), CURRENT_DATE())");

//Installation des bibliothèques et des fichiers utiles à l'édition des cartes de membres
if(def('cartemembre_state')) {
	// Création du répertoire de cache s'il n'existe pas
	$cache_dir = dirname(__FILE__).'/../cache';
	if(!file_exists($cache_dir)) {
		if(!mkdir($cache_dir))
			echo "Création du répertoire ".$cache_dir." impossible!<br />";
	}
	elseif(!file_exists($cache_dir.'/cardgen')) {
		if(!mkdir($cache_dir.'/cardgen'))
			echo "Création du répertoire ".$cache_dir."/cardgen impossible!<br />";
	}
	// TCPDF
	$tcpdf_url = 'https://codeload.github.com/tecnickcom/TCPDF/zip/master';
	$dest_dir = sys_get_temp_dir()."/php".time();
	$dest_file = basename($tcpdf_url).".zip";
	mkdir($dest_dir);
	$ch = curl_init();
	$fp = fopen($dest_dir."/".$dest_file, "w");
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_URL, $tcpdf_url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_exec($ch);
	curl_close($ch);
	fclose($fp);
	$zip = new ZipArchive();
	if($zip->open($dest_dir."/".$dest_file) === TRUE) {
		$ziplist = array();
		$exclu = array('/config', '/examples');
		for($i = 0; $i < $zip->numFiles; $i++) {
			$found = FALSE;
			foreach($exclu as $n)
				if( strpos($zip->getNameIndex($i), $n) !== FALSE )
					$found = TRUE;
			if(!$found)
				$ziplist[] = $zip->getNameIndex($i);
		}
		$error = TRUE; // FALSE si une erreur
		$error = $error && $zip->extractTo($dest_dir."/", $ziplist);
		$zip->close();
		// Le répertoire "classes" doit être accessible en écriture
		$error = $error && rename($dest_dir."/TCPDF-master", dirname(__FILE__).'/../classes/tcpdf');
		if($error)
			echo "Bibliothèque TCPDF installée avec succès par CardGen!<br />";
		else
			echo "Erreur lors de l'installation de la bibliothèque TCPDF!<br />";
	}
	else {
		echo "Erreur : Ne peut pas extraire l'archive TCPDF!<br />";
	}
}

echo 'Installation terminée, n\'oubliez pas de supprimer le dossier install !<br />
<a href="..">Aller à l\'accueil</a>';
?>
