<?php
function faire_apercu_odt($adresse_dossier)
{
    //On sauvegarde la miniature
    rename($adresse_dossier.'/tmp/Thumbnails/thumbnail.png', $adresse_dossier.'/apercu.png');
    $xml = new DOMDocument();
    if($xml->load($adresse_dossier.'/tmp/content.xml'))
    $style = 'p { margin: 0px; }'."\n";
    $liste_style = $xml->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:style:1.0', 'style');
    foreach($liste_style as $l)
    {
	$attribut = $l->attributes;
	foreach($attribut as $a)
	{
	    if($a->name == 'name')
		$nom_classe = $a->value;
	    elseif($a->name == 'family')
	    {
		if($a->value == 'paragraph')
		    $type_classe = 'p';
		elseif($a->value == 'text')
		    $type_classe = 'span';
	    }
	}
	$style .= $type_classe.'.'.$nom_classe."\n{\n";
	
	foreach($l->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:style:1.0', 'text-properties') as $b)
	{
	    foreach($b->attributes as $a)
	    {
		    if($a->name == 'font-name')
			$style .= 'font-family:"'.$a->value.'";'."\n";
		    elseif($a->name == 'font-weight')
			$style .= 'font-weight: '.$a->value.';'."\n";
		    elseif($a->name == 'font-style')
			$style .= 'font-style: '.$a->value.';'."\n";
		    elseif($a->name == 'text-underline-style' && $a->value == 'solid')
			$style .= 'text-decoration: underline;'."\n";
		    elseif($a->name == 'text-underline-style' && $a->value == 'none')
			$style .= 'text-decoration: none;'."\n";
		    elseif($a->name == 'font-size')
			$style .= 'font-size: '.intval($a->value).'px;';
	    }
	}

	foreach($l->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:style:1.0', 'paragraph-properties') as $b)
	{
	    foreach($b->attributes as $a)
	    {
		    if($a->name == 'text-align')
		    {
			$style .= 'text-align:';
			if($a->value == 'start')
			    $style .= 'left';
			elseif($a->value == 'end')
			    $style .= 'right';
			elseif($a->value == 'center')
			    $style .= 'center';
			elseif($a->value == 'justify')
			    $style .= 'justify';
			$style .= ";\n";
		    }
	    }
	}
	$style .= '}'."\n";
    }

    $corps = '';
    $liste_corps = $xml->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:office:1.0', 'text')->item(0)->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:text:1.0', 'p');
    foreach($liste_corps as $l)
    {
	$corps .= '<p class="'.$l->getAttribute('text:style-name').'">';
	
	if($l->nodeValue == '')
	    $corps .= ' ';
	else
	{
	    $span = $l->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:text:1.0', 'span');
	    if($span->length != 0) //Y'a des spans à traiter
	    {
		foreach($span as $l_span)
		{
		    $corps .= '<span class="'.$l_span->getAttribute('text:style-name').'">';
		    $corps .= $l_span->nodeValue;
		    $corps .= '</span>';
		}
	    }
	    else
		$corps .= $l->nodeValue;
	}
	$corps .= '</p>'."\n";
    }

    $fichier = fopen($adresse_dossier.'/style.css', 'a+');
    fwrite($fichier, $style);
    fclose($fichier);

    $fichier = fopen($adresse_dossier.'/contenu.html', 'a+');
    fwrite($fichier, $corps);
    fclose($fichier);
}
?>