<?php
/**
 * Générateur de cartes d'adhérents (Extension pour HARUHI)
 * @author Nicolas FAURE <contact@nicronics.com>
 * @created on 2016-03-31
 */
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

// Inclus la classe cardgen
require_once(dirname(__FILE__).'/classes/cardgen.php');

$cg = new CardGen();

if($_POST['config']=='save') {
	$error = '';
	foreach($cg->lengths as $key=>$val) {
		if(strlen($_POST[$key]) > $val) {
			die("Error! Abnormal use of this script!");
			break;
		}
		else {
			$cg->config[$key] = $_POST[$key];
		}
	}
	$cg->updateConfig();
	if(!isset($_POST['bgrm'])) {
		/* Copier le fond de carte si celui-ci a été téléchargé */
		if(!isset($_FILES['bgcard']['error']) || is_array($_FILES['bgcard']['error'])) {
			$error .= 'Erreur : Paramètres invalides.<br />';
		}
		elseif($_FILES['bgcard']['error'] == UPLOAD_ERR_OK) {
			$error .= $cg->manage_downloaded_background($_FILES['bgcard']['tmp_name']);
		}
	}
	else { /* supprime l'image de fond */
		$bgfilename = dirname(__FILE__).CACHE_DIR.'fond_carte';
		@unlink($bgfilename.'_mini.png');
		@unlink($bgfilename.'.png');
	}
}

if($_GET['pdf']) {
	$cg->getPDF() OR die("Error during PDF generation");
}
elseif($_GET['id']) {
	$cg->getCard($_GET['id']) OR die("Error : Unable to get card");
}
else {
	$htmlconfig = str_replace("''", "'", $cg->config);
	foreach($htmlconfig as $key=>$val) {
		$htmlconfig[$key] = htmlentities($val, ENT_QUOTES, "UTF-8");
	}
	echo <<<EOD
<!DOCTYPE HTML>
<html lang="fr">
<head>
<meta charset="utf-8" />
<title>Cartes Adhérents</title>
<meta name="description" content="Générateur de cartes d'adhérents" />
<meta name="keywords" content="adhérents,cartes,cartes adhérents" />
<meta name="robots" content="noindex,nofollow">
<link rel="icon" type="image/png" href="images/favicon.png" />
<link rel="stylesheet" href="css/cardgen.css" type="text/css" />
<link rel="stylesheet" href="principal.css" type="text/css" media="screen"/>
</head>
<body>
EOD;
	include('haut_page.php');
	echo <<<EOD
<div class="wrapper">
<label>Configuration Cartes d'Adhérent</label>
<form action="/cardgen.php" method="post" enctype="multipart/form-data"><input type="hidden" name="config" value="save" />
<p>Année : <input type="text" name="year" value="{$htmlconfig['year']}" title="Année" id="annee" maxlength="{$cg->lengths['year']}" /></p>
<p>Titre : <input type="text" name="title" value="{$htmlconfig['title']}" title="titre" id="titre" maxlength="{$cg->lengths['title']}" /></p>
<p>Coordonnées association : <textarea name="coor" title="Coordonnées" id="coor" maxlength="{$cg->lengths['coor']}">{$htmlconfig['coor']}</textarea></p>
<p>Texte : <textarea name="txt" title="Texte" id="texte" maxlength="{$cg->lengths['txt']}">{$htmlconfig['txt']}</textarea></p>
<p>Image de fond (en .png, .jpeg ou .jpg) : <input type="file" name="bgcard" value="" title="Image de fond" id="bgcard" /><br />
<i>La transparence n'étant pas géré pour le logo, celui-ci doit être intégré à l'image de fond.<br />Une image de 1004&nbsp;x&nbsp;638&nbsp;pixels est recommandée.</i></p>
EOD;
	if(!empty($error))
		echo '<p class="center erreur">'.$error.'</p>';
	$miniature = CACHE_DIR.'fond_carte_mini.png';
	if(file_exists(dirname(__FILE__).$miniature))
		echo '<p class="center"><img id="bgmini" alt="aperçu fond" title="aperçu fond" src="'.$miniature.'?t='.time().'" /><br /><input type="checkbox" name="bgrm" value="supprime" />Supprimer le fond</p>';
	echo <<<EOD
<p class="center"><input type="submit" value="Sauver" /><input type="reset" value="Annuler" /></p>
</form>
</div>
<p class="center pdf"><a class="pdf" href="/cardgen.php?pdf=true">Télécharger le PDF des cartes adhérents</a></p>
</section>
<div class="footer">CardGen (2016) - créé par <a href="mailto:contact@nicronics.com">Nicolas FAURE</a> pour Haruhi</div>
</body>
</html>
EOD;
}
?>
