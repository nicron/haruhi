-- Création de cardgen_config
CREATE TABLE `haruhi_cardgen_config` (
  `year` varchar(4),
  `title` varchar(20),
  `coor` text,
  `txt` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Extension de la table adhérents
ALTER TABLE `haruhi_adherents` CHANGE `cartemembre` `cartemembre` VARCHAR(64) NULL;
