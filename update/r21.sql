-- Extension de factures_type
ALTER TABLE `haruhi_factures_type` ADD `externe` BOOLEAN NOT NULL DEFAULT FALSE AFTER `nom`;
ALTER TABLE `haruhi_factures_type` CHANGE `phrase` `phrase` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `haruhi_factures_type` ADD `cotisation` TINYINT(1) NOT NULL AFTER `phrase`;
ALTER TABLE `haruhi_factures_type` ADD `duree` INT(11) NULL AFTER `cotisation`, ADD `unite` TINYINT(1) NULL AFTER `duree`, ADD `debut_periode` DATE NULL AFTER `unite`, ADD `fin_periode` DATE NULL AFTER `debut_periode`, ADD `termine_periode` DATE NULL AFTER `fin_periode`;

-- Création de factures_details
CREATE TABLE `haruhi_factures_details` (
  `id` int(11) NOT NULL,
  `asso` text,
  `nom_adherent` varchar(255) DEFAULT NULL,
  `adresse_adherent` text,
  `signature` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Extension de factures
ALTER TABLE `haruhi_factures` ADD `lien` VARCHAR(1024) NULL AFTER `payement`;

-- Extension de la table adhérents
ALTER TABLE `haruhi_adherents` ADD `avatar` VARCHAR(24) NULL AFTER `courriel`, ADD `cartemembre` VARCHAR(255) NULL AFTER `avatar`;
