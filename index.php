<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Accueil</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<?php 	$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'membres m LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id_membre = m.id WHERE m.id = '.$_SESSION['id']);
	$donnees = mysql_fetch_array($requete); ?>

	<div id="colonne_accueil">
	
		<div class="info_accueil">
		      Bienvenue <?php echo $donnees['prenom'].' '.$donnees['nom']; ?>
		</div>
		
		<div class="info_accueil <?php
			$jour = reste_jour($donnees['date_fin_cotisation']);
			if($donnees['date_inscription'] == "0000-00-00")
				echo ' pas_membre';
			elseif($jour > 30)
				echo ' cotisation_ok';
			elseif($jour <= 30 && $jour > 7)
				echo ' cotisation_fin_loin';
			elseif($jour <= 7 && $jour >= 0)
				echo ' cotisation_fin_proche';
			else
				echo ' cotisation_fini';
			?>" >
			Cotisation : 
			<?php 

						
			if($donnees['date_fin_cotisation'] != '0000-00-00')
				echo 'Expire le '.formater_date($donnees['date_fin_cotisation']).'.';
			else
				echo 'Pas adhérent';
			?>
		</div>
		
		<?php if($fonctionnalites_statut['factures'])
{ ?>
		<div class="info_accueil">
			<h3>Factures</h3>
			<ul>
			<?php
			$requete = $pdo->query('SELECT *, f.id AS id FROM '.$bdd_prefixe.'factures f
			INNER JOIN '.$bdd_prefixe.'factures_type t ON t.id = f.type
			WHERE `adherent` = '.$_SESSION['id_adherent'].' AND `visibilite` = 1 ORDER BY `date` DESC LIMIT 0,3');
			if(!($requete === false))
			{
				$factures = $requete->fetchAll();
				foreach($factures as $facture)
				{
					echo '<li>#'.$facture['id'].' : '.formater_date($facture['date']).' '.number_format($facture['somme'], 2, ',', ' ').' € ';
							if($facture['externe'] == 0) //Facture interne
								echo '<a href="afficher_facture.php?id='.$facture['id'].'">';
							else //Facture externe
								echo '<a target="_blank" href="'.$facture['lien'].'">';
							echo 'Afficher</a></li>';
				}
				echo '<li><a href="mes_factures.php">Plus de factures</a></li>';
			} ?>
			</ul>
		</div>
		<?php } ?>

		<?php if($fonctionnalites_statut['calendrier']) { ?>
		<div class="info_accueil">
			<h3>À venir</h3>
			<ul>
			<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'calendrier WHERE date >= CURRENT_DATE() AND date <= DATE_ADD(CURRENT_DATE(), INTERVAL 3 MONTH) ORDER BY date ASC;');
				if(!($requete === false))
				{
					while($donnees = mysql_fetch_array($requete))
					{
						echo '<ol>'.formater_date($donnees['date']).' : '.$donnees['titre'].' <a href="lire_evenement.php?id='.$donnees['id'].'">Lire</a></ol>';
					}
				} ?>
			</ul>
		</div>
		<?php } ?>
		
	</div>

	<?php include('bas_page.php'); ?>
	</body>
</html>
