<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;
	$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
	$adresse_fixe = htmlspecialchars($_POST['adresse'], ENT_QUOTES);
	$telephone = htmlspecialchars($_POST['telephone'], ENT_QUOTES);
	$courriel = htmlspecialchars($_POST['courriel'], ENT_QUOTES);
	$titre = intval($_POST['titre']);
	$date_fin_cotisation = htmlspecialchars($_POST['date_fin_cotisation'], ENT_QUOTES);

	if(isset($_POST['avatar']) && $_POST['avatar'] == 1)
		$avatar_supp = true;
	else
		$avatar_supp = false;

	$obj_date = new DateTime($date_fin_cotisation);
	$obj_auj = new DateTime();

	//TODO : À TESTER ! fonctionnalites_statut['ovh']
	//Si c'est un membre connu des services, on va l'enlever de ses précédents ML
	if($mode == MODE_MODIF)
	{
		$user_requete = $pdo->query('SELECT courriel, titre FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
		$user = $user_requete->fetch();
		if($fonctionnalites_statut['ovh'] && $user['courriel'] != NULL && ($titre != $user['titre'] || $user['courriel'] != $courriel)) //Une des deux informations à changer
		{
			//On regarde dans quelles ML on va le retirer. Si c'est seulement le titre qui a changé, on ne touche pas à ses inscriptions persos. Sinon, on le retire de partout
			$condition_ml_perso = '';
			if($user['courriel'] != $courriel)
				$condition_ml_perso = ' OR (l.id_entite = '.$id.' AND l.is_titre = 0)';
			$requete = $pdo->query('SELECT m.adresse, m.cotisation, l.is_titre FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'ml m ON m.id = l.id_ml WHERE (l.id_entite = '.$titre.' AND l.is_titre = 1)'.$condition_ml_perso);
			if(!($requete === false))
			{
				try {
					$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

					//login
					$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);
					$listes = $requete->fetchAll();
					foreach($listes as $donnees)
					{
						$adresse = explode('@', $donnees['adresse']);
						$adresse = $adresse[0];

						if($user['courriel'] != NULL)
							$soap->mailingListSubscriberDel($session, $domaine_ovh, $adresse, $user['courriel']);
					}

					//logout
					$soap->logout($session);
				} catch(SoapFault $fault) {
					echo $fault;
				}
			}
		}
	}

	//Maintenant, on regarde du côté des créations
	if($titre == 0) //Nouveau titre, pas de ML associé, on ne fait que le créer
	{
		$titre_autre = htmlspecialchars($_POST['titre_autre'], ENT_QUOTES);
		$pdo->exec('INSERT INTO '.$bdd_prefixe.'titre (id, nom) VALUES ("", "'.$titre_autre.'")');
		$titre = $pdo->lastInsertId();

		if($mode == MODE_MODIF && $fonctionnalites_statut['ovh'] && $courriel != NULL && $user['courriel'] != $courriel) //Le courriel a changé, on le met quand même dans ses ML perso
		{
			if($user['courriel'] != $courriel)
				$condition_ml_perso = ' OR ';
			$requete = $pdo->query('SELECT m.adresse, m.cotisation, l.is_titre FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'ml m ON m.id = l.id_ml WHERE (l.id_entite = '.$id.' AND l.is_titre = 0)');
			if(!($requete === false))
			{
				try {
					$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

					//login
					$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);
					$listes = $requete->fetchAll();
					foreach($listes as $donnees)
					{
						$adresse = explode('@', $donnees['adresse']);
						$adresse = $adresse[0];

						if($donnees['is_titre'] == 0 || $donnees['cotisation'] == 0 || $obj_date >= $obj_auj)
							$soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $courriel);
					}

					//logout
					$soap->logout($session);
				} catch(SoapFault $fault) {
					echo $fault;
				}
			}
		}
	} else //Sinon, c'est un titre existant
	{
		if($fonctionnalites_statut['ovh'] && !is_null($courriel))
		{
			//On s'occupe d'ajouter le membre dans les bonnes ML, suite à son changement de titre
			$requete = $pdo->query('SELECT m.adresse, m.cotisation FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'ml m ON m.id = l.id_ml WHERE l.id_entite = '.$titre.' AND l.is_titre = 1');
			$ml = array();
			$adresses = $requete->fetchAll();
			foreach($adresses as $donnees)
			{
				if($donnees['cotisation'] == 0 || $obj_date >= $obj_auj)
					$ml[] = $donnees['adresse'];
			}

			if(isset($_POST['newsletter'])) //On regarde aussi les newsletter
			{
				foreach($_POST['newsletter'] as $nl)
				{
					$requete = $pdo->query('SELECT adresse FROM '.$bdd_prefixe.'ml WHERE id = '.intval($nl));
					$donnees = $requete->fetch();
					$ml[] = $donnees['adresse'];
				}
			}

			foreach($ml as $adr) //Y'a plus qu'à ajouter
			{
				$adresse = explode('@', $adr);
				$adresse = $adresse[0];
				try {
					$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

					//login
					$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

					$soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $courriel);

					//logout
					$soap->logout($session);
				} catch(SoapFault $fault) {
					echo $fault;
				}
			}
		}
	}

	$date_inscription = htmlspecialchars($_POST['date_inscription'], ENT_QUOTES);
	$commentaire = htmlspecialchars($_POST['com'], ENT_QUOTES);
	if(strlen($prenom) > 0 && strlen($nom) > 0 && strlen($date_inscription) > 0 && strlen($date_fin_cotisation) > 0)
	{
		if($mode == MODE_AJOUT)
		{
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (id, prenom, nom, pseudo, adresse, telephone, courriel, titre, id_membre, date_inscription, date_fin_cotisation, commentaire)
			VALUES ("", "'.$prenom.'", "'.$nom.'", "'.$pseudo.'", "'.$adresse_fixe.'", "'.$telephone.'", "'.$courriel.'", "'.$titre.'", "0", "'.$date_inscription.'", "'.$date_fin_cotisation.'", "'.$commentaire.'")');
			$id_adherent = $pdo->lastInsertId();
			if(isset($_POST['newsletter']))
			{
				foreach($_POST['newsletter'] as $nl)
					$pdo->exec('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.intval($nl).', '.$id_adherent.', 0)');
			}
			if($fonctionnalites_statut['intervenants'] && count($_POST['nom_enfants']) == count($_POST['prenom_enfants']) && count($_POST['nom_enfants']) == count($_POST['date_enfants']))
			{
				foreach($_POST['nom_enfants'] as $i => $nome)
				{
					if(strlen($nome) > 0)
					{
						$nom = htmlspecialchars($nome);
						$prenom = htmlspecialchars($_POST['prenom_enfants'][$i]);
						$classe = htmlspecialchars($_POST['classe_enfants'][$i]);
						$date_naissance = htmlspecialchars($_POST['date_enfants'][$i]);
						$pdo->exec('INSERT INTO '.$bdd_prefixe.'enfants (id, prenom, nom, classe, naissance, id_parent, id_intervenant)
			    VALUES ("", "'.$prenom.'", "'.$nom.'", "'.$classe.'", "'.$date_naissance.'", "'.$id_adherent.'", "0")');
					}
				}
			}
		} elseif($mode == MODE_MODIF)
		{
			if($avatar_supp)
			{
				//On doit récupérer le nom de l'avatar courant
				$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
				$user = $requete->fetch();
				if(!is_null($user['avatar']))
					unlink('images/avatar/'.$user['avatar']);
			}
			$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET prenom = "'.$prenom.'", nom = "'.$nom.'", pseudo = "'.$pseudo.'", adresse = "'.$adresse_fixe.'", telephone = "'.$telephone.'", courriel = "'.$courriel.'",
			titre = "'.$titre.'", date_inscription = "'.$date_inscription.'", date_fin_cotisation = "'.$date_fin_cotisation.'", commentaire = "'.$commentaire.'"'.($avatar_supp ? ', avatar = NULL' : '').' WHERE id = '.$id);
		}
	}
	header('location: adherents.php');
	exit();
}

if(isset($_GET['id']) && intval($_GET['id']) != 0)
{
	$mode = MODE_MODIF;
	$id = intval($_GET['id']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
	$user = $requete->fetch();
} else
	$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un adhérent</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
		<script type="text/javascript" src="js/form_adherent.min.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un adhérent</h2>

		<form action="form_adherent.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="prenom">Prénom</label></div>
					<div class="cellule"><input name="prenom" <?php if($mode == MODE_MODIF) echo 'value="'.$user['prenom'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom</label></div>
					<div class="cellule"><input id="champ_nom" name="nom" <?php if($mode == MODE_MODIF) echo 'value="'.$user['nom'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="pseudo">Pseudo</label></div>
					<div class="cellule"><input id="champ_pseudo" name="pseudo" <?php if($mode == MODE_MODIF) echo 'value="'.$user['pseudo'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="adresse">Adresse</label></div>
					<div class="cellule"><textarea name="adresse"><?php if($mode == MODE_MODIF) echo $user['adresse']; ?></textarea></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="telephone">Téléphone</label></div>
					<div class="cellule"><input name="telephone" <?php if($mode == MODE_MODIF) echo 'value="'.$user['telephone'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="courriel">Courriel</label></div>
					<div class="cellule"><input name="courriel" <?php if($mode == MODE_MODIF) echo 'value="'.$user['courriel'].'" '; ?>/></div>
				</div>
				<?php
				if($mode == MODE_MODIF && $fonctionnalites_statut['avatar'])
				{
				?>
				<div class="ligne">
					<div class="cellule intitule">Avatar</div>
					<div class="cellule">
						<?php
					if(is_null($user['avatar']))
						echo 'Aucun avatar';
					else
						echo '<input id="avatar_supp" name="avatar" type="checkbox" value="1" /><label for="avatar_supp">Supprimer l\'avatar de l\'adherent</label>';
						?>
					</div>
				</div>
				<?php
				}
				?>
				<div class="ligne">
					<div class="cellule intitule">Titre</div>
					<div class="cellule"><select name="titre">
						<?php
						$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'titre ORDER BY ordre ASC');
						if(!($requete === false))
						{
							$donnees = $requete->fetchAll();
							foreach($donnees as $ml)
							{
								echo '<option value="'.$ml['id'].'" style="background-color: '.$ml['couleur'].';"';
								if($mode == MODE_MODIF && $ml['id'] == $user['titre']) echo ' selected';
								echo '>'.$ml['nom'].'</option>';
							}
						} ?>
						<option value="0">Nouveau titre</option></select></div>
					<div class="cellule">ou <input name="titre_autre" placeholder="Nouveau titre" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="date_inscription">Date d'inscription</label></div>
					<div class="cellule">
						<input id="debut_cotis" name="date_inscription" type="date" onChange="afficher_verif_format_date('debut_cotis', 'erreur_debut_cotis'); maj_date_fin_cotisation();"
									 value="<?php if($mode == MODE_MODIF) echo $user['date_inscription']; else echo date("Y-m-d"); ?>" /></div>
					<div class="cellule" id="erreur_debut_cotis">(Format : AAAA-MM-JJ)</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="date_fin_cotisation">Date de fin de cotisation</label></div>
					<div class="cellule">
						<input id="fin_cotis" name="date_fin_cotisation" type="date" onChange="afficher_verif_format_date('fin_cotis', 'erreur_fin_cotis');"
									 value="<?php if($mode == MODE_MODIF) echo $user['date_fin_cotisation']; else echo date("Y-m-d"); ?>" readonly /></div>
					<div class="cellule" id="erreur_fin_cotis">(Format : AAAA-MM-JJ)</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="com">Commentaire</label></div>
					<div class="cellule"><textarea name="com"><?php if($mode == MODE_MODIF) echo $user['commentaire']; ?></textarea></div>
				</div>
				<?php if($fonctionnalites_statut['ovh']) { ?>
				<div class="ligne">
					<div class="cellule intitule"><label name="newsletter">Lettre d'information</label></div>
					<div class="cellule"><select multiple name="newsletter[]">
						<?php
	$nl_user = array();

	if($mode == MODE_MODIF)
	{
		$requete = $pdo->query('SELECT id_ml FROM '.$bdd_prefixe.'ml_lien WHERE id_entite = '.$id.' AND is_titre = 0');
		$user_ml = $requete->fetchAll();

		foreach($user_ml as $nl)
		{
			$nl_user[] = $nl['id_ml'];
		}
	}

	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml WHERE newsletter = 1');
	if(!($requete === false))
	{
		$newsletters = $requete->fetchAll();
		foreach($newsletters as $nl)
		{
			echo '<option value="'.$nl['id'].'"';
			if(in_array($nl['id'], $nl_user))
				echo ' selected';
			echo '>'.$nl['nom'].'</option>';
		}
	}
						?>
						</select></div>
				</div>
				<?php } ?>
			</div>
			<?php if($fonctionnalites_statut['intervenants']) { ?>
			<h3>Ajouter des enfants</h3>
			<p>Note : Ne pas laisser de champ vide ! Pour les dates de naissances, au pire, mettre 0000-00-00. Format : AAAA-MM-JJ.</p>
			<div class="formulaire" id="tab_enfants">
				<div class="ligne">
					<div class="cellule"><input name="nom_enfants[]" placeholder="Nom" /></div>
					<div class="cellule"><input name="prenom_enfants[]" placeholder="Prénom" /></div>
					<div class="cellule"><input name="classe_enfants[]" placeholder="Classe" /></div>
					<div class="cellule"><input name="date_enfants[]" placeholder="Date de naissance" /></div>
					<div class="cellule"><input type="button" onClick="ajouter_ligne();" value="+" /> <input type="button" onClick="supp_ligne(1);" value="-" /></div>
				</div>
			</div>
			<?php } ?>

			<p>
				<input type="hidden" name="envoi" value="1" />
				<input type="hidden" name="id" value="<?php echo ($mode == MODE_MODIF ? $id : 0); ?>" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
