<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
{
	header('location: connexion.php');
	exit();
}
require('configuration.php');

if(!((int)$_SESSION['permission'] & FAIRE_FACTURE))
{
	header('location: index.php');
	exit();
}

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$adherent = intval($_POST['adherent']);
	$somme = floatval(str_replace(',', '.', $_POST['somme']));
	$date = htmlspecialchars($_POST['date'], ENT_QUOTES);
	$payement = htmlspecialchars($_POST['payement'], ENT_QUOTES);
	//On récupère le type de cotisation
	$dateInitFin = htmlspecialchars($_POST['dateInitFin']);
	$typeFacture = intval($_POST['type']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type WHERE id = '.$typeFacture);
	$type = $requete->fetch();
	$infosTypeFacture = analyserTypeFacture($type, $dateInitFin);
	$pdo->exec('INSERT INTO '.$bdd_prefixe.'factures (id, adherent, somme, date, payement, type, visibilite)
			VALUES ("", "'.$adherent.'", "'.$somme.'", "'.$date.'", "'.$payement.'", '.$infosTypeFacture['id'].', 1)');
	$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET date_fin_cotisation = "'.$infosTypeFacture['fin'].'" WHERE id = '.$adherent);
	// Régénération de la carte d'adhérent au format pdf
	require_once(dirname(__FILE__).'/classes/cardgen.php');
	$cg = new CardGen();
	$cg->getCard($adherent, true);
	// On sort
	header('location: adherents.php');
	exit();
}
elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
{
	header('location: adherents.php');
	exit();
}

$id = intval($_GET['id']);
$mode = MODE_AJOUT;

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
if($requete === false)
{
	header('location: adherents.php');
	exit();
}
$user = $requete->fetch();

//On récupère les types de facture, uniquement de type cotisation
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type WHERE cotisation != 0');
if($requete === false)
{
	header('location: adherents.php');
	exit();
}
$types = $requete->fetchAll();

if(count($types) == 0)
{
	header('location: adherents.php');
	exit();
}

$typesFacture = array();
foreach($types as $type)
{
	$typesFacture[] = analyserTypeFacture($type, $user['date_fin_cotisation']);
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Cotisation</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Cotisation</h2>
		<form action="cotisation.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="adherent">Adherent : </label></div>
					<div class="cellule"><?php echo $user['prenom'].' '.$user['nom']; ?></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label>Fin de cotisation actuelle : </label></div>
					<div class="cellule">
					<?php echo $user['date_fin_cotisation']; ?>
					<input type="hidden" name="dateInitFin" value="<?php echo $user['date_fin_cotisation']; ?>" />
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="cotisation">Type de cotisation : </label></div>
					<div class="cellule">
						<?php
						if(count($typesFacture) == 1)
							echo '<input type="hidden" name="type" value="'.$typesFacture[0]['id'].'" />'.$typesFacture[0]['nom'].' (jusqu\'au '.$typesFacture[0]['fin'].')';
						else
						{
						?>
						<select name="type" style="width: auto;">
							<?php
							foreach($typesFacture as $type)
							{
								echo '<option value="'.$type['id'].'">'.$type['nom'].' (jusqu\'au '.$type['fin'].')</option>';
							}
							?>
						</select>
						<?php
						}
						?>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="date">Date : </label></div>
					<div class="cellule"><input name="date" type="date" value="<?php echo date("Y-m-d"); ?>" readonly /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="somme">Montant : </label></div>
					<div class="cellule"><input name="somme" type="number" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="payement">Moyen de payement : </label></div>
					<div class="cellule"><input name="payement" required /></div>
				</div>
			</div>
			<input type="hidden" name="adherent" value="<?php echo $id; ?>" />
			<input type="hidden" name="envoi" value="1" />
			<input type="submit" id="bouton_valider" value="Valider" />
		</form>
		<?php include('bas_page.php'); ?>
	</body>
</html>
