<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: calendrier.php');

$id = intval($_GET['id']);

$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'calendrier WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Afficher un événement</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2><a href="calendrier.php">Calendrier</a></h2>
	</nav>

	<div id="cadre_stockage"><h3><?php echo $donnees['titre']; ?></h3>

	<p>Date : <?php echo formater_date($donnees['date']); ?><br />
	Type : <?php echo ($donnees['type'] == 0) ? 'Sortie culturelle' : 'Sortie loisir'; ?><br />
	<?php echo $donnees['description']; ?></p>

	<?php if(isset($donnees['lieu']) && $donnees['lieu'] != NULL)
	{ ?>
	<p>Lieu : <?php echo $donnees['lieu']; ?></p>
	<?php }
	if (isset($donnees['latitude']) && $donnees['latitude'] != 0)
	{ ?>
    <!-- DIV xhtml de la carte. La taille peut être spécifiée soit ici,
         dans l'attribut style, soit dans les css.
    -->
    <div id="demo-map" style="border: 1px solid black; width: 800px; height: 150px;"></div>
    <p style="font-size: 7px;"><a href="http://openstreetmap.org/" hreflang="fr" title="OpenStreetMap">Carte OpenStreetMap</a> (Licence CC by-sa 2.0)</p>
    
    
    <!-- Inclusion des bibliothèques javascript OpenLayers et Mapstraction -->
    <script
        type="text/javascript"
        src="http://openlayers.org/api/OpenLayers.js"
    ></script>
    <script
        type="text/javascript"
        src="mapstraction.js"
    ></script>
    
    <script type="text/javascript">
    // Initialisation de la carte.
    var mapstraction = new Mapstraction('demo-map','openlayers');
    mapstraction.addControls({
        pan: false, 
        zoom: 'small',
        map_type: false 
    });

    var centre = new LatLonPoint(<?php echo $donnees['latitude']; ?>,<?php echo $donnees['longitude']; ?>);
    mapstraction.setCenterAndZoom(centre, 16);

    lieu = new Marker(new LatLonPoint(<?php echo $donnees['latitude']; ?>,<?php echo $donnees['longitude']; ?>));
    lieu.setIcon('images/marqueur_geo.png', [18, 31]);
    mapstraction.addMarker(lieu);
    
    </script> 
    <?php } ?>
	</div>
	<?php include('bas_page.php'); ?>
	</body>
</html>