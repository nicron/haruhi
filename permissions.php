<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & MODIFIER_PERMISSION))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'titre');
	$i_visibilite = 0;
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
		{
			$permission = 0;
			if(isset($_POST['page_intervenant'])) { if(in_array($donnees['id'], $_POST['page_intervenant'])) $permission |= PAGE_INTERVENANT; }
			if(isset($_POST['lire_dossier'])) { if(in_array($donnees['id'], $_POST['lire_dossier'])) $permission |= LIRE_DOSSIER; }
			if(isset($_POST['liste_adherent'])) { if(in_array($donnees['id'], $_POST['liste_adherent'])) $permission |= LISTE_ADHERENT; }
			if(isset($_POST['touche_adherent'])) { if(in_array($donnees['id'], $_POST['touche_adherent'])) $permission |= TOUCHE_ADHERENT; }
			if(isset($_POST['voir_facture'])) { if(in_array($donnees['id'], $_POST['voir_facture'])) $permission |= VOIR_FACTURE; }
			if(isset($_POST['faire_facture'])) { if(in_array($donnees['id'], $_POST['faire_facture'])) $permission |= FAIRE_FACTURE; }
			if(isset($_POST['envoyer_fichier'])) { if(in_array($donnees['id'], $_POST['envoyer_fichier'])) $permission |= ENVOYER_FICHIER; }
			if(isset($_POST['avoir_courriel'])) { if(in_array($donnees['id'], $_POST['avoir_courriel'])) $permission |= AVOIR_COURRIEL; }
			if(isset($_POST['modifier_permission'])) { if(in_array($donnees['id'], $_POST['modifier_permission'])) $permission |= MODIFIER_PERMISSION; }
			if(isset($_POST['touche_calendrier'])) { if(in_array($donnees['id'], $_POST['touche_calendrier'])) $permission |= TOUCHE_CALENDRIER; }
			if(isset($_POST['gerer_ml'])) { if(in_array($donnees['id'], $_POST['gerer_ml'])) $permission |= GERER_ML; }
			if(isset($_POST['gerer_type_facture'])) { if(in_array($donnees['id'], $_POST['gerer_type_facture'])) $permission |= GERER_TYPE_FACTURE; }
			if(isset($_POST['consulter_ml'])) { if(in_array($donnees['id'], $_POST['consulter_ml'])) $permission |= CONSULTER_ML; }
			if(isset($_POST['visibilite_fichier'])) { $visibilite = intval($_POST['visibilite_fichier'][$i_visibilite]); } else { $visibilite = 0; }
			if(isset($_POST['carte_adherent'])) { if(in_array($donnees['id'], $_POST['carte_adherent'])) $permission |= CARTE_ADHERENT; }
			mysql_query('UPDATE '.$bdd_prefixe.'titre SET permission = "'.$permission.'", visibilite = '.$visibilite.' WHERE id = '.$donnees['id']);
			$i_visibilite++;
		}
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Permissions</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<form action="permissions.php" method="post">
	<table id="permission">
	<tr><th>Titres</th><th>Accéder à la page des intervenants</th><th>Consulter les dossiers</th><th>Accéder à la liste des adhérents</th><th>Ajouter/Modifier les adhérents</th>
	<th>Consulter les factures</th><th>Faire des factures</th><th>Envoyer des fichiers</th><th>Avoir un courriel</th><th>Modifier les permissions</th>
	<th>Ajouter un événement</th><th>Gérer les mailing lists</th><th>Gérer les types des factures</th><th>Consulter les ML</th><th>Gérer les cartes de membre</th><th>Visibilité des fichiers</th></tr>
	<?php
	$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'titre');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
		{
			echo '<tr><td>'.$donnees['nom'].'</td>';
			echo '<td><input type="checkbox" name="page_intervenant[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & PAGE_INTERVENANT) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="lire_dossier[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & LIRE_DOSSIER) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="liste_adherent[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & LISTE_ADHERENT) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="touche_adherent[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & TOUCHE_ADHERENT) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="voir_facture[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & VOIR_FACTURE) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="faire_facture[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & FAIRE_FACTURE) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="envoyer_fichier[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & ENVOYER_FICHIER) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="avoir_courriel[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & AVOIR_COURRIEL) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="modifier_permission[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & MODIFIER_PERMISSION) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="touche_calendrier[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & TOUCHE_CALENDRIER) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="gerer_ml[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & GERER_ML) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="gerer_type_facture[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & GERER_TYPE_FACTURE) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="consulter_ml[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & CONSULTER_ML) echo ' checked '; echo ' /></td>';
			echo '<td><input type="checkbox" name="carte_adherent[]" value="'.$donnees['id'].'"';  if((int)$donnees['permission'] & CARTE_ADHERENT) echo ' checked '; echo ' /></td>';
			echo '<td><input name="visibilite_fichier[]" style="width: 30px;" value="'.$donnees['visibilite'].'" /></td>';
			echo '</tr>';
		}
	}
	?>
	</table>
	<p>
	<input type="hidden" name="envoi" value="1" />
	<input type="submit" value="Valider" />
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>
