<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['factures'] || !((int)$_SESSION['permission'] & FAIRE_FACTURE))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$mode = MODE_AJOUT;
	$adherentId = intval($_POST['adherent']);
	$somme = htmlspecialchars($_POST['somme'], ENT_QUOTES);
	$date = htmlspecialchars($_POST['date'], ENT_QUOTES);
	$payement = htmlspecialchars($_POST['payement'], ENT_QUOTES);
	$lien = '"'.htmlspecialchars($_POST['lien'], ENT_QUOTES).'"';
	$typeId = intval($_POST['type']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type WHERE id = '.$typeId);
	$type = $requete->fetch();
	if($type['externe'] == 0) //facture interne, pas de lien
		$lien = 'NULL';
	if(strlen($somme) > 0 && strlen($date) > 0 && strlen($payement) > 0 && $somme >= 0)
	{
		$pdo->exec('INSERT INTO '.$bdd_prefixe.'factures (id, adherent, somme, date, payement, type, visibilite, lien)
			VALUES ("", "'.$adherentId.'", "'.$somme.'", "'.$date.'", "'.$payement.'", '.$typeId.', 1, '.$lien.')');
		$id = $pdo->lastInsertId();
		if($type['externe'] == 0) //facture interne, on va copier les valeurs dans factures détails
		{
			//On a besoin des infos de l'adherent
			$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$adherentId);
			$adherent = $requete->fetch();
			$pdo->exec('INSERT INTO `'.$bdd_prefixe.'factures_details` (`id`, `asso`, `nom_adherent`, `adresse_adherent`, `signature`)
			VALUES ('.$id.', "'.$infos_nomasso.'<br />'.$infos_adresseasso.'", "'.$adherent['prenom'].' '.$adherent['nom'].'", "'.$adherent['adresse'].'", "'.signature($date).'")');
		}
	}
	header('location: factures.php');
	exit();
}

$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajout d'une facture</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<script type="application/javascript" src="js/communtateur.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Ajout d'une facture</h2>

		<form action="ajout_facture.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="adherent">Adhérent :</label></div>
					<div class="cellule">
						<select name="adherent" required>
							<?php $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents');
							if(!($requete === false))
							{
								$users = $requete->fetchAll();
								foreach($users as $user)
								{
									echo '<option value="'.$user['id'].'">'.$user['prenom'].' '.$user['nom'].'</option>';
								}
							} ?>
						</select>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="somme">Montant : </label></div>
					<div class="cellule"><input type="number" min="0" name="somme" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="date">Date : </label></div>
					<div class="cellule"><input name="date" type="date" value="<?php echo date("Y-m-d"); ?>" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="payement">Moyen de paiement : </label></div>
					<div class="cellule"><input name="payement" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="type">Type : </label></div>
					<div class="cellule">
						<select name="type" id="type-select">
							<?php $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type');
							if(!($requete === false))
							{
								$types = $requete->fetchAll();
								$externe = array();
								$interne = array();
								foreach($types as $type)
								{
									echo '<option value="'.$type['id'].'">'.$type['nom'].'</option>';
									if($type['externe'] == 1)
										$externe[] = $type['id'];
									else
										$interne[] = $type['id'];
								}
							} ?>
						</select></div>
				</div>
				<div class="ligne" id="champ_lien">
					<div class="cellule intitule"><label name="lien">Lien : </label></div>
					<div class="cellule"><input name="lien" /></div>
				</div>
			</div>
			<p>
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>
		<script type="application/javascript">
			communtateur_select('type', 'type-select');
			<?php
			//On fait la correspondance des valeurs
			foreach($externe as $id)
			{
				echo 'communtateur_souscrire(\'type\', '.$id.', \'champ_lien\', \'visible\');'."\n";
			}

			foreach($interne as $id)
			{
				echo 'communtateur_souscrire(\'type\', '.$id.', \'champ_lien\', \'cache\');'."\n";
			}

			//Et on va régler le communtateur sur la première valeur
			echo 'communtateur_mettreValeur(\'type\', '.$types[0]['id'].')';
			?>
		</script>
		<?php include('bas_page.php'); ?>
	</body>
</html>
