<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<link rel="icon" type="image/png" href="images/favicon.png" />
		<title>Haruhi → Titres</title>

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Titres</h2>

	<table>
	<tr><th>Id</th><th>Nom</th><th>Action</th></tr>
	<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'titre');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
		{
			echo '<tr><td style="background-color:'.$donnees['couleur'].'">'.$donnees['id'].'</td>';
			echo '<td>'.$donnees['nom'].'</td><td><a href="modif_titre.php?id='.$donnees['id'].'">Modifier</a></td></tr>';
		}
	} ?>
	</table>

	<p><a href="ajout_titre.php">Ajouter un titre</a>
	<?php if((int)$_SESSION['permission'] & MODIFIER_PERMISSION) { ?><br /><a href="permissions.php">Gérer les permissions</a><?php } ?></p>

	<?php include('bas_page.php'); ?>
	</body>
</html>