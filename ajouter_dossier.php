<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['intervenants'] || !((int)$_SESSION['permission'] & LIRE_DOSSIER))
	header('location: index.php');
if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	//$message = htmlspecialchars($_POST['message'], ENT_QUOTES);
	$message = '';
	foreach($questions_tutorat as $i => $q)
		$message .= '<b>'.$q.'</b>'."\n\n".htmlspecialchars($_POST['question_'.$i])."\n";
	$enfant = intval($_POST['enfant']);
	if(strlen($message) > 0)
	{
	    $requete = mysql_query('SELECT id_intervenant FROM '.$bdd_prefixe.'enfants WHERE id = '.$enfant);
	    $donnees = mysql_fetch_array($requete);
		mysql_query('INSERT INTO '.$bdd_prefixe.'dossiers (message, intervenant, enfant, date)
			VALUES ("'.$message.'", "'.$donnees['id_intervenant'].'", "'.$enfant.'", CURDATE())');
	}
	header('location: dossiers.php');
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajouter un dossier</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Ajouter un dossier</h2>

	<form action="ajouter_dossier.php" method="post">
	<p class="dossier_formulaire">
	Enfant : 
	<select name="enfant">
	<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'enfants WHERE id_intervenant != 0');
	while($donnees = mysql_fetch_array($requete))
	{
	    echo '<option value="'.$donnees['id'].'">'.$donnees['prenom'].' '.$donnees['nom'].'</option>';
	}
	?></select><br />
	<?php foreach($questions_tutorat as $i => $q)
	{
		echo $q.'<br /><textarea name="question_'.$i.'"></textarea><br />';
	} ?>
	<input type="hidden" name="envoi" value="1" />
	<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>