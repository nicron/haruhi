<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	if($_POST['mdp'] == $_POST['mdp_re'] && strlen($_POST['mdp']) > 0)
	{
		$mdp = hash('sha512', $_POST['mdp']);
		mysql_query('UPDATE '.$bdd_prefixe.'membres SET mdp = "'.$mdp.'" WHERE id = '.$_SESSION['id']);
		header('location: mon_compte.php');
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification du mot de passe</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Modifier son mot de passe</h2>

	<form action="modif_mdp.php" method="post">
	<p>
		<label name="mdp">Mot de passe : <input type="password" name="mdp" /></label><br />
		<label name="mdp_re">Mot de passe (encore) : <input type="password" name="mdp_re" /></label><br />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>