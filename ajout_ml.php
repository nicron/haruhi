<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['ovh'] || !((int)$_SESSION['permission'] & GERER_ML))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$adresse = htmlspecialchars($_POST['adresse'], ENT_QUOTES);
	$proprio = htmlspecialchars($_POST['proprio'], ENT_QUOTES);
	$reponse = htmlspecialchars($_POST['reponse'], ENT_QUOTES);
	if(isset($_POST['moderer_msg']) && $_POST['moderer_msg'] == 1)
		$moderer_msg = true;
	else
		$moderer_msg = false;
	if(isset($_POST['membres_msg']) && $_POST['membres_msg'] == 1)
		$membres_msg = true;
	else
		$membres_msg = false;
	if(isset($_POST['moderer_inscription']) && $_POST['moderer_inscription'] == 1)
		$moderer_inscription = true;
	else
		$moderer_inscription = false;

	if(isset($_POST['newsletter']) && $_POST['newsletter'] == 1)
	    $newsletter = 1;
	else
	    $newsletter = 0;
		
	if(isset($_POST['cotisation']) && $_POST['cotisation'] == 1)
	    $cotisation = 1;
	else
	    $cotisation = 0;

	try {
		$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

		//login
		$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

		//on créer la ML
		$soap->mailingListAdd($session, $domaine_ovh, $adresse, $proprio, "fr", $reponse, $moderer_msg, $membres_msg, $moderer_inscription);
		mysql_query('INSERT INTO '.$bdd_prefixe.'ml (nom, adresse, newsletter, cotisation) VALUES ("'.$nom.'", "'.$adresse.'@'.$domaine_ovh.'", '.$newsletter.', '.$cotisation.')');

		$id_ml = mysql_insert_id();

		//On ajoute les gens qu'il faut à cette dernière
		if(isset($_POST['titre']) && count($_POST['titre']) > 0)
		{
			foreach($_POST['titre'] as $titre)
			{
				$titre = intval($titre);
				$requete = mysql_query('SELECT courriel FROM '.$bdd_prefixe.'adherents WHERE titre = '.$titre);
				while($donnees = mysql_fetch_array($requete))
				{
					if($donnees['courriel'] != NULL)
					    $soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $donnees['courriel']);
				}

				mysql_query('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.$id_ml.', '.$titre.', 1);');
			}
		}

		if(isset($_POST['adherent']) && count($_POST['adherent']) > 0)
		{
			foreach($_POST['adherent'] as $adherent)
			{
				$adherent = intval($adherent);
				$requete = mysql_query('SELECT courriel FROM '.$bdd_prefixe.'adherents WHERE id = '.$adherent);
				$donnees = mysql_fetch_array($requete);
				if($donnees['courriel'] != NULL)
				    $soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $donnees['courriel']);

				mysql_query('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.$id_ml.', '.$adherent.', 0);');
			}
		}

		//logout
		$soap->logout($session);

		header('location: liste_ml.php');
	} catch(SoapFault $fault) {
		echo $fault;
	}

}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajouter une mailing list</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Ajouter une Mailing List</h2>
	<form action="ajout_ml.php" method="post">
	<p>Nom : <input name="nom" /><br />
	Adresse : <input name="adresse" />@<?php echo $domaine_ovh; ?><br />
	Courriel du propriétaire : <input name="proprio" /><br />
	Réponse à : <select name="reponse"><option value="mailinglist">À la Mailing List</option><option value="lastuser">À l'auteur</option></select><br />
	Modérer les messages ? <input type="checkbox" name="moderer_msg" value="1" /><br />
	Seuls les membres peuvent envoyer des messages <input type="checkbox" name="membres_msg" value="1" /><br />
	Inscriptions contrôlées ? <input type="checkbox" name="moderer_inscription" value="1" /><br />
	Uniquement à jour de leur cotisation ? <input type="checkbox" name="cotisation" value="1" /><br />
	Liste d'information ? <input type="checkbox" name="newsletter" value="1" /><br />
	Membres de la ML :<br />
	Titres :<br />
	<select multiple name="titre[]">
	<?php $requete = mysql_query('SELECT id, nom FROM '.$bdd_prefixe.'titre');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
			echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
	?>
	</select><br />
	Adherents :<br />
	<select multiple name="adherent[]">
	<?php $requete = mysql_query('SELECT id, nom, prenom FROM '.$bdd_prefixe.'adherents');
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
			echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].' '.$donnees['prenom'].'</option>';
	}
	?>
	</select><br />
	<input type="hidden" name="envoi" value="1" />
	<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>