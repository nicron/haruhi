<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['factures'] || !((int)$_SESSION['permission'] & VOIR_FACTURE))
	header('location: index.php');

if(isset($_GET['visibilite']) && intval($_GET['visibilite']) > 0)
{
	$requete = mysql_query('SELECT id, visibilite FROM '.$bdd_prefixe.'factures WHERE id = '.intval($_GET['visibilite']));
	$donnees = mysql_fetch_array($requete);
	if($donnees['id'] != 0)
	{
		if($donnees['visibilite'] == 0)
			mysql_query('UPDATE '.$bdd_prefixe.'factures SET visibilite = 1 WHERE id ='.$donnees['id']);
		else
			mysql_query('UPDATE '.$bdd_prefixe.'factures SET visibilite = 0 WHERE id ='.$donnees['id']);
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Factures</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<nav id="panneau_page">
			<h2>Factures</h2>
			<?php if((int)$_SESSION['permission'] & FAIRE_FACTURE) { ?>
			<p>
				<a href="ajout_facture.php">Ajouter une facture</a><br />
				<a href="import_facture.php">Importer des factures</a>
			</p>
			<?php }
			if((int)$_SESSION['permission'] & GERER_TYPE_FACTURE) { ?>
			<p><a href="gerer_type_factures.php">Gérer les types de factures</a></p>
			<?php } ?>
		</nav>

		<div id="cadre_stockage">
			<table>
				<tr><th>N°</th><th>Adhérent</th><th>Montant</th><th>Date</th><th>Type</th><th>Actions</th></tr>
				<?php
				$requete = $pdo->query('SELECT *, f.id AS id, t.nom AS nom_type_facture, a.nom AS nom
FROM '.$bdd_prefixe.'factures f LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = f.adherent LEFT JOIN '.$bdd_prefixe.'factures_type t ON t.id = f.type');
				if(!($requete === false))
				{
					$factures = $requete->fetchAll();
					foreach($factures as $facture)
					{
						if($facture['visibilite'] == 1 || (int)$_SESSION['permission'] & FAIRE_FACTURE)
						{
							echo '<tr>';
							echo '<td style="text-align: center;">'.$facture['id'].'</td>';
							echo '<td>'.$facture['prenom'].' '.$facture['nom'].'</td><td>'.number_format($facture['somme'], 2, ',', ' ').' €</td><td>'.formater_date($facture['date']).'</td>';
							echo '<td>'.$facture['nom_type_facture'].'</td>';
							echo '<td>';
							if($facture['externe'] == 0) //Facture interne
								echo '<a href="afficher_facture.php?id='.$facture['id'].'">';
							else //Facture externe
								echo '<a target="_blank" href="'.$facture['lien'].'">';
							echo '<img src="images/stockage_voir.png" title="Afficher" alt="Afficher" /></a>';
							if((int)$_SESSION['permission'] & FAIRE_FACTURE)
							{
								echo ' <a href="factures.php?visibilite='.$facture['id'].'">';
								if($facture['visibilite'] == 0)
									echo '<img src="images/facture_afficher.png" title="Rendre visible" alt="Rendre visible" />';
								else
									echo '<img src="images/facture_masquer.png" title="Masquer" alt="Masquer" />';
								echo '</a>';
							}
							echo '</td></tr>';
						}
					}
				}
				?>
			</table>
		</div>
		<?php include('bas_page.php'); ?>
	</body>
</html>
