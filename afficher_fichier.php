<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['stockage'])
	header('location: index.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: stockage.php');

$id = intval($_GET['id']);

$requete = mysql_query('SELECT nom, taille, version, visibilite FROM '.$bdd_prefixe.'fichiers WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);

if($donnees['visibilite'] > $_SESSION['visibilite'])
	header('location: stockage.php');

$coupe = explode('.', $donnees['nom']);
$coupe[count($coupe) - 2] .= '_v'.$donnees['version'];
$nom_fichier = implode('.', $coupe);

$finfo = finfo_open(FILEINFO_MIME_TYPE);

header('Content-Transfer-Encoding: binary');
header('Content-Type: '.finfo_file($finfo, 'fichiers/'.$nom_fichier));
header('Content-Disposition: attachment; filename="'.$donnees['nom'].'"');
header('Content-Length: '.$donnees['taille']);
 
readfile('fichiers/'.$nom_fichier);
?>
