<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['ovh'] || !((int)$_SESSION['permission'] & GERER_ML))
	header('location: index.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: liste_ml.php');

$id = intval($_GET['id']);

$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'ml WHERE id = '.$id);
$donnees = mysql_fetch_array($requete);

$requete_lien = mysql_query('SELECT * FROM '.$bdd_prefixe.'ml_lien WHERE id_ml = '.$id);
$titres = [];
$adherents = [];
while($donnees_lien = mysql_fetch_array($requete_lien))
{
	if($donnees_lien['is_titre'] == 1)
		$titres[] = $donnees_lien['id_entite'];
	else
		$adherents[] = $donnees_lien['id_entite'];
}

$nom = $donnees['nom'];
$explode = explode('@', $donnees['adresse']);
$adresse = $explode[0];
$cotisation = $donnees['cotisation'];

try {
	$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

	//login
	$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

	//on récupère la liste
	$liste_adresse = $soap->mailingListSubscriberList($session, $domaine_ovh, $adresse);

	$liste_ajout = [];
	$liste_ok = [];
	$liste_supp = [];
	$id_ml = mysql_insert_id();

	//On ajoute les gens qu'il faut à cette dernière
	if(count($titres) > 0)
	{
		foreach($titres as $titre)
		{
			$titre = intval($titre);
			if($cotisation == 1)
				$where = ' AND date_fin_cotisation >= NOW()';
			else
				$where = '';
			$requete = mysql_query('SELECT courriel FROM '.$bdd_prefixe.'adherents WHERE titre = '.$titre.$where);
			while($donnees = mysql_fetch_array($requete))
			{
				if($donnees['courriel'] != NULL)
				{
					if(in_array($donnees['courriel'], $liste_adresse))
						$liste_ok[] = $donnees['courriel'];
					else
						$liste_ajout[] = $donnees['courriel'];
				}
			}
		}
	}

	if(count($adherents) > 0)
	{
		foreach($adherents as $adherent)
		{
			$adherent = intval($adherent);
			$requete = mysql_query('SELECT courriel FROM '.$bdd_prefixe.'adherents WHERE id = '.$adherent);
			$donnees = mysql_fetch_array($requete);
			if($donnees['courriel'] != NULL)
			{
				if(in_array($donnees['courriel'], $liste_adresse))
					$liste_ok[] = $donnees['courriel'];
				else
					$liste_ajout[] = $donnees['courriel'];
			}
		}
	}

	foreach($liste_adresse as $mail)
	{
		if(!in_array($mail, $liste_ok) && !in_array($mail, $liste_ajout))
			$liste_supp[] = $mail;
	}

	foreach($liste_ajout as $mail)
		$soap->mailingListSubscriberAdd($session, $domaine_ovh, $adresse, $mail);

	foreach($liste_supp as $mail)
		$soap->mailingListSubscriberDel($session, $domaine_ovh, $adresse, $mail);

	//logout
	$soap->logout($session);
	header('location: liste_ml.php');
} catch(SoapFault $fault) {
	echo $fault;
}
?>