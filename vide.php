<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Connexion</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<?php include('bas_page.php'); ?>
	</body>
</html>