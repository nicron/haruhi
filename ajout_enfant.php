<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$classe = htmlspecialchars($_POST['classe']);
	$adherent = intval($_POST['adherent']);
	$intervenant = intval($_POST['intervenant']);
	$naissance = htmlspecialchars($_POST['naissance'], ENT_QUOTES);
	mysql_query('INSERT INTO '.$bdd_prefixe.'enfants (id, prenom, nom, classe, naissance, id_parent, id_intervenant)
			VALUES ("", "'.$prenom.'", "'.$nom.'", "'.$classe.'", "'.$naissance.'", "'.$adherent.'", "'.$intervenant.'")');
	//header('location: enfant.php?id='.$adherent);
	header('location: voir_adherent.php?id='.$adherent);
} elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: adherents.php');

$id = intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Ajout d'un enfant</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Ajouter un enfant</h2>

	<form action="ajout_enfant.php" method="post">
	<p>
		<label name="nom">Nom : <input name="nom" /></label><br />
		<label name="prenom">Prénom : <input name="prenom" /></label><br />
		<label name="classe">Classe : <input name="classe" /></label><br />
		<label name="naissance">Date de naissance : <input name="naissance" /> (Format : AAAA-MM-JJ)</label><br />
		Intervenant : <select name="intervenant">
		<option value="0">Aucun</option>
		<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'adherents');
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<option value="'.$donnees['id'].'">'.$donnees['prenom'].' '.$donnees['nom'].'</option>';
			}
		} ?>
		</select><br />
		<input type="hidden" name="adherent" value="<?php echo $id; ?>" />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>
