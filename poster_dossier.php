<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['intervenants'] || !((int)$_SESSION['permission'] & PAGE_INTERVENANT))
	header('location: index.php');
if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	//$message = htmlspecialchars($_POST['message'], ENT_QUOTES);
	$message = '';
	foreach($questions_tutorat as $i => $q)
		$message .= '<b>'.$q.'</b>'."\n\n".htmlspecialchars($_POST['question_'.$i])."\n";
	$id = intval($_POST['id']);
	if(strlen($message) > 0)
	{
		mysql_query('INSERT INTO '.$bdd_prefixe.'dossiers (message, intervenant, enfant, date)
			VALUES ("'.$message.'", "'.$_SESSION['id_adherent'].'", "'.$id.'", CURDATE())');
		//Truc caca à suivre
		mail('none@perdu.com', "Nouveau dossier sur Haruhi", wordwrap("Un intervenant a posté un dossier sur Haruhi \\o\\\n
		C'est tellement rare qu'il faut aller le lire tout de suite /o/\n
		Son adresse : ".'http://perdu.com'."/lire_dossier.php?id=".mysql_insert_id()." \\o/\n
		Bonne journée :)\n
		Votre déesse préféré, Haruhi", 70));
	}
	header('location: intervenant.php');
} elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: intervenant.php');

$id = intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Poster un dossier</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Poster un dossier</h2>

	<form action="poster_dossier.php" method="post">
	<p class="dossier_formulaire">
	<?php foreach($questions_tutorat as $i => $q)
	{
		echo $q.'<br /><textarea name="question_'.$i.'"></textarea><br />';
	} ?>
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<input type="hidden" name="envoi" value="1" />
	<input type="submit" value="Valider" />
	</p>
	</form>
	<?php include('bas_page.php'); ?>
	</body>
</html>