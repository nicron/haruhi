<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: factures.php');

$id = intval($_GET['id']);

$requete = $pdo->query('SELECT *, f.id AS id_facture, f.adherent AS id_adherent FROM '.$bdd_prefixe.'factures f
INNER JOIN '.$bdd_prefixe.'factures_details d ON d.id = f.id
LEFT JOIN '.$bdd_prefixe.'factures_type t ON t.id = f.type
WHERE f.id = '.$id);
$facture = $requete->fetch();

if(!$fonctionnalites_statut['factures'] || ($facture['id_adherent'] != $_SESSION['id_adherent']) && !((int)$_SESSION['permission'] & VOIR_FACTURE))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Afficher la facture</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
		<link rel="stylesheet" href="edition_facture.css" type="text/css" media="print">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<div id="num">N°<?php echo $facture['id_facture']; ?></div>
	<h2 style="text-align: center">Facture</h2>

	<div id="cadre_asso">
		<div id="logo_asso"><img src="images/logo_asso_facture.png" alt="Logo" /></div>
		Association : <br /><br />
		<div class="donnees"><?php echo $facture['asso']; ?></div>
		<br />
	</div>

	<div id="cadre_adherent">
		<div id="cadre_nom">Nom de l'adhérent : <div class="donnees"><?php echo $facture['nom_adherent']; ?></div></div>
		<div id="cadre_adresse">Adresse de l'adhérent : <div class="donnees"><?php echo $facture['adresse_adherent']; ?></div></div>
	</div>

	<div id="cadre_montant">
	<?php echo $facture['phrase']; ?> : <div class="donnees"><?php echo number_format($facture['somme'], 2, ',', ' '); ?> €</div>
	</div>

		<?php if($fonctionnalites_statut['intervenants']) { ?>
	<div id="cadre_enfant">
		Enfant(s) inscrit(s) pour l'année :
		<div class="donnees"><?php $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'enfants WHERE id_parent = '.$donnees['id_adherent']);
		$i = 0;
		if(!($requete === false))
		{
			$enfants = $requete->fetchAll();
			foreach($enfants as $enfant)
			{
				if($i != 0)
					echo ' ';
				
				echo $enfant['prenom'];
				list($an, $mois, $jour) = explode("-", $enfant['naissance']);
				if(intval($an) == 0)
				{
					if(intval($an) == 0)
						$age = 0;
					elseif(($jour > date("d") && $mois == date("m")) || $mois > date("m"))
						$age = date("Y") - $an - 1;
					else
						$age = date("Y") - $an;

					if($age != 0)
						echo ' ('.$age.' ans)';
				}
				$i++;
			}
		}
		if($i == 0) echo 'Aucun';
		?></div>
	</div>
		<?php } ?>

	<div id="cadre_divers">
		<div id="cadre_signature">Signature : <div id="nom_president"><?php echo $facture['signature']; ?></div></div>
		<div id="cadre_date">Date : <div class="donnees"><?php echo formater_date($facture['date']); ?></div></div>
		<div id="cadre_payement">Moyen de paiement : <div class="donnees"><?php echo $facture['payement']; ?></div></div>
	</div>

	<div id="bas_page_facture">
	<strong><?php echo $infos_nomasso; ?></strong> Association Loi 1901<br />
	Siège Social : <?php echo $infos_adresseasso; ?><br />
	Site Web : <?php echo $infos_siteasso; ?>
	</div>

	<?php include('bas_page.php'); ?>
	</body>
</html>
