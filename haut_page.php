<header>
<!-- 	<h1>Haruhi</h1> -->
	<div id="logo"><img src="images/logo.png" alt="Haruhi" /></div>

	<nav>
	<a href="index.php">Accueil</a>
	<?php if($fonctionnalites_statut['intervenants'] && (int)$_SESSION['permission'] & PAGE_INTERVENANT) { ?><a href="intervenant.php">Intervenant</a><?php } ?>
	<?php if($fonctionnalites_statut['intervenants'] && (int)$_SESSION['permission'] & LIRE_DOSSIER) { ?><a href="dossiers.php">Dossiers</a><?php } ?>
	<?php if((int)$_SESSION['permission'] & LISTE_ADHERENT) { ?><a href="adherents.php">Adhérents</a><?php } ?>
	<?php if($fonctionnalites_statut['factures'] && (int)$_SESSION['permission'] & VOIR_FACTURE) { ?><a href="factures.php">Factures</a><?php } ?>
	<?php if($fonctionnalites_statut['stockage']) { ?><a href="stockage.php">Stockage</a><?php } ?>
	<?php if($fonctionnalites_statut['calendrier']) { ?><a href="calendrier.php">Calendrier</a><?php } ?>
	<?php if((int)$_SESSION['permission'] & TOUCHE_ADHERENT) { ?><a href="titres.php">Titres</a><?php } ?>
	<?php if($fonctionnalites_statut['ovh'] && (int)$_SESSION['permission'] & GERER_ML) { ?><a href="liste_ml.php">Listes ML</a><?php } ?>
	<?php if($fonctionnalites_statut['yuki'] && (int)$_SESSION['permission'] & CONSULTER_ML) { ?><a href="voir_ml.php">Mailing-List</a><?php } ?>
	<?php if($fonctionnalites_statut['cartemembre'] && (int)$_SESSION['permission'] & CARTE_ADHERENT) { ?><a href="cardgen.php">Générateur Cartes</a><?php } ?>
	<a href="mon_compte.php">Mon compte</a>
	<a href="<?php echo $infos_siteasso; ?>">Site de l'asso</a>
	<a href="deconnexion.php">Deconnexion</a>
	</nav>
</header>
<section>
