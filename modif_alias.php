<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & AVOIR_COURRIEL))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$courriel = htmlspecialchars($_POST['courriel'], ENT_QUOTES);
	$requete = mysql_query('SELECT a.nom, a.prenom, m.mail FROM '.$bdd_prefixe.'membres m LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id_membre = m.id WHERE m.id = '.$_SESSION['id']);
	$donnees = mysql_fetch_array($requete);

	try {
	$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

	$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

	//popAdd
	$soap->redirectedEmailModify($session, $domaine_ovh, $donnees['prenom'].'.'.$donnees['nom'], $donnees['mail'], $courriel, "");

	//logout
	$soap->logout($session);

	header('location: mon_compte.php');
	} catch(SoapFault $fault) {
	echo $fault;
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification d'un alias</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Modifier son alias</h2>

	<form action="modif_alias.php" method="post">
	<p>
		<label name="courriel">Courriel où sera rediriger les mails : <input name="courriel" /></label><br />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Valider" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>