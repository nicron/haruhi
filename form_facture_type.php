<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['factures'] || !((int)$_SESSION['permission'] & GERER_TYPE_FACTURE))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;

	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);

	//Externe/Interne
	$externe = intval($_POST['externe']);
	if($externe == 1) //Externe à oui, pas de valeur pour $phrase
		$phrase = 'NULL';
	else
		$phrase = '"'.htmlspecialchars($_POST['phrase'], ENT_QUOTES).'"';

	//Cotisation
	$cotisation = intval($_POST['cotisation']);
	if($cotisation == 0) //Pas de type cotisation
	{
		$duree = 'NULL';
		$unite = 'NULL';
		$debut_periode = 'NULL';
		$fin_periode = 'NULL';
		$termine_periode = 'NULL';
	}
	elseif($cotisation == 1) //Type durée
	{
		$duree = intval($_POST['duree_nombre']);
		switch($_POST['duree_unite'])
		{
			case 'jour':
				$unite = 1;
				break;
			case 'mois':
				$unite = 2;
				break;
			case 'annee':
				$unite = 3;
				break;
			default:
				$unite = 1;
		}
		$debut_periode = 'NULL';
		$fin_periode = 'NULL';
		$termine_periode = 'NULL';
	}
	elseif($cotisation == 2) //Type période
	{
		$debut = htmlspecialchars($_POST['periode_debut']);
		$fin = htmlspecialchars($_POST['periode_fin']);
		$termine = htmlspecialchars($_POST['periode_termine']);

		//On coupe tout ça ^^
		$explode_debut = explode('/', $debut);
		$debut_periode = '"0000-'.$explode_debut[1].'-'.$explode_debut[0].'"';

		$explode_fin = explode('/', $fin);
		$fin_periode = '"'.intval($_POST['periode_fin_an']).'-'.$explode_fin[1].'-'.$explode_fin[0].'"';

		$explode_termine = explode('/', $termine);
		$termine_periode = '"'.intval($_POST['periode_termine_an']).'-'.$explode_termine[1].'-'.$explode_termine[0].'"';

		$duree = 'NULL';
		$unite = 'NULL';
	}

	if(strlen($nom) > 0)
	{
		if($mode == MODE_AJOUT)
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'factures_type (nom, externe, phrase, cotisation, duree, unite, debut_periode, fin_periode, termine_periode) VALUES ("'.$nom.'", "'.$externe.'", '.$phrase.', '.$cotisation.', '.$duree.', '.$unite.', '.$debut_periode.', '.$fin_periode.', '.$termine_periode.')');
		elseif($mode == MODE_MODIF)
			$pdo->exec('UPDATE '.$bdd_prefixe.'factures_type SET nom = "'.$nom.'", externe = "'.$externe.'", phrase = '.$phrase.', cotisation = '.$cotisation.', duree = '.$duree.', unite = '.$unite.', debut_periode =  '.$debut_periode.', fin_periode = '.$fin_periode.', termine_periode = '.$termine_periode.' WHERE id = '.$id);
	}

	header('location: gerer_type_factures.php');
	exit();
}

function prendreDate($date)
{
	$explode = explode('-', $date);
	return $explode[2].'/'.$explode[1];
}

function prendreAnnee($date)
{
	$explode = explode('-', $date);
	return intval($explode[0]);
}

if(isset($_GET['id']) && intval($_GET['id']) > 0)
{
	$id = intval($_GET['id']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type WHERE id = '.$id);
	$type_facture = $requete->fetch();
	$mode = MODE_MODIF;
} else
	$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un type de facture</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<script type="application/javascript" src="js/communtateur.min.js"></script>
		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un type de facture</h2>

		<form action="form_facture_type.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom : </label></div>
					<div class="cellule"><input name="nom" value="<?php if($mode == MODE_MODIF) echo $type_facture['nom']; ?>" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label>Factures externes :</label></div>
					<div class="cellule">
						<input type="radio" id="externe_non" name="externe" value="0" <?php if(($mode == MODE_MODIF && $type_facture['externe'] == 0) || $mode == MODE_AJOUT) echo 'checked'; ?> />
						<label class="label-radio" for="externe_non">Non</label>
						<input type="radio" id="externe_oui" name="externe" value="1" <?php if($mode == MODE_MODIF && $type_facture['externe'] == 1) echo 'checked'; ?> />
						<label class="label-radio" for="externe_oui">Oui</label>
					</div>
				</div>
				<div class="ligne" id="champ_phrase">
					<div class="cellule intitule"><label name="phrase">Phrase : </label></div>
					<div class="cellule"><input name="phrase" value="<?php if($mode == MODE_MODIF) echo $type_facture['phrase']; ?>" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label>Cotisation :</label></div>
					<div class="cellule">
						<input type="radio" id="cotisation_non" name="cotisation" value="0" <?php if(($mode == MODE_MODIF && $type_facture['cotisation'] == 0) || $mode == MODE_AJOUT) echo 'checked'; ?> />
						<label class="label-radio" for="cotisation_non">Non</label>
						<input type="radio" id="cotisation_duree" name="cotisation" value="1" <?php if($mode == MODE_MODIF && $type_facture['cotisation'] == 1) echo 'checked'; ?> />
						<label class="label-radio" for="cotisation_duree">Par ajout de durée</label>
						<input type="radio" id="cotisation_periode" name="cotisation" value="2" <?php if($mode == MODE_MODIF && $type_facture['cotisation'] == 2) echo 'checked'; ?> />
						<label class="label-radio" for="cotisation_periode">Par période</label>
					</div>
				</div>
				<div class="ligne" id="champ_duree">
					<div class="cellule intitule"><label name="duree_nombre">Durée supplémentaire : </label></div>
					<div class="cellule">
						+
						<input name="duree_nombre" type="number" value="<?php if($mode == MODE_MODIF) echo $type_facture['duree']; ?>" />
						<select name="duree_unite">
							<option value="jour" <?php if($mode == MODE_MODIF && $type_facture['unite'] == 1) echo 'selected'; ?>>jour(s)</option>
							<option value="mois" <?php if($mode == MODE_MODIF && $type_facture['unite'] == 2) echo 'selected'; ?>>mois</option>
							<option value="annee" <?php if($mode == MODE_MODIF && $type_facture['unite'] == 3) echo 'selected'; ?>>année(s)</option>
						</select>
					</div>
				</div>
				<div class="ligne" id="champ_periode">
					<div class="cellule intitule"><label>Période : </label></div>
					<div class="cellule">
						Du
						<input name="periode_debut" placeholder="JJ/MM" value="<?php if($mode == MODE_MODIF) echo prendreDate($type_facture['debut_periode']); ?>" /> de l'an n
						au
						<input name="periode_fin" placeholder="JJ/MM" value="<?php if($mode == MODE_MODIF) echo prendreDate($type_facture['fin_periode']); ?>" /> de l'an n+
						<input name="periode_fin_an" type="number" value="<?php if($mode == MODE_MODIF) echo prendreAnnee($type_facture['fin_periode']); ?>" />
					</div>
				</div>
				<div class="ligne" id="champ_periode_termine">
					<div class="cellule intitule"><label>Fin de cotisation : </label></div>
					<div class="cellule">
						<input name="periode_termine" placeholder="JJ/MM" value="<?php if($mode == MODE_MODIF) echo prendreDate($type_facture['termine_periode']); ?>" /> de l'an n+
						<input name="periode_termine_an" type="number" value="<?php if($mode == MODE_MODIF) echo prendreAnnee($type_facture['termine_periode']); ?>" />
					</div>
				</div>
			</div>
			<p>
				<input type="hidden" name="id" value="<?php if($mode == MODE_MODIF) echo $id; ?>" />
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>
		<script type="application/javascript">
			//On met en place le communtateur
			communtateur_creer('externe', false, 'externe_non');
			communtateur_creer('externe', true, 'externe_oui');
			communtateur_creer('cotisation', 0, 'cotisation_non');
			communtateur_creer('cotisation', 1, 'cotisation_duree');
			communtateur_creer('cotisation', 2, 'cotisation_periode');

			//On enregistre les champs
			communtateur_souscrire('externe', false, 'champ_phrase', 'visible');
			communtateur_souscrire('externe', true, 'champ_phrase', 'cache');

			communtateur_souscrire('cotisation', 0, 'champ_duree', 'cache');
			communtateur_souscrire('cotisation', 1, 'champ_duree', 'visible');
			communtateur_souscrire('cotisation', 2, 'champ_duree', 'cache');

			communtateur_souscrire('cotisation', 0, 'champ_periode', 'cache');
			communtateur_souscrire('cotisation', 1, 'champ_periode', 'cache');
			communtateur_souscrire('cotisation', 2, 'champ_periode', 'visible');

			communtateur_souscrire('cotisation', 0, 'champ_periode_termine', 'cache');
			communtateur_souscrire('cotisation', 1, 'champ_periode_termine', 'cache');
			communtateur_souscrire('cotisation', 2, 'champ_periode_termine', 'visible');

			//On met le communtateur sur la position de base
			communtateur_mettreValeur('externe', <?php if(($mode == MODE_MODIF && $type_facture['externe'] == 0) || $mode == MODE_AJOUT) echo 'false'; else echo 'true'; ?>);
			communtateur_mettreValeur('cotisation', <?php if($mode == MODE_AJOUT) echo 0; else echo $type_facture['cotisation']; ?>);
		</script>
		<?php include('bas_page.php'); ?>
	</body>
</html>
