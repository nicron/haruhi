<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!$fonctionnalites_statut['yuki'] || !((int)$_SESSION['permission'] & CONSULTER_ML))
	header('location: index.php');

if(isset($_GET['visibilite']) && intval($_GET['visibilite']) > 0)
{
	$requete = mysql_query('SELECT id, visibilite FROM '.$bdd_prefixe.'factures WHERE id = '.intval($_GET['visibilite']));
	$donnees = mysql_fetch_array($requete);
	if($donnees['id'] != 0)
	{
		if($donnees['visibilite'] == 0)
			mysql_query('UPDATE '.$bdd_prefixe.'factures SET visibilite = 1 WHERE id ='.$donnees['id']);
		else
			mysql_query('UPDATE '.$bdd_prefixe.'factures SET visibilite = 0 WHERE id ='.$donnees['id']);
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Consulter ML</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		<script type="text/javascript">
		function lire(id)
		{
			if(document.getElementById('m'+id).className == "fermer")
			{
				document.getElementById('m'+id).className = 'ouvert';
			} else {
				document.getElementById('m'+id).className = 'fermer';
			}
		}
		</script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2>Mailing-List</h2>
	    <form action="voir_ml.php" method="get">
	    <p>Filtrer :
	    <select name="ml">
	    <?php $requete = mysql_query('SELECT id, nom FROM '.$bdd_prefixe.'ml');
	    if(!($requete === false))
	    {
		    while($donnees = mysql_fetch_array($requete))
		    {
			    echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
		    }
	    }
	    ?>
	    </select><br />
	    <input type="submit" value="OK" /></p>
	    </form>
	</nav>

	<div id="cadre_stockage">
	<table class="messages_ml">
	<tr><th>Id</th><th>Sujet</th><th>Auteur</th><th>Date</th><th>ML</th><th>Action</th></tr>
	<?php
	if(isset($_GET['ml']) && intval($_GET['ml']) > 0)
		$plus = ' WHERE a.id_ml = '.intval($_GET['ml']).' ';
	else
		$plus = NULL;
		
	$requete = mysql_query('SELECT a.id, a.message, a.titre, a.expediteur, a.date, a.type_contenu, m.nom FROM '.$bdd_prefixe.'archives_ml a
					LEFT JOIN '.$bdd_prefixe.'ml m ON m.id = a.id_ml
					'.$plus.'
					ORDER BY a.date DESC') or die(mysql_error());
					//WHERE ((a.type_contenu = "HTML") OR (a.type_contenu = "PLAIN" AND a.id_message_serveur NOT IN
					//	(SELECT id_message_serveur FROM '.$bdd_prefixe.'archives_ml WHERE type_contenu = \'HTML\')))
	if(!($requete === false))
	{
		while($donnees = mysql_fetch_array($requete))
		{
			//On regarde s'il y a des fichiers
			$requete_bis = mysql_query('SELECT f.id, f.nom FROM '.$bdd_prefixe.'lien_archives l INNER JOIN '.$bdd_prefixe.'fichiers f
			ON f.id = l.id_fichier WHERE l.id_message = '.$donnees['id']);
			$liste_fichier = array();
			while($donnees_bis = mysql_fetch_array($requete_bis))
				$liste_fichier[] = '<a href="voir_fichier.php?id='.$donnees_bis['id'].'">'.$donnees_bis['nom'].'</a>';
			echo '<tr>';
			echo '<td>'.$donnees['id'].'</td>';
			echo '<td>'.utf8_encode($donnees['titre']).'</td>';
			echo '<td>'.$donnees['expediteur'].'</td>';
			echo '<td>'.formater_date_heure($donnees['date']).'</td>';
			echo '<td>'.$donnees['nom'].'</td>';
			echo '<td><a onclick="lire('.$donnees['id'].');" style="cursor: pointer;">Lire</td>';
			echo '</tr>';
			echo '<tr id="m'.$donnees['id'].'" class="fermer"><td colspan="6">';
			if($donnees['type_contenu'] == "HTML")
			{
				//On sauve son body ^^'
				preg_match('#<body[^>]*>(.*)</body[^>]*>#isU', $donnees['message'], $rep);
				if(isset($rep[1]))
					echo htmlspecialchars_decode($rep[1]);
				else
					echo htmlspecialchars_decode($donnees['message']);
			}
			else if ($donnees['type_contenu'] == "PLAIN")
				echo nl2br(htmlspecialchars_decode($donnees['message']));
			else
				echo htmlspecialchars_decode($donnees['message']);
				
			if(count($liste_fichier) > 0)
				echo '<br />Pièces jointes : '.implode(' − ', $liste_fichier);
			echo '</td></tr>';
		}
	}
	?>
	</table>
	</div>
	<?php include('bas_page.php'); ?>
	</body>
</html>