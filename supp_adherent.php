<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & TOUCHE_ADHERENT))
	header('location: index.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($_POST['choix'] == "Oui")
	{
		require(dirname(__FILE__).'/classes/cardgen.php'); // Gestion des cartes de membre
		
		//S'il a un compte, il saute aussi
		$requete = $pdo->query('SELECT id_membre, titre, courriel, cartemembre FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
		$donnees = $requete->fetch(PDO::FETCH_ASSOC);
		// Supprime le fichier de carte adhérent généré
		$md5str = str_replace(array("CG_",".pdf"), '', $donnees['cartemembre']);
		if(CardGen::isMD5($md5str)) // le nom de fichier est-il conforme?
			unlink(dirname(__FILE__).CACHE_DIR.$donnees['cartemembre']); // si oui, on supprime
		//Les ML sautent également
		if($donnees['courriel'] != NULL)
		{
			$requete0 = $pdo->query('SELECT m.adresse FROM '.$bdd_prefixe.'ml_lien l LEFT JOIN '.$bdd_prefixe.'ml m ON m.id = l.id_ml WHERE (l.id_entite = '.$donnees['titre'].' AND l.is_titre = 1) OR (l.id_entite = '.$id.' AND l.is_titre = 0)');
			while($donnees0 = $requete0->fetch(PDO::FETCH_ASSOC))
			{
				$adresse = explode('@', $donnees0['adresse']);
				$adresse = $adresse[0];
				try {
					$soap = new SoapClient("https://www.ovh.com/soapi/soapi-re-1.24.wsdl");

					//login
					$session = $soap->login($nic_ovh, $mdp_ovh, "fr", false);

					$soap->mailingListSubscriberDel($session, $domaine_ovh, $adresse, $donnees['courriel']);

					//logout
					$soap->logout($session);
				} catch(SoapFault $fault) {
					echo $fault;
				}
			}
		}
		if(count($donnees) >= 1)
			$pdo->query('DELETE FROM '.$bdd_prefixe.'membres WHERE id = '.$donnees['id_membre']);
		$pdo->query('DELETE FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
	}
	header('location: adherents.php');
} elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
	header('location: adherents.php');

$id = intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Suppression d'adhérent</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Supprimer un adhérent</h2>

	<form action="supp_adherent.php" method="post">
	<p>
		Voulez-vous vraiment supprimer cet adhérent ?
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Oui" name="choix" /> <input type="submit" value="Non" name="choix" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>
