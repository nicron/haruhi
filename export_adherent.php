<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');
$string = <<<XML
<?xml version="1.0"?>
<kyon version="1.0">
</kyon>
XML;
$xml_retour = simplexml_load_string($string);
$xml_retour->addChild('adherents');
$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'adherents');
if(!($requete === false))
{
	while($donnees = mysql_fetch_array($requete))
	{
		$adherent = $xml_retour->adherents->addChild('adherent');
		$adherent->addAttribute('id', $donnees['id']);
		$adherent->addAttribute('prenom', $donnees['prenom']);
		$adherent->addAttribute('nom', $donnees['nom']);
		$adherent->addAttribute('adresse', $donnees['adresse']);
		$adherent->addAttribute('telephone', $donnees['telephone']);
		$adherent->addAttribute('courriel', $donnees['courriel']);
		$adherent->addAttribute('id_titre', $donnees['titre']);
		$adherent->addAttribute('date_inscription', $donnees['date_inscription']);
		$adherent->addAttribute('date_fin_cotisation', $donnees['date_fin_cotisation']);
		$requete2 = mysql_query('SELECT *, e.id AS id, e.prenom AS prenom, e.nom AS nom, a.prenom AS prenom_intervenant, a.nom AS nom_intervenant FROM '.$bdd_prefixe.'enfants e LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = e.id_intervenant WHERE id_parent = '.$donnees['id']);
		if(!($requete2 === false))
		{
			while($donnees2 = mysql_fetch_array($requete2))
			{
				$enfant = $adherent->addChild('enfant');
				$enfant->addAttribute('id', $donnees2['id']);
				$enfant->addAttribute('prenom', $donnees2['prenom']);
				$enfant->addAttribute('nom', $donnees2['nom']);
			}
		}
	}
}

$requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'titre');
$xml_retour->addChild('titres');
if(!($requete === false))
{
	while($donnees = mysql_fetch_array($requete))
	{
		$titre = $xml_retour->titres->addChild('titre');
		$titre->addAttribute('id', $donnees['id']);
		$titre->addAttribute('nom', $donnees['nom']);
	}
}

header('Content-Transfer-Encoding: binary');
header('Content-Type: text/xml');
header('Content-Disposition: attachment; filename="kyon.xml"');

echo $xml_retour->asXML();
mysql_close();
?>
