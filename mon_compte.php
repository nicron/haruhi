<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');
require(dirname(__FILE__).'/classes/cardgen.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Mon compte</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Gérer son compte</h2>

	<nav>
	<a href="modif_pseudo.php">Modifier son identifiant de connexion</a><br />
	<a href="modif_mdp.php">Modifier son mot de passe</a><br />
	<a href="modif_info_perso.php">Modifier ses informations personnelles</a><br />
	<?php if($fonctionnalites_statut['avatar']) { ?><a href="modif_avatar.php">Modifier son avatar</a><br /><?php } ?>
	<?php if($fonctionnalites_statut['factures']) { ?><a href="mes_factures.php">Voir mes factures</a><br /><?php } ?>
	<?php 
	if( $fonctionnalites_statut['cartemembre'] ) {
		$cg = new CardGen();
		if( $URLCarte = $cg->getURL($_SESSION['id_adherent']) )
			echo '<a href="'.$URLCarte.'">Télécharger sa carte de membre</a><br />';
	}
	?>
	<?php if($fonctionnalites_statut['mikuru'] && (int)$_SESSION['permission'] & AVOIR_COURRIEL) { ?>
		<?php if($_SESSION['type_courriel'] == 1) { ?>
		<a href="modif_mdp_courriel.php">Modifier le mot de passe de son courriel</a><br />
		<?php } elseif($_SESSION['type_courriel'] == 2) { ?>
		<a href="ajout_courriel.php">Transformer son alias en vraie boîte mail</a><br />
		<a href="modif_alias.php">Modifier son alias mail</a><br />
		<?php } else { ?>
		<a href="ajout_courriel.php">Créer une boîte mail</a><br />
		<a href="ajout_alias.php">Créer un alias mail</a><br />
		<?php } ?>
		
		<?php
		$requete = $pdo->query('SELECT mikuru FROM '.$bdd_prefixe.'membres WHERE id = '.$_SESSION['id']);
		$donnees = $requete->fetch(PDO::FETCH_ASSOC);
		if($donnees['mikuru'] == '')
		{ ?>
		<a href="creer_identifiants_mikuru.php">Créer ses identifiants Mikuru</a>
		<?php } else { ?>
		<a href="infos_mikuru.php">Infos Mikuru</a>
		<?php } ?>
	<?php } ?>
	</nav>
	<?php include('bas_page.php'); ?>
	</body>
</html>
