<?php
session_start();
if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	require('configuration.php');
	$pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
	$mdp = hash('sha512', $_POST['mdp']);
	$requete = $pdo->query('SELECT *, m.id AS id, a.id AS id_adherent FROM '.$bdd_prefixe.'membres m
				LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id_membre = m.id 
				LEFT JOIN '.$bdd_prefixe.'titre t ON t.id = a.titre 
				WHERE m.pseudo = "'.$pseudo.'" AND m.mdp = "'.$mdp.'"');
	if(!($requete === false) && $donnees = $requete->fetch())
	{
		$_SESSION['pseudo'] = $donnees['pseudo'];
		$_SESSION['id'] = $donnees['id'];
		$_SESSION['id_adherent'] = $donnees['id_adherent'];
		$_SESSION['co'] = true;
		$_SESSION['type_courriel'] = $donnees['type'];
		$_SESSION['permission'] = $donnees['permission'];
		$_SESSION['visibilite'] = $donnees['visibilite'];
		header('location: index.php');
		exit();
	} else
		$erreur = "Identifiant et/ou mot de passe incorrect(s).";
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Connexion</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<header>
			<h1>Herbier d'Adhérents Réalisé Uniquement pour l'Harmonisation Interne</h1>
			<div id="logo"><img src="images/logo.png" alt="Logo de l'association" /></div>
		</header>

		<?php if(isset($erreur)) { ?><p><?php echo $erreur; ?></p><?php } ?>

		<form action="connexion.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="pseudo">Identifiant : </label></div>
					<div class="cellule"><input name="pseudo" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="mdp">Mot de passe : </label></div>
					<div class="cellule"><input type="password" name="mdp" required /></div>
				</div>
			</div>
			<p>
				<input type="hidden" name="envoi" value="1" />
				<input type="submit" id="bouton_valider" value="Valider" />
			</p>
		</form>

		<p><a href="oubli_mdp.php">J'ai oublié mon mot de passe (et j'ai honte)</a></p>

		<?php include('bas_page.php'); ?>
	</body>
</html>
