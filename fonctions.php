<?php
function formater_date($date)
{
	if($date == "0000-00-00")
		return '';
	else
	{
		$coupe = explode('-', $date);
		if(count($coupe) == 3)
			return $coupe[2].'/'.$coupe[1].'/'.$coupe[0];
		else
			return $date;
	}
}

function formater_date_heure($date)
{
	$coupe = explode(' ', $date);
	return formater_date($coupe[0]).' '.$coupe[1];
}

function signature($date)
{
	return 'Président de l\'association';
}

function reste_jour($date) //Format AAAA-MM-JJ
{
	$coupe = explode('-', $date);
	/* Prend en compte les années bissextiles, etc */
	return round((mktime(0, 0, 0, intval($coupe[1]), intval($coupe[2]), intval($coupe[0])) - time())/86400);
}

function analyserTypeFacture($type, $dateInit)
{
	if($type['cotisation'] == 1) //Type durée supplémentaire
	{
		switch($type['unite'])
		{
			case 1: //jour
				$interval = 'P'.$type['duree'].'D';
				break;
			case 2: //mois
				$interval = 'P'.$type['duree'].'M';
				break;
			case 3: //année
				$interval = 'P'.$type['duree'].'Y';
				break;
			default: //Dans le doute, c'est un 1
				$interval = 'P1Y';
				break;
		}
		$obj_fc = new DateTime($dateInit);
		$obj_fc->add(new DateInterval($interval));
		$date_fin_cotisation = $obj_fc->format("Y-m-d");
	} else if($type['cotisation'] == 2) //Type période
	{
		//On doit connaitre la valeur de n
		$debut_coupe = explode('-', $type['debut_periode']);
		$debut_jour = $debut_coupe[2];
		$debut_mois = $debut_coupe[1];
		$auj_jour = date('d');
		$auj_mois = date('m');

		$n = 0;
		if($auj_mois < $debut_mois || ($auj_mois == $debut_mois && $auj_jour < $debut_jour)) //Si on est après le début de la période, c'est qu'on est à la période suivante
			$n++;

		$termine_coupe = explode('-', $type['termine_periode']);
		$termine_an = date('Y')+$n+$termine_coupe[0];
		$date_fin_cotisation = $termine_an.'-'.$termine_coupe[1].'-'.$termine_coupe[2];
	} else {
		$date_fin_cotisation = date('Y-m-d');
	}

	return array(
		'id' => $type['id'],
		'nom' => $type['nom'],
		'fin' => $date_fin_cotisation
	);
}

function genererCodeAleatoire($longueur = 8)
{
	$list = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	$code = '';
	for($i = 0; $i < $longueur; $i++)
		$code .= $list[rand(0, strlen($list) - 1)];
	return $code;
}

define('MODE_AJOUT', 1);
define('MODE_MODIF', 2);
define('MODE_SUPPR', 3);
?>
