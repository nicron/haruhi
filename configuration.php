<?php

//Infos de connexion
require('infos_connexion.php');

// Connexion a la base de donnée
mysql_connect($host,$username,$password);
mysql_select_db($bdd_name);

//PDO
try{
	if(!isset($bdd_pdo_dns))
		$bdd_pdo_dns = 'mysql:host='.$host.';dbname='.$bdd_name;
	$pdo = new PDO($bdd_pdo_dns, $username, $password);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //Renvoi des exceptions lors d'erreurs
} catch ( Exception $e ) {
	echo "Connexion à MySQL impossible : ", $e->getMessage();
	die();
}

//Permissions
require('permission.php');

//Fonctions
require('fonctions.php');
?>
