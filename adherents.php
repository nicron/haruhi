<?php
session_start();
if(!(isset($_SESSION['co'])) || $_SESSION['co'] === false)
	header('location: connexion.php');
require('configuration.php');

if(!((int)$_SESSION['permission'] & LISTE_ADHERENT))
	header('location: index.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Adhérents</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="edition_facture.css" type="text/css" media="print">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<nav id="panneau_page">
			<h2>Adhérents</h2>

			<?php if((int)$_SESSION['permission'] & TOUCHE_ADHERENT) { ?>
			<p><a href="form_adherent.php">Ajouter un adhérent</a></p>
			<?php } ?>
			<?php if((int)$_SESSION['permission'] & CARTE_ADHERENT) { ?>
			<p><a href="/cardgen.php?pdf=true">Cartes des adhérents</a></p>
			<?php } ?>


			<p>Recherche :
			<form action="adherents.php" method="post">
				<input name="nom" placeholder="Nom" value="<?php if(isset($_POST['nom'])) echo htmlspecialchars($_POST['nom']); ?>" /><br />
				<input name="prenom" placeholder="Prénom" value="<?php if(isset($_POST['prenom'])) echo htmlspecialchars($_POST['prenom']); ?>" /><br />
				Catégorie : <br />
				<select multiple name="titre[]">
					<?php $requete = mysql_query('SELECT id, nom FROM '.$bdd_prefixe.'titre');
					if(!($requete === false))
					{
						while($donnees = mysql_fetch_array($requete))
						{
							echo '<option value="'.$donnees['id'].'"';
							if(isset($_POST['titre']) && count($_POST['titre']) > 0 && in_array($donnees['id'], $_POST['titre']))
								echo ' selected ';
							echo '>'.$donnees['nom'].'</option>';
						}
					}
					?>
				</select><br />
				<!-- 	    <input type="checkbox" value="1" name="liste_ml" /> Afficher sous forme de liste de courriel<br /> -->
				<input type="submit" value="OK" />
			</form></p>

		</nav>

	<div id="cadre_stockage">
		<table>
			<tr><th>ID <a href="adherents.php?tri=id&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=id&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
				<th>Prénom <a href="adherents.php?tri=prenom&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=prenom&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
				<th>Nom <a href="adherents.php?tri=nom&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=nom&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
				<th>Pseudo <a href="adherents.php?tri=pseudo&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=pseudo&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
				<!--<th>Adresse</th>-->
				<!--<th>Téléphone</th>-->
				<th>Courriel</th>
				<th>Statut <a href="adherents.php?tri=statut&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=statut&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
				<th>Date<br />inscription <a href="adherents.php?tri=di&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=di&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
				<th>Date de fin<br />de cotisation <a href="adherents.php?tri=df&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=df&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
				<!--<th>Commentaires <a href="adherents.php?tri=com&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=com&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>-->
				<th>Actions</th></tr>
			<?php 
			$plus_req = array();
			if(isset($_POST['nom']) && strlen($_POST['nom']) > 0)
				$plus_req[] = 'a.nom LIKE "%'.htmlspecialchars($_POST['nom']).'%"';

			if(isset($_POST['prenom']) && strlen($_POST['prenom']) > 0)
				$plus_req[] = 'a.prenom LIKE "%'.htmlspecialchars($_POST['prenom']).'%"';

			if(isset($_POST['titre']) && count($_POST['titre']) > 0)
				$plus_req[] = 'a.titre = '.implode(' OR a.titre = ', $_POST['titre']);

			if(isset($_GET['tri']) && strlen($_GET['tri']) > 0)
			{
				$option_tri = array('id' => 'a.id', 'nom' => 'a.nom', 'prenom' => 'a.prenom', 'pseudo' => 'a.pseudo', 'statut' => 't.nom', 'di' => 'date_inscription', 'df' => 'date_fin_cotisation', 'com' => 'commentaire');
				$tri = htmlspecialchars($_GET['tri']);
				if(isset($option_tri[$tri]))
					$tri = $option_tri[$tri];
				else
					$tri = 'a.nom, a.prenom';

				if(isset($_GET['ordre']) && $_GET['ordre'] == 'desc')
					$tri = ' ORDER BY '.$tri.' DESC';
				else
					$tri = ' ORDER BY '.$tri.' ASC';
			} else
				$tri = ' ORDER BY a.id ASC';

			if(count($plus_req) > 0)
				$plus_req = 'WHERE '.implode(' AND ', $plus_req);
			else
				$plus_req = NULL;

			$requete = $pdo->query('SELECT *, a.id AS id, a.nom AS nom, t.nom AS nom_titre FROM '.$bdd_prefixe.'adherents a LEFT JOIN '.$bdd_prefixe.'titre t ON a.titre = t.id '.$plus_req.$tri);
			if(!($requete === false))
			{
				$items = $requete->fetchAll();
				foreach($items as $donnees)
				{
					if(isset($_POST['liste_ml']) && $_POST['liste_ml'] == 1)
						echo $donnees['courriel'].'<br />';
					else
					{
						$jour = reste_jour($donnees['date_fin_cotisation']);
						echo '<tr';
						if($donnees['date_inscription'] == "0000-00-00")
							echo ' class="pas_membre" ';
						elseif($jour > 30)
							echo ' class="cotisation_ok" ';
						elseif($jour <= 30 && $jour > 7)
							echo ' class="cotisation_fin_loin" ';
						elseif($jour <= 7 && $jour >= 0)
							echo ' class="cotisation_fin_proche" ';
						else
							echo ' class="cotisation_fini" ';
						//echo ' style="background-color: '.$donnees['couleur'].';"';
						echo '><td style="text-align: center;">'.$donnees['id'].'</td><td>'.$donnees['prenom'].'</td><td>'.$donnees['nom'].'</td>';
						echo '<td>'.$donnees['pseudo'].'</td>';
//						echo '<td>'.$donnees['adresse'].'</td>';
						//echo '<td>'.$donnees['telephone'].'</td>';
						echo '<td>'.$donnees['courriel'].'</td>';
						echo '<td>'.$donnees['nom_titre'].'</td>';
						echo '<td>'.formater_date($donnees['date_inscription']).'</td><td>'.formater_date($donnees['date_fin_cotisation']).'</td>';
						//echo '<td>'.$donnees['commentaire'].'</td>';
						echo '<td>';
						if((int)$_SESSION['permission'] & TOUCHE_ADHERENT)
							echo '<a href="form_adherent.php?id='.$donnees['id'].'"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a> ';
						echo '<a href="voir_adherent.php?id='.$donnees['id'].'"><img src="images/adherent_fiche.png" title="Fiche" alt="Fiche" /></a> ';
						//echo '<a href="enfant.php?id='.$donnees['id'].'"><img src="images/adherent_enfant.png" title="Enfants" alt="Enfants" /></a>';
						if((int)$_SESSION['permission'] & FAIRE_FACTURE)
							echo ' <a href="cotisation.php?id='.$donnees['id'].'"><img src="images/adherent_cotisation.png" title="Cotisation" alt="Cotisation" /></a>';
						if($donnees['id_membre'] == 0 && (int)$_SESSION['permission'] & TOUCHE_ADHERENT)
							echo ' <a href="creer_compte.php?id='.$donnees['id'].'"><img src="images/adherent_creer_compte.png" title="Créer un compte" alt="Créer un compte" /></a>';
						if((int)$_SESSION['permission'] & TOUCHE_ADHERENT)
							echo '<a href="supp_adherent.php?id='.$donnees['id'].'"><img src="images/adherent_supprimer.png" title="Supprimer" alt="Supprimer" /></a> ';
						if($jour >= 0) /* lien carte */
							echo '<a href="cardgen.php?id='.$donnees['id'].'"><img src="images/stockage_pdf.png" title="Carte" alt="Carte" /></a> ';
						echo '</td></tr>';
					}
				}
			}
			?>
		</table>
	</div>

	<?php include('bas_page.php'); ?>
	</body>
</html>
